import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check'
import Orders from '../orders.js'
import Invoices from '../../invoices/invoices.js'


// const insertCustomers = (id, custId) => {
//   try {
//     const upsert = {
//       chargeId: id,
//       customerId: custId
//     }
//     return Customers.insert(upsert)
//   } catch (exception) {
//     console.warn(`${exception}`);
//   }
// };

var stripe = StripeAPI(Meteor.settings.StripePri);
let amountToBeCharged

Meteor.methods({

  "addCard": function(cardToken){
    check(cardToken, String)
    if(Meteor.user().stripeCust == null){
      var custCreate = Async.runSync(function(done){
        stripe.customers.create({
          source: cardToken
        }, function(err, response){
          done(err, response);
        })
      })

      if(custCreate.error){
        throw new Meteor.error(500, "stripe-error", custCreate.error.message);
      }else{
        Meteor.users.update(Meteor.userId(), {$set: {stripeCust: custCreate.result.id}});
        return
      }
    }else{
      var custUpdate = Async.runSync(function(done){
        stripe.customers.update(Meteor.user().stripeCust,{
          source: cardToken
        }, function(err, result) {
          done(err, result);
        })
      })

      if(custUpdate.error){
        throw new Meteor.error(500, "stripe-error", custUpdate.error.message);
      }else{
        return
      }
    }
  },
  "loadCardInfo": function(){
     if(Meteor.user().stripeCust == null){
       return {
         "hasCard": false
       }
     }else{
       var custDetails = Async.runSync(function(done){
         stripe.customers.retrieve(Meteor.user().stripeCust, function(err, result){
           done(err, result);
         })
       })

       if(custDetails.result.sources.data.length == 0){
         return {
           "hasCard": false
         }
       }else{
         var cardDetails = custDetails.result.sources.data[0];
         var allOrders = Orders.find({userId: this.userId}, {sort: {createdAt: -1}}).fetch()
         amountToBeCharged = allOrders[0].amount
         return {
           "hasCard": true,
           "order_amount": amountToBeCharged,
           "cardInfo": {
             "brand": cardDetails.brand,
             "exp_month": cardDetails.exp_month,
             "exp_year": cardDetails.exp_year,
             "last4": cardDetails.last4
           }
         }
       }
     }
   },
  "chargeUser": function(){
      stripe.charges.create({
        amount: amountToBeCharged*100,
        currency: "usd",
        customer: Meteor.user().stripeCust
        }, function(err, result){
        return {
          "id" : result.id,
          "status": result.status
        }
      })
   }
})
