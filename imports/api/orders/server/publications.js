import { Meteor } from 'meteor/meteor'
import { check } from 'meteor/check'
import Orders from '../orders'
import _ from 'lodash'


Meteor.publish('orders.list', () => Orders.find());


Meteor.publish('order.view', (_id) => {
  check(_id, String);
  return Orders.find({_id}, {limit: 10, sort: {viewCounts: -1}});
});

Meteor.publish('order.new', function(_id){
  if(Meteor.user().stripeCust){
  return Orders.find({_id}, {limit: 10, sort: {viewCounts: -1}})
  } else {
    return false
  }
});

Meteor.publish('order.amount', function () {
  if (!this.userId) {
    return this.ready();
  }
  return Orders.find({userId: this.userId}, { sort: {createdAt: -1}})
});
