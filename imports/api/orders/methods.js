import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check'
import { Bert } from 'meteor/themeteorchef:bert';
import { Email } from 'meteor/email';
import Orders from './orders';
import Blogs from '../blogs/blogs';
import Menus from '../menus/menus'
import Instructions from '../instructions/instructions'
import rateLimit from '../../modules/rate-limit.js';
import { SSR } from 'meteor/meteorhacks:ssr';
// import templateToHTML from './server/template-to-html.js'
// if (Meteor.isServer){
// import { sendEmailNotification } from './server/email-notifs.js';
// }




export const upsertOrder = new ValidatedMethod({
  name: 'orders.upsert',
  validate: new SimpleSchema({
    _id: { type: String, optional: true },
    menuId: { type: String, optional: true },
    userId: { type: String, regEx: SimpleSchema.RegEx.Id, optional: true },
    menuTitle: { type: String, optional: true },
    address: { type: String, optional: true },
    items: { type: Number, optional: true },
    amount: { type: Number, optional: true }
  }).validator(),
  run(order) {
    return Orders.upsert({ _id: order._id }, { $set: order });
  },
});

export const removeOrder = new ValidatedMethod({
  name: 'order.remove',
  validate: new SimpleSchema({
    _id: { type: String },
  }).validator(),
  run({ _id }) {
    Orders.remove(_id);
  },
});

export const sendOrderNotification = new ValidatedMethod({
    name: 'orders.sendNotification',
    validate: new SimpleSchema({
      type: { type: String },
      payload: { type: Object, blackbox: true },
    }).validator(),
    run({ type, payload }) {
      sendEmailNotification(type, payload);
    },
});

export const sendReceivedOrderEmail = Meteor.methods({
  sendReceivedOrderEmailMethod: function (to, from,  bcc, replyTo, subject, html) {
    // check([to, from, subject, html], [String]);
    const currentUser = Meteor.users.findOne({_id: this.userId})
    const allOrders = Orders.find({userId: this.userId}, {sort: {createdAt: -1}}).fetch()
    const allBlogs = Blogs.find({}, {sort: {createdAt: -1}}).fetch()

    const instructions = Instructions.find({"menuId" : "QnWBu8TSjmStjBPFu"}).fetch()


    const instructionContent = instructions.map((instruction, index) =>{
      return (
        `<table id="gif-instruction${index}" cellpadding="0" cellspacing="0" border="0" bgcolor="#4F6331" align="center" style="width: 100%; padding: 0; margin: 0; background-image: url(http://media2.giphy.com/media/${instruction.image}/200.gif); background-size: auto 102%; background-position: center center; background-repeat: no-repeat; background-color: #080e02">
                <tbody >
                <!-- Main Title -->
                  <tr>
                    <td height="460"></td>
                  </tr>
                  <tr>
                    <td colspan="3" height="60" align="center">
                      <div border="0" style="border: none; line-height: 60px; color: #ffffff; font-family: Verdana, Geneva, sans-serif; font-size: 32px; text-transform: uppercase; font-weight: bolder;">${instruction.title}</div>
                    </td>
                  </tr>
                  <!-- Line 1 -->
                  <tr>
                    <td colspan="3" height="20" valign="bottom" align="center">
                      <img src="https://github.com/lime7/responsive-html-template/blob/master/index/line-1.png?raw=true" alt="line" border="0" width="760" height="5" style="border: none; outline: none; max-width: 760px; width: 100%; -ms-interpolation-mode: bicubic;" >
                    </td>
                  </tr>
                  <!-- Instruction 1 -->
                  <tr>
                    <td colspan="3">
                      <table cellpadding="0" cellspacing="0" border="0" align="center" style="padding: 0; margin: 0; width: 100%;">
                        <tbody>
                              <div border="0" style="border: none; height: 100px;">
                                <div style="border: none; height: 50px;">
                                  <p style="font-size: 18px; line-height: 24px; margin-top: 10px; padding-left: 20px; padding-right: 20px; font-family: Verdana, Geneva, sans-serif; color: #ffffff; text-align: center; mso-table-lspace:0;mso-table-rspace:0;">
                                  ${instruction.description}<a href="http://www.2-5pm.com/restaurants/suggest" style="color: #1ab703"> 6mins watch</a>
                                  </p>
                                </div>
                              </div>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table><!-- End Start Section -->
              <tbody>
              <table id="text-instructions1" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" align="center" style="width: 100%; padding: 0; margin: 0; background-color: #ffffff;">
                <tbody>
                  <tr><td colspan="3" height="1"></td></tr>
                  <tr>
                      <div border="0" style="border: none; line-height: 14px; color: #727272; font-family: Verdana, Geneva, sans-serif; font-size: 16px; width: 100%; padding-bottom: 20px; padding-top: 15px; margin: 0; background-color: #ffffff;">
                        <p href="http://www.2-5pm.com" target="_blank" border="0" style="border: none; outline: none; text-decoration: none; line-height: 22px; font-size: 18px; color: #333; font-family: Verdana, Geneva, sans-serif; -webkit-text-size-adjust:none; text-align: left; padding-right: 20px; padding-left: 35px;">  ${instruction.instructions}<a style="color: #1ab703">Save To Collection</a></p>
                      </div>

                  </tr>

                </tbody>
              </table>
              </tbody>

              <tr>
                <td colspan="3" height="0" valign="bottom" align="center">
                  <img src="https://github.com/lime7/responsive-html-template/blob/master/index/line-1.png?raw=true" alt="line" border="0" width="960" height="35" style="border: none; outline: none; max-width: 960px; width: 100%; -ms-interpolation-mode: bicubic; background-color: #ffffff;" >
                </td>
              </tr>`
      )
    })
    const instructionBody = instructionContent.join(' ')
    const menuId = allOrders[0].menuId
    const newsId = allBlogs[0]._id
    const newsCoverImage = allBlogs[0].coverImage
    const newsTitle = allBlogs[0].title
    const newsHighlight = allBlogs[0].highlight
    const menuDetails = Menus.findOne({_id: menuId})
    console.log(menuDetails.coverImage)
    // const firstName = currentUser.profile.name.first
    // const currentUserEmail = currentUser.emails[0]
    console.log(currentUser.emails[0])
    SSR.compileTemplate('htmlEmail', Assets.getText('email-templates/order-received.html'))


    var emailData = {
      greeting: "Hello, " + currentUser.profile.name.first,
      message: `We have received your order the menu ${menuDetails.title} from ${menuDetails.restaurantName} restaurant for the amount of $${allOrders[0].amount}. You should receive a delivery email within 24 hours. Thanks and have a great day ${currentUser.profile.name.first}!`,
      restaurantName: menuDetails.restaurantName,
      linkToMenu: `http://www.2-5pm.com/menus/${menuId}`,
      messageStyle: `width: 100%; padding: 0; margin: 0; background-image: url(https://source.unsplash.com/${menuDetails.coverImage}/960x300); background-size: 100% 100%; background-position: center center; background-repeat: no-repeat; background-color: #ECECED;`,
      mainImageStyle: `width: 100%; padding: 0; margin: 0; background-image: url(https://source.unsplash.com/${menuDetails.coverImage}/); background-size: auto 100%; background-position: center center; background-repeat: no-repeat; background-color: #080e02`,
      linkToNews: `http://www.2-5pm.com/news/${newsId}`,
      newsTitle: newsTitle,
      newsHighlight: newsHighlight,
      newsStyle: `width: 100%; padding: 0; margin: 0; background-image: url(https://source.unsplash.com/${newsCoverImage}//960x300); background-size: 100% 100%; background-position: center center; background-repeat: no-repeat; background-color: #ECECED;`
    };

    // Let other method calls from the same client start running,
    // without waiting for the email sending to complete.
    this.unblock();
    Email.send({
      to: currentUser.emails[0].address,
      from: "deliverhappyh@gmail.com",
      bcc: ["philipTranP@gmail.com", "deliverhappyh@gmail.com"],
      replyTo: "deliverhappyh@gmail.com",
      subject: "YOUR ORDER WAS RECEIVED!",
      html: SSR.render('htmlEmail', emailData)
    });
  }
});


rateLimit({
  methods: [
    upsertOrder,
    removeOrder,
    sendOrderNotification
  ],
  limit: 5,
  timeRange: 1000,
});

//   Meteor.call('orders.sendNotification', { type: 'orderWasPlaced',
//    payload: {to: 'plt1028@gmail.com', orderId: '123456'}
//   })
