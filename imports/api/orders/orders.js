import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { Factory } from 'meteor/dburles:factory';


const Orders = new Mongo.Collection('Orders');
export default Orders;

Orders.allow({
  insert: () => false,
  update: () => false,
  remove: () => false,
});

Orders.deny({
  insert: () => true,
  update: () => true,
  remove: () => true,
});

Orders.schema = new SimpleSchema({
  menuId: {
    type: String,
    label: "Menu ID",
  },
  userId: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    label: "User ID"
  },
  menuTitle: {
    type: String,
    label: 'The title of the menu.',
  },
  address: {
    type: String,
    label: 'The address and optional message.',
  },
  items: {
    type: Number,
    label: 'Numbers of items',
  },
  amount: {
    type: Number,
    label: 'Total amount',
  },
  createdAt: {
    type: Date,
    autoValue: function() {
      if (this.isInsert) {
        return new Date();
      } else if (this.isUpsert) {
        return {$setOnInsert: new Date()};
      } else {
        this.unset();  // Prevent user from supplying their own value
      }
    }
  }

});

Orders.attachSchema(Orders.schema);

Factory.define('order', Orders, {
  menuId: () => 'Factory Menu ID',
  userId: () => 'Factory User ID',
  menuTitle: () => 'Factory Menu Title',
  address: () => 'Factory Customer Address',
  items: () => 3,
  amount: () => 80
});
