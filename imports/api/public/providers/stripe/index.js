/* eslint-disable consistent-return */

import { Meteor } from 'meteor/meteor';
import { chargeSucceeded } from './charge-succeeded.js';
import { payoutCreated } from './payout-created.js';
import { invoicePaymentSucceeded } from '../../../../modules/server/stripe/webhooks/invoice.payment_succeeded.js'

const scenarios = {
  'charge.succeeded': chargeSucceeded,
  'payout.created': payoutCreated,
  'invoice.payment_succeeded': invoicePaymentSucceeded
};

const handler = ({ body }) => {
  try {
    const { type, data } = body;
    const scenario = scenarios[type];
    if (scenario) scenario(data.object);
  } catch (exception) {
    throw new Meteor.Error('500', `[stripeHandler.handler] ${exception}`);
  }
};

export const stripeHandler = (options) => handler(options);
