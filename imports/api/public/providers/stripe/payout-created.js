import { Meteor } from 'meteor/meteor';
import Transactions from '../../../transactions/transactions.js';

const insertTransactions = (id, custId, amount) => {
  try {
    const upsert = {
      userId: id,
      customerId: custId,
      amount: amount
    }
    return Transactions.insert(upsert)
  } catch (exception) {
    console.warn(`[payoutCreated.insertTransactions] ${exception}`);
  }
};

let module;

const handler = (data, promise) => {
  try {
    module = promise;
    console.log(data)
/*
    const customerId = data.source.customer
    const amount = data.object.amount
    const currentUser = Meteor.users.find({"stripeCust": customerId}).fetch()
    console.log(currentUser.emails[0])
    console.log(currentUser.profile.name)
    console.log(currentUser[0]._id)
    if(currentUser) insertTransactions(currentUser[0]._id, customerId, amount)
*/
  } catch (exception) {
    module.reject(`[payoutCreated.handler] ${exception}`);
  }
};

export const payoutCreated = (data) => handler(data);
