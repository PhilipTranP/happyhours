import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { Factory } from 'meteor/dburles:factory';

const Documents = new Mongo.Collection('Documents');
export default Documents;

Documents.allow({
  insert: () => false,
  update: () => false,
  remove: () => false,
});

Documents.deny({
  insert: () => true,
  update: () => true,
  remove: () => true,
});

Documents.schema = new SimpleSchema({
  restaurantName: {
    type: String,
    label: 'The name of the restaurant.',
  },
  restaurantAbout: {
    type: String,
    label: 'Some info of the restaurant.',
  },
  chefName: {
    type: String,
    label: 'Name of the chef.',
  },
  chefBio: {
    type: String,
    label: 'Some bio of the chef',
  },
});

Documents.attachSchema(Documents.schema);

Factory.define('document', Documents, {
  restaurantName: () => 'Factory Name',
  restaurantAbout: () => 'Factory About',
  chefName: () => 'Factory Chef Name',
  chefBio: () => 'Factory Chef Bio'
});
