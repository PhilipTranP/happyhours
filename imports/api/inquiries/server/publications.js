import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import Inquiries from '../inquiries';


Meteor.publish('inquiries.list', () => Inquiries.find());


Meteor.publish('inquiries.view', (_id) => {
  check(_id, String);
  return Inquiries.find(_id);
});
