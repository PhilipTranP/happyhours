import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { Factory } from 'meteor/dburles:factory';

const Inquiries = new Mongo.Collection('Inquiries');
export default Inquiries;

Inquiries.allow({
  insert: () => false,
  update: () => false,
  remove: () => false,
});

Inquiries.deny({
  insert: () => true,
  update: () => true,
  remove: () => true,
});

Inquiries.schema = new SimpleSchema({
  firstName: {
    type: String,
    label: 'name',
  },
  email: {
    type: String,
    label: 'email',
  },
  message: {
    type: String,
    label: 'message',
  },
  createdAt: {
    type: Date,
    autoValue: function() {
      if (this.isInsert) {
        return new Date();
      } else if (this.isUpsert) {
        return {$setOnInsert: new Date()};
      } else {
        this.unset();  // Prevent user from supplying their own value
      }
    }
  }
});

Inquiries.attachSchema(Inquiries.schema);

Factory.define('inquiry', Inquiries, {
  firstName: () => 'Factory Name',
  email: () => 'Factory Email',
  message: () => 'Factory Message'
});
