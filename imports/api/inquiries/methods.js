import { SimpleSchema } from 'meteor/aldeed:simple-schema'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import Inquiries from './inquiries'
import Blogs from '../blogs/blogs'
import rateLimit from '../../modules/rate-limit.js'
import { Email } from 'meteor/email'
import { SSR } from 'meteor/meteorhacks:ssr'

export const upsertInquiry = new ValidatedMethod({
  name: 'inquiry.upsert',
  validate: new SimpleSchema({
    _id: { type: String, optional: true },
    firstName: { type: String, optional: true },
    email: { type: String, optional: true },
    message: { type: String, optional: true }
  }).validator(),
  run(inquiry) {
    return Inquiries.upsert({ _id: inquiry._id }, { $set: inquiry });
  },
});

export const removeInquiry = new ValidatedMethod({
  name: 'inquiry.remove',
  validate: new SimpleSchema({
    _id: { type: String },
  }).validator(),
  run({ _id }) {
    Inquiries.remove(_id);
  },
});

rateLimit({
  methods: [
    upsertInquiry,
    removeInquiry,
  ],
  limit: 5,
  timeRange: 1000,
});


export const sendApproveEmail = Meteor.methods({
  sendApproveEmailMethod: function (to, from, bcc, replyTo, subject, html) {
    const allInquiries = Inquiries.find({}, {sort: {createdAt: -1}}).fetch()
    const welcomeArticle = Blogs.findOne({"_id" : "gzXiyubsKtgna4T7E"})
    // const welcomeArticle = Blogs.findOne({"_id" : "uQiXBfuBxhHZxA2Qp"})

    SSR.compileTemplate('htmlEmail', Assets.getText('email-templates/response-signup.html'))


    var emailData = {
     inquiryName: allInquiries[0].firstName,
     inquiryEmail: allInquiries[0].email,
     inquiryMessage: allInquiries[0].message,
     signUpLink: `http://www.2-5pm.com/member-signup/${allInquiries[0]._id}`,
     articleLink: `http://www.2-5pm.com/news/${welcomeArticle}`,
     articleHighlight: welcomeArticle.highlight.replace(/(([^\s]+\s\s*){17})(.*)/,"$1…"),
     articleTitle: welcomeArticle.title,
     articleStyle: `width: 100%; padding: 0; margin: 0; background-image: url(https://source.unsplash.com/${welcomeArticle.coverImage}/960x300); background-size: 100% 100%; background-position: center center; background-repeat: no-repeat; background-color: #ECECED;`
    }

    // Let other method calls from the same client start running,
    // without waiting for the email sending to complete.
    this.unblock();
    Email.send({
      to: "philipTranP@gmail.com",
      from: "deliverhappyh@gmail.com",
      bcc: ["plt1028@gmail.com", "deliverhappyh@gmail.com"],
      replyTo: "deliverhappyh@gmail.com",
      subject: `Signup link for ${allInquiries[0].firstName}`,
      html: SSR.render('htmlEmail', emailData)
    });
  }
});

// export const sendEmail = Meteor.methods({
//   sendEmail: function (to, from, subject, text) {
//     check([to, from, subject, text], [String]);
//     // Let other method calls from the same client start running,
//     // without waiting for the email sending to complete.
//     this.unblock();
//     Email.send({
//       to: to,
//       from: from,
//       subject: subject,
//       text: text
//     });
//   }
// });
