import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { check } from 'meteor/check'
import Instructions from './instructions';


export const CookingInstruction = new ValidatedMethod({
  name: 'CookingInstruction.upsert',
  validate: new SimpleSchema({
    _id: { type: String, optional: true },
    authorId: { type: String, optional: true },
    subscribers: { type: [String], optional: true },
    title: { type: String, optional: true },
    price: { type: Number, optional: true },
    description: { type: String, optional: true },
    instructionVideo: { type: String, optional: true },
    startTime: { type: Number, optional: true },
    endTime: { type: Number, optional: true },
    image: { type: String, optional: true },
    instructions: { type: String, optional: true },
    menuId: { type: String, optional: true },
    restaurantId: { type: String, optional: true }

  }).validator(),
  run(instruction) {
    return Instructions.upsert({ _id: instruction._id }, { $set: instruction });
  },
});


export const removeInstruction = new ValidatedMethod({
  name: 'instructions.remove',
  validate: new SimpleSchema({
    _id: { type: String },
  }).validator(),
  run({ _id }) {
    Instructions.remove(_id);
  },
});

export const UpdateSubscribers = Meteor.methods({
  "update-subscribers" : function(instructionId){
    check(instructionId, String)
    return Instructions.update(instructionId, { $push: {subscribers: this.userId}})
  }
})
