import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { Factory } from 'meteor/dburles:factory';

const Instructions = new Mongo.Collection('Instructions');
export default Instructions;

Instructions.allow({
  insert: () => false,
  update: () => false,
  remove: () => false,
});

Instructions.deny({
  insert: () => true,
  update: () => true,
  remove: () => true,
});

Instructions.schema = new SimpleSchema({

  authorId: {
    type: String,
    label: 'Author ID',
  },
  subscribers: {
    type: [String],
    label: 'Author ID',
  },
  title: {
    type: String,
    label: 'Instruction Title',
  },
  price: {
    type: Number,
    label: "Item Price"
  },
  description: {
    type: String,
    label: 'Description',
  },
  instructionVideo: {
    type: String,
    label: 'Instruction Video',
  },
  startTime: {
    type: Number,
    label: 'Start Time',
  },
  endTime: {
    type: Number,
    label: 'End Time',
  },
  image: {
    type: String,
    label: 'Image',
  },
  instructions: {
    type: String,
    label: 'Description',
  },
  menuId: {
    type: String,
    label: 'Menu ID',
  },
  restaurantId: {
    type: String,
    label: 'Restaurant ID',
  },
  createdAt: {
    type: Date,
    autoValue: function() {
      if (this.isInsert) {
        return new Date();
      } else if (this.isUpsert) {
        return {$setOnInsert: new Date()};
      } else {
        this.unset();  // Prevent user from supplying their own value
      }
    }
  }

});

Instructions.attachSchema(Instructions.schema);

Factory.define('document', Instructions, {
  authorId: () => 'Author ID',
  subscribers: () => ['subscribers ID'],
  title: () => 'Factory Title',
  price: () => 10,
  description: () => "Factory Description",
  instructionVideo: () => 'Factory Instruction Video',
  startTime: () => 0,
  endTime: () => 10,
  image: () => "Factory Image",
  instructions: () => "Factory Instructions",
  menuId: () => 'Factory Menu Id',
  restaurantId: () => 'Factory Restaurant Id'
});
