import { Meteor } from 'meteor/meteor';
import { check, Match } from 'meteor/check';
import Instructions from '../instructions';
import Menus from '../../menus/menus'
import _ from 'lodash';


Meteor.publish('instructions.list', () => Instructions.find());


Meteor.publish('instructions.view', (_id) => {
  check(_id, String);
  return Instructions.find(_id);
});

Meteor.publish('instructions.subscribed', function(){
  if(!this.userId){ return }
  return Instructions.find({subscribers: this.userId})  
})

Meteor.publish('instructions.topMenu', () => {
  const { _id } = Menus.findOne({}, {sort: {viewCounts: -1}})
  return Instructions.find({"menuId" : _id})
});

Meteor.publish('instructions.search', (searchTerm) => {
  check(searchTerm, Match.OneOf(String, null, undefined));

  let query = {};
  const projection = { limit: 20, sort: {createdAt: -1} };


  if (searchTerm) {
    const regex = new RegExp(searchTerm, 'i');

    query = {
      $or: [
        { title: regex },
        { description: regex },
        { instructions: regex }
      ],
    };

    projection.limit = 100;
  }

  return Instructions.find(query, projection);
})
