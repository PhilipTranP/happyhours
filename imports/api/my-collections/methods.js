import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import MyCollections from './my-collections';


export const MyCollectionsMethod = new ValidatedMethod({
  name: 'my-collections.upsert',
  validate: new SimpleSchema({
    _id: { type: String, optional: true },
    ownerId: { type: String, optional: true },
    instructionId: { type: String, optional: true }
  }).validator(),
  run(instruction) {
    return MyCollections.upsert({ _id: instruction._id }, { $set: instruction });
  },
});


export const removeItemCollections = new ValidatedMethod({
  name: 'my-collections.remove',
  validate: new SimpleSchema({
    _id: { type: String },
  }).validator(),
  run({ _id }) {
    MyCollections.remove(_id);
  },
});
