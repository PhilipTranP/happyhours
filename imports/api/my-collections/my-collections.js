import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

const MyCollections = new Mongo.Collection('MyCollections');

MyCollections.allow({
  insert: () => false,
  update: () => false,
  remove: () => false,
});

MyCollections.deny({
  insert: () => true,
  update: () => true,
  remove: () => true,
});

const MyCollectionsSchema = new SimpleSchema({
  ownerId: {
    type: String,
    label: 'owner ID',
  },
  instructionId: {
    type: String,
    label: 'Instruction ID',
  }
});

MyCollections.attachSchema(MyCollectionsSchema);

export default MyCollections;
