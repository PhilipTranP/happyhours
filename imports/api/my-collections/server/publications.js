import { Meteor } from 'meteor/meteor';
import MyCollections from '../my-collections';
import Instructions from '../../instructions/instructions';

Meteor.publish('all-collections', () => MyCollections.find());

Meteor.publish('my-collections', function() {
  return MyCollections.find({ownerId: this.userId})
})

Meteor.publish('my-collections.search', (searchTerm) => {
  check(searchTerm, Match.OneOf(String, null, undefined));

  let query = {};
  const projection = { limit: 20, sort: {createdAt: -1} };

  if (searchTerm) {
    const regex = new RegExp(searchTerm, 'i');

    query = {
      $or: [
        { title: regex },
        { description: regex },
        { instructions: regex }
      ],
    };

    projection.limit = 100;
  }

  return MyCollections.find(query, projection);
})
