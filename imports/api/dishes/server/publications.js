import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import Dishes from '../dishes';


Meteor.publish('dishes.list', () => Dishes.find());


Meteor.publish('dishes.view', (_id) => {
  check(_id, String);
  return Dishes.find(_id);
});
