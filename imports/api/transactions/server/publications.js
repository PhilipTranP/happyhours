import { Meteor } from 'meteor/meteor';
import { check, Match } from 'meteor/check';
import Transactions from '../transactions';


Meteor.publish('transactions.list', () => Transactions.find({}, {sort: {createdAt: -1}}));


Meteor.publish('transaction.view', (_id) => {
  return Transactions.find(_id);
});



Meteor.publish('transactions.search', (searchTerm) => {
  check(searchTerm, Match.OneOf(String, null, undefined));

  let query = {};
  const projection = { limit: 20, sort: {createdAt: -1} };

  if (searchTerm) {
    const regex = new RegExp(searchTerm, 'i');

    query = {
      $or: [
        { title: regex },
        { highlight: regex },
        { body: regex }
      ],
    };

    projection.limit = 100;
  }

  return Transactions.find(query, projection);
})
