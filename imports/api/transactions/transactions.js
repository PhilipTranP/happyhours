import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

const Transactions = new Mongo.Collection('Transactions');

Transactions.allow({
  insert: () => false,
  update: () => false,
  remove: () => false,
});

Transactions.deny({
  insert: () => true,
  update: () => true,
  remove: () => true,
});

const TransactionsSchema = new SimpleSchema({
  userId: {
    type: String,
    label: 'Charge ID from Stripe',
  },
  customerId: {
    type: String,
    label: 'The user\'s customer ID on Stripe.',
  },
  amount: {
    type: Number,
    label: 'Amount in cents.',
  }

});

Transactions.attachSchema(TransactionsSchema);

export default Transactions;
