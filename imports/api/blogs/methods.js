import { SimpleSchema } from 'meteor/aldeed:simple-schema'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import Blogs from './blogs';
import rateLimit from '../../modules/rate-limit.js'

export const upsertBlog = new ValidatedMethod({
  name: 'blog.upsert',
  validate: new SimpleSchema({
    _id: { type: String, optional: true },
    title: { type: String, optional: true },
    highlight: { type: String, optional: true },
    body: { type: String, optional: true },
    coverImage: { type: String, optional: true },
    bodyImage: { type: String, optional: true },
    viewCounts: {type: Number, optional: true}
  }).validator(),
  run(blog) {
    return Blogs.upsert({ _id: blog._id }, { $set: blog })
  },
});

export const removeBlog = new ValidatedMethod({
  name: 'blog.remove',
  validate: new SimpleSchema({
    _id: { type: String },
  }).validator(),
  run({ _id }) {
    Blogs.remove(_id)
  },
});

export const blogUpdateViewCounts = new ValidatedMethod({
  name: 'blogViewCounts.update',
  validate: new SimpleSchema({
    _id: { type: String },
  }).validator(),
  run({ _id }) {
    Blogs.update(_id, { $inc: {viewCounts: 1} })
  }
});

rateLimit({
  methods: [
    upsertBlog,
    removeBlog,
    blogUpdateViewCounts
  ],
  limit: 5,
  timeRange: 1000,
});
