import { Meteor } from 'meteor/meteor';
import { check, Match } from 'meteor/check';
import Blogs from '../blogs';


Meteor.publish('blogs.list', () => Blogs.find({}, {sort: {createdAt: -1}}));


Meteor.publish('blog.view', (_id) => {
  return Blogs.find(_id);
});



Meteor.publish('blogs.search', (searchTerm) => {
  check(searchTerm, Match.OneOf(String, null, undefined));

  let query = {};
  const projection = { limit: 20, sort: {createdAt: -1} };

  if (searchTerm) {
    const regex = new RegExp(searchTerm, 'i');

    query = {
      $or: [
        { title: regex },
        { highlight: regex },
        { body: regex }
      ],
    };

    projection.limit = 100;
  }

  return Blogs.find(query, projection);
})

Meteor.publish('blogs.topBlog', () =>{
  return Blogs.find({}, {limit: 1, sort: {viewCounts: -1}})
})
