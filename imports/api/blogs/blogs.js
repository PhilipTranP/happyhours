import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { Factory } from 'meteor/dburles:factory';

const Blogs = new Mongo.Collection('Blogs');
export default Blogs;

Blogs.allow({
  insert: () => false,
  update: () => false,
  remove: () => false,
});

Blogs.deny({
  insert: () => true,
  update: () => true,
  remove: () => true,
});

Blogs.schema = new SimpleSchema({
  title: {
    type: String,
    label: 'title',
  },
  highlight: {
    type: String,
    label: 'highlight',
  },
  body: {
    type: String,
    label: 'body',
  },
  coverImage: {
    type: String,
    label: 'cover image',
  },
  bodyImage: {
    type: String,
    label: 'body image',
  },
  viewCounts: {
    type: Number,
    defaultValue: 0,
    label: 'view counts'
  },
  createdAt: {
    type: Date,
    autoValue: function() {
      if (this.isInsert) {
        return new Date();
      } else if (this.isUpsert) {
        return {$setOnInsert: new Date()};
      } else {
        this.unset();  // Prevent user from supplying their own value
      }
    }
  }
});

Blogs.attachSchema(Blogs.schema);

Factory.define('blog', Blogs, {
  title: () => 'Factory Blog Title',
  highlight: () => 'Factory Blog Highlight',
  body: () => 'Factory Blog Body',
  coverImage: () => 'Factory Blog Cover Image',
  bodyImage: () => 'Factory Blog Body Image'
});
