import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

const Invoices = new Mongo.Collection('Invoices');

Invoices.allow({
  insert: () => false,
  update: () => false,
  remove: () => false,
});

Invoices.deny({
  insert: () => true,
  update: () => true,
  remove: () => true,
});

const InvoicesSchema = new SimpleSchema({

  stripeCust: {
    type: String,
    label: 'The ID of the user from Stripe',
  }

});

Invoices.attachSchema(InvoicesSchema);

export default Invoices;
