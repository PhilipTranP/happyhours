import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import Menus from './menus';
import rateLimit from '../../modules/rate-limit.js';


export const upsertMenu = new ValidatedMethod({
  name: 'menus.upsert',
  validate: new SimpleSchema({
    _id: { type: String, optional: true },
    isPublished: { type: Boolean, optional: true, defaultValue: false},
    title: { type: String, optional: true },
    description: { type: String, optional: true },
    restaurantId: { type: String, optional: true },
    restaurantName: { type: String, optional: true },
    restaurantAbout: { type: String, optional: true },
    chefName: { type: String, optional: true },
    chefBio: { type: String, optional: true },
    introVideo: { type: String, optional: true },
    instructionVideo: { type: String, optional: true },
    coverImage: { type: String, optional: true },
    viewCounts: { type: Number, optional: true }

  }).validator(),
  run(document) {
    return Menus.upsert({ _id: document._id }, { $set: document });
  },
});

export const removeMenu = new ValidatedMethod({
  name: 'menu.remove',
  validate: new SimpleSchema({
    _id: { type: String },
  }).validator(),
  run({ _id }) {
    Menus.remove(_id);
  },
});

export const updateViewCounts = new ValidatedMethod({
  name: 'viewCounts.update',
  validate: new SimpleSchema({
    _id: { type: String },
  }).validator(),
  run({ _id }) {
    Menus.update(_id, { $inc: {viewCounts: 1} })
  }
});

rateLimit({
  methods: [
    upsertMenu,
    removeMenu,
    updateViewCounts
  ],
  limit: 5,
  timeRange: 1000,
});
