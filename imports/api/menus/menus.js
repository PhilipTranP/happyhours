import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { Factory } from 'meteor/dburles:factory';


const Menus = new Mongo.Collection('Menus');
export default Menus;

Menus.allow({
  insert: () => false,
  update: () => false,
  remove: () => false,
});

Menus.deny({
  insert: () => true,
  update: () => true,
  remove: () => true,
});

Menus.schema = new SimpleSchema({
  isPublished: {
    type: Boolean,
    label: " to publish menu",
    defaultValue: false
  },
  title: {
    type: String,
    label: 'The name of the menu.',
  },
  description: {
    type: String,
    label: 'Some info of the menu.',
  },
  restaurantId: {
    type: String,
    label: 'restaurant Id',
  },
  restaurantName: {
    type: String,
    label: 'restaurant name',
  },
  restaurantAbout: {
    type: String,
    label: 'restaurant about',
  },

  chefName: {
    type: String,
    label: 'chef name',
  },
  chefBio: {
    type: String,
    label: 'chef name',
  },
  introVideo: {
    type: String,
    label: 'video intro',
  },
  instructionVideo: {
    type: String,
    label: 'instruction video',
  },
  coverImage: {
    type: String,
    label: 'cover image',
  },
  viewCounts: {
    type: Number,
    defaultValue: 0,
    label: 'view counts'
  },
  createdAt: {
    type: Date,
    autoValue: function() {
      if (this.isInsert) {
        return new Date();
      } else if (this.isUpsert) {
        return {$setOnInsert: new Date()};
      } else {
        this.unset();  // Prevent user from supplying their own value
      }
    }
  }

});

Menus.attachSchema(Menus.schema);

Factory.define('document', Menus, {
  isPublished: () => false,
  title: () => 'Factory Title',
  description: () => 'Factory Desciption',
  restaurantId: () => 'Factory Restaurant Id',
  restaurantName: () => 'Factory Restaurant Name',
  restaurantAbout: ()=> 'Factory About Restaurant',
  chefName: () => 'Factory Chef Name',
  chefBio: () => 'Factory Chef Bio',
  introVideo: () => 'Factory Menu Intro Video',
  instructionVideo: () => 'Factory Menu Intruction Video',
  coverImage: () => 'Factory Cover Image',
  viewCounts: () => 0
});
