import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import Menus from '../menus';


Meteor.publish('menus.list', () => Menus.find());


Meteor.publish('menus.view', (_id) => {
  check(_id, String);
  return Menus.find(_id);
});

Meteor.publish('menus.topMenu', () =>{
  return Menus.find({}, {limit: 1, sort: {viewCounts: -1}})
})

Meteor.publish('menus.search', (searchTerm) => {
  check(searchTerm, Match.OneOf(String, null, undefined));

  let query = {};
  const projection = { limit: 20, sort: {createdAt: -1} };

  if (searchTerm) {
    const regex = new RegExp(searchTerm, 'i');

    query = {
      $or: [
        { restaurantName: regex }
      ],
    };

    projection.limit = 100;
  }

  return Menus.find(query, projection);
})
