import React, { Component } from 'react';
import MenuEditor from '../components/MenuEditor.js';



class NewMenu extends Component {

  render() {

    const { restaurantInfo } = this.props

    if(!restaurantInfo){return<div>loading...</div>}


    return (
      <div className="NewDocument">
        <h4 className="page-header">Add a new menu for the restaurant</h4>
        <MenuEditor restaurantInfo={restaurantInfo} />
      </div>
    )
  }
}

NewMenu.propTypes = {
  restaurantInfo: React.PropTypes.object,
};
export default NewMenu;
