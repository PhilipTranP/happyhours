import React, { Component } from 'react'
import { Link } from 'react-router'

export default class ViewMenuButton extends Component {
  render() {
    return (
      <p>
         <Link to="/menu-details" className="btn btn-success" href="#" role="button">
           View Menu
         </Link>
      </p>
    )
  }
}
