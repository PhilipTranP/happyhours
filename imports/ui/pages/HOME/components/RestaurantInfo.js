import React, { Component , PropTypes } from 'react';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import { Jumbotron, Row, Col } from 'react-bootstrap';
import { browserHistory, Link } from 'react-router';
import Loading from '../../../common/Loading';
import YouTube from 'react-youtube';
import _ from 'lodash';


import Menus from '../../../../api/menus/menus.js';
import { updateViewCounts } from '../../../../api/menus/methods.js'



    const handleClickData = (_id) => {
      Meteor.call('viewCounts.update', {_id})
    }


class RestaurantInfo extends Component {
  constructor(props){
    super(props)
    this.state = {
      menuId: null,
    }

  }




  renderMenus(){
    if(!this.props.menus){return <div style={{marginTop: "80px"}}><Loading /></div>}
      return this.props.menus.map(menu =>{
        return(
          <Jumbotron className="menu-wrapper" key={menu._id}>
            <div className="menu-info text-center ">
              <div>
                <img className="img-responsive" src={`https://source.unsplash.com/${menu.coverImage}/960x350`}/>
              </div>

              <h2>{ menu.restaurantName }</h2>
              <h4 style={{fontWeight: "300"}}>{ menu.restaurantAbout }</h4>

              <h4 style={{ fontWeight: '300', marginTop: '30px' }}>
                Featured Chef
                <span style={{fontWeight: '400' }}> <a> { menu.chefName } </a></span>
                Serving
                <span style={{fontWeight: '400' }}> <a>{ menu.title }</a></span>
              </h4>
            </div>

            <div style={{ paddingTop: "30px"}}>
              <div className="embed-responsive embed-responsive-16by9" >
                  <YouTube videoId={menu.introVideo} />
              </div>
            </div>
            <div className="menu-info text-center" >
               <div className="menu-info text-center" style={{ paddingTop: "10px" }}>
                   <h4 style={{fontWeight: "300"}}>{ menu.description }</h4>
               </div>

               <Link to={`/menus/${menu._id}`}><button style={{ marginTop: "10px"}} onClick={()=>handleClickData(menu._id)} className="btn btn-default" href="#" role="button"> <i className="fa fa-eye" aria-hidden="true" style={{marginRight: "8px", color: "green"}}></i>
                  VIEW MENU
               </button></Link>

           </div>

         </Jumbotron>
        )
      })
  }

  render() {

    const handleNav = () => {
        this.setState({menuId: menu._id})
        browserHistory.push(`restaurants/menus/${this.state.menuId}`)
      }
      return (
        <div>
          {this.renderMenus()}
        </div>
      )

  }
}

// export default RestaurantInfo


export default createContainer(()=>{
  Meteor.subscribe('menus.list')
  return { menus: Menus.find({}).fetch()}
}, RestaurantInfo)
