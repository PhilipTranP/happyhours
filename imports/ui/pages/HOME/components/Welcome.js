import React, { Component } from 'react'
import { Jumbotron, Row, Col } from 'react-bootstrap';

export default class Welcome extends Component {
  render() {
    return (
      <Jumbotron className="text-center" style={{ marginTop: "20px" }}>
        <h2>Welcome to Deliver Happy Hours: 2-5pm.com</h2>
        <h4 style={{fontWeight: "300"}}>While our competitors deliver frozen foods and promote centralized farming, we deliver fresh local produce with selected ingredients and cooking instructions from your favorite local restaurant chefs who buy from local farmers.
        </h4>
        <p style={ { fontSize: '16px', color: '#aaa' } }><a href="#">Read more about us...</a></p>
      </Jumbotron>
    )
  }
}
