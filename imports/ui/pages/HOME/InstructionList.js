import React, { Component } from 'react'
import StackGrid from 'react-stack-grid'
import { ReactiveVar } from 'meteor/reactive-var';
import { Jumbotron, FormControl} from 'react-bootstrap'
import { Link } from 'react-router'
import YouTube from 'react-youtube';
import { createContainer} from 'meteor/react-meteor-data'
import Instructions from '../../../api/instructions/instructions.js'
import { MyCollectionsMethod } from '../../../api/my-collections/methods.js'
import { UpdateSubscribers } from '../../../api/instructions/methods.js'

import Loading from '../../common/Loading'

const searchQuery = new ReactiveVar(null)

class InstructionList extends Component {

  constructor(props){
    super(props)
    this.state = {
      searchTerm: '',
      showInstructionVideo: false,
      startTime: null,
      endTime: null,
      videoId: null,
      player: null,
      instructionId: null,
      instructionTitle:'',
      instructionText:'',
      hover: false
    }
    this.handleSearch = this.handleSearch.bind(this)
    this.onToStartTime = this.onToStartTime.bind(this)
    this.first17Words = this.first17Words.bind(this)
    this.handleMouseIn = this.handleMouseIn.bind(this)
    this.handleMouseOut = this.handleMouseOut.bind(this)
  }

  handleSearch(event){
    const searchTerm = event.target.value
    this.setState({ searchTerm})
    searchQuery.set(searchTerm)
  }
  handleMouseIn() {
    this.setState({ hover: true })
  }

  handleMouseOut() {
    this.setState({ hover: false })
  }

  onToStartTime(event) {
    this.setState({
      player: event.target.playVideo(),
    });
      this.state.player.seekTo(this.state.startTime, true);
      if(YouTube.PlayerState=1){
        const loopStart = () => {this.state.player.seekTo(this.state.startTime, true)}
        return setTimeout(loopStart, (this.state.endTime - this.state.startTime)*1000)
      }
  }

  first17Words(words){
    return words.split(' ').slice(0,17).join(' ') + " ..."
  }

  renderInstructions(){
    handleAddInstruction = (event, id) => {
      Meteor.call('update-subscribers', id, (error, response) =>{
        if (error) {
          Bert.alert(error.reason, 'danger');
        } else {
          Bert.alert("Added the intructions to my collections success", 'success');
          }
      })
    }
    let width = window.innerWidth
    if(!this.props.instructions){return<div style={{marginTop: "80px"}}><Loading /></div>}
    if(this.props.instructions.length > 1){
      return this.props.instructions.map(instruction => {
      return (
        <div key={instruction._id} style={{margin: "0px"}}>
          <Jumbotron style={{padding: "0px", margin: "0px", marginRight: width>789 ? "5px" : "0px", marginTop: "-25px"}}>
             <h3 style={{padding: "15px", paddingTop: "25px"}}>{instruction.title} </h3>
             <a href="#" onClick={()=>this.setState({videoId: instruction.instructionVideo,
             startTime: instruction.startTime,
             endTime: instruction.endTime,
             instructionId: instruction._id,
             instructionTitle: instruction.title,
             instructionText: instruction.instructions,
             showInstructionVideo: !this.state.showInstructionVideo})}><img className="img-responsive"
               style={{display: "block",
               marginTop: "auto",
               marginLeft: "auto",
               marginRight: "auto",
              }} src={`http://media2.giphy.com/media/${instruction.image}/200.gif`} alt="" width="140%"/></a>
            <h4 style={{fontWeight: "400", paddingLeft: "15px", paddingRight: "15px"}}>{instruction.description}  <a href="#" onClick={()=>handleAddInstruction(event, instruction._id)}> <small>Add to My Collections</small></a></h4>
             <a href="#"  onClick={ ()=>{this.setState({ videoId:               instruction.instructionVideo,
              startTime: instruction.startTime,
              endTime: instruction.endTime,
              instructionId: instruction._id,
              instructionText: instruction.instructions,
              instructionTitle: instruction.title,
              showInstructionVideo: !this.state.showInstructionVideo})} } >
             <h4 style={{fontWeight: "300", paddingLeft: "15px", paddingRight: "15px", paddingBottom: "30px"}}> {this.first17Words(instruction.instructions)}</h4>
             </a>
          </Jumbotron>
        </div>
      )
    })
    } else {
      return(<Jumbotron><h2> Sorry! No results for <strong>{ this.state.searchTerm }</strong> </h2></Jumbotron>)
    }
  }
  render() {
    let width = window.innerWidth
    const tooltipStyle = {
     display: this.state.hover ? 'inline-block' : 'none'
    }

    addToCollections = () => {
       var instructionId = this.state.instructionId
       Meteor.call('update-subscribers', instructionId, (error, response) =>{
         if (error) {
           Bert.alert(error.reason, 'danger');
         } else {
           Bert.alert("Added the intructions to my collections success", 'success');
           }
       })
    }
    return (
      <div>
        { this.state.showInstructionVideo
         ?
            <div style={{ position: "absolute", top: "50px", zIndex:"100", backgroundColor: "#cccdce", width: width > 769 ? "65%" : "100%", marginLeft: width > 769 ? "12%" : "0px"}}>

                <div style={{marginTop: "5px", marginRight: "20px"}}>
                  <a href="#" onClick={()=>this.setState({showInstructionVideo: !this.state.showInstructionVideo})}><i className="fa fa-lg fa-window-close pull-right" aria-hidden="true" style={{width:"30px", backgroundColor: "red", color: "#cccdce", fontSize: "32px"}}></i></a>
                </div>

                <div style={{backgroundColor: "#000", color: "#FFF"}}>
                  <h4 style={{padding: "30px", paddingTop: "20px", paddingBottom:"5px"}}>{this.state.instructionTitle}</h4>

                  <div className="embed-responsive embed-responsive-16by9">

                     <YouTube videoId={this.state.videoId}    onReady={this.onToStartTime}/>

                   </div>
                   <h4 style={{paddingTop:"10px", paddingBottom: "20px", paddingLeft: "30px", paddingRight: "10px", fontWeight:"400"}}>
                      {this.state.instructionText} <a href="#" onClick={()=>addToCollections()} onMouseOver={this.handleMouseIn} onMouseOut={this.handleMouseOut}> <i className="fa fa-plus-circle" aria-hidden="true" style={{width:"30px", backgroundColor: "#000", marginLeft: "20px", color: "green", fontSize: "24px"}}></i><p style={tooltipStyle}> Add to My Collections</p></a>
                   </h4>
                </div>
            </div>
         : null
        }

        <div style={{opacity: this.state.showInstructionVideo ? "0.3" : "1", marginTop: "80px", width: width > 769 ? "90%" : "96%", marginLeft: width > 769 ? "5%" : "2%", marginBottom: "10px"}}>
          <div style={{marginTop: "20px", marginLeft: width > 769 ? "10%" : "0", marginRight: width > 769 ? "10%" : "0", marginBottom: "10px"}}>
             <FormControl
               style={{fontSize: "24px", height: "60px", paddingLeft: "20px"}}
               type="search"
               autoFocus
               onKeyUp={ this.handleSearch }
               placeholder="Type to search recipes"
               className="Search"
             />
             <hr />
          </div>
          { width < 330
            ?
              <StackGrid
                columnWidth= {270}
              >
                {this.renderInstructions()}
              </StackGrid>
            :
              <StackGrid
                columnWidth= {320}
              >
                {this.renderInstructions()}
              </StackGrid>
          }
       </div>
      </div>
    );
  }
}

export default createContainer(() => {
    Meteor.subscribe('instructions.search', searchQuery.get())
    return {instructions: Instructions.find({}, {sort: {title: 1}}).fetch()}
}, InstructionList)
