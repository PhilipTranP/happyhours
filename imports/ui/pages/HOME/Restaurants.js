import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'
import { ReactiveVar } from 'meteor/reactive-var'
import { Jumbotron, Button, FormControl } from 'react-bootstrap';
import { browserHistory, Link } from 'react-router';
import YouTube from 'react-youtube';

import Menus from '../../../api/menus/menus.js'
import { Loading } from '../../common/Loading.js'


const searchQuery = new ReactiveVar(null)

class Restaurants extends Component {
  constructor(props){
    super(props)
    this.state = {
      searchTerm: null
    }
    this.handleSearch = this.handleSearch.bind(this)
    this.handleScroll = this.handleScroll.bind(this)
  }

  componentWillMount(){
  window.addEventListener('scroll', this.handleScroll);
  }
  componentWillUnmount(){
    window.removeEventListener('scroll', this.handleScroll);
  }
  componentDidMount(){
    this.setState({searchTerm: this.props.location.query.q})
    searchQuery.set(this.props.location.query.q)
  }
  handleScroll() {
     this.setState({scrollTop: $(window).scrollTop()});
  }

  handleSearch(event){
    const searchTerm = event.target.value
    this.setState({ searchTerm})
    searchQuery.set(searchTerm)
  }

  renderSearchResults(){
    if(!this.props.restaurants){return<div style={{marginTop: "80px"}}><Loading /></div>}
    const { restaurants } = this.props
    if(restaurants.length > 0){
     return restaurants.map(menu => {
      return (
        <div key={menu._id}>
          <Jumbotron className="menu-wrapper" >
            <div className="menu-info text-center ">

              <h2>{ menu.restaurantName }</h2>
              <div style={{marginTop: "20px", marginBottom: "20px"}}>
                <img className="img-responsive" src={`https://source.unsplash.com/${menu.coverImage}/1200x500`}/>
              </div>
              <h4 style={{fontWeight: "300"}}>{ menu.restaurantAbout }</h4>

              <h4 style={{ fontWeight: '300', marginTop: '30px' }}>
                Features chef
                <span style={{fontWeight: '400' }}> { menu.chefName } </span>
                serving
                <span style={{fontWeight: '400' }}> <a>{ menu.title }</a></span> Menu
              </h4>
            </div>

            <div style={{ paddingTop: "20px"}}>
              <div className="embed-responsive embed-responsive-16by9" >
                  <YouTube videoId={menu.introVideo} />
              </div>
            </div>
            <div className="menu-info text-center" >
               <div className="menu-info text-center" style={{ paddingTop: "10px" }}>
                   <h4 style={{fontWeight: "300"}}>{ menu.description }</h4>
               </div>

               <Link to={`/menus/${menu._id}`}><button style={{ marginTop: "10px"}} onClick={()=>handleClickData(menu._id)} className="btn btn-default" href="#" role="button"><i className="fa fa-eye" aria-hidden="true" style={{marginRight: "8px", color: "green"}}></i> 
                 View {menu.title} Menu
               </button></Link>

           </div>
          </Jumbotron>
          <div style={{paddingTop: "10px"}}>
            <a href="#"><div className="pull-right" onClick={()=>this.handleScroll()} style={{paddingBottom: "0px"}}>GO TO TOP</div></a>
             <hr />
           </div>
         </div>
        )
      })
     } else {
      return(<Jumbotron><h2> Sorry! No results for <strong>{ this.state.searchTerm }</strong> </h2></Jumbotron>)
     }
  }

  render(){
    const { q } = this.props.location.query

    let width = window.innerWidth
    return (
        <div style={{backgroundColor: "#FFF", paddingTop: "60px", width: width > 769 ? "80%" : "96%", marginLeft: width > 769 ? "10%" : "2%", marginBottom: "50px"}}>
          <div style={{marginTop: "20px", marginLeft: width > 769 ? "20%" : "2%"}} className="text-center">
             <form>
               <FormControl
                 style={{fontSize: "24px", height: "60px", paddingLeft: "20px", width: width > 769 ? "70%" : "98%"}}
                 type="search"
                 autoFocus
                 onKeyUp={ this.handleSearch }
                 placeholder="Type to search for restaurants or menus"
                 className="Search"
                />
              </form>
          </div>
          <hr />
          {this.renderSearchResults()}

        </div>
    )
  }
}

export default createContainer(()=>{
  Meteor.subscribe('menus.search', searchQuery.get())
  return { restaurants: Menus.find({}).fetch() }
}, Restaurants)
