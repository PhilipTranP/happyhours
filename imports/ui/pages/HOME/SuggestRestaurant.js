import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'
import { Meteor } from 'meteor/meteor';
import { browserHistory } from 'react-router'
import Inquiries from '../../../api/inquiries/inquiries.js'
import { sendApproveEmail } from '../../../api/inquiries/methods.js'


class SuggestRestaurant extends Component {
  constructor(props){
    super(props)
    this.state={
    }
    this.handleFormSubmit = this.handleFormSubmit.bind(this)
  }
  handleFormSubmit(event){
    event.preventDefault()
    const upsert = {
       _id: new Meteor.Collection.ObjectID()._str,
       firstName: this.refs.name.value,
       email: this.refs.contact.value,
       message: this.refs.message.value
     }

     const isNotEmpty = () => {
       if(upsert.firstName && upsert.email && upsert.message != ''){
         return true
       }
        Bert.alert("Please fill in all fields", 'danger');
        return false
     }

     const confirmation = 'Thank you for your suggestion.'
     const emailInput = upsert.email
     const nameInput = upsert.firstName
     const messageInput = upsert.message

     if(isNotEmpty(nameInput) && isNotEmpty(emailInput) && isNotEmpty(messageInput)){

       Meteor.call('inquiry.upsert', upsert , (error, response) =>{
         if (error) {
           Bert.alert(error.reason, 'danger');
         } else {
           Meteor.call('sendApproveEmailMethod', (error, response) =>{
             if (error) {
               Bert.alert(error.reason, 'danger');
             } else {
               Bert.alert(confirmation, "success")
               }
            })
           this.refs.name.value = ''
           this.refs.contact.value = ''
           this.refs.message.value = ''
           browserHistory.push('/news')
         }
       })
     }
  }
  render(){
    let width= window.innerWidth
    return(
      <div style={{
        background: "url(https://source.unsplash.com/slyyeNC6qzY/)",
        backgroundSize: "cover",
        position: "fixed",
        backgroundPosition: width>980 ? "center" : "70%",
        top: "0",
        left: "0",
        width: "100%",
        height: "100%",
        zIndex: "-9999",
        opacity: "1"
      }}>

              <form className="form-horizontal" style={{background: "none", marginTop: "290px", width: width>980 ? "60%" : "90%", marginLeft: width>980 ? "29%" : "5%"}} onSubmit={this.handleFormSubmit}>
                  <div className = "form-group">


                      <div className="col-md-10" style={{marginBottom: "10px"}}>
                          <input type = "text" className="form-control transparent-input" ref="name" placeholder="Your name" />
                      </div>
                      <div className="col-md-10" style={{marginBottom: "10px"}}>
                          <input type = "text" className="form-control transparent-input" ref="contact" placeholder="Your email, phone or both" />
                      </div>

                      <div className = "col-md-10">
                          <textarea rows="5" type = "text" className="form-control  transparent-input" ref="message" placeholder="Restaurant's name, address, contacts, or any info you would like us to know about the restaurant" />
                      </div>
                  </div>
                  <div className="form-group">
                      <div className="col-xs-offset-2 col-xs-5">
                          <button type="submit" className="btn btn-success" style={{marginLeft: "auto", marginRight: "auto", display: "table"}}> {width>980 ? "SUGGEST MY LOCAL RESTAURANT TO 2-5PM.COM" : "SUGGEST LOCAL RESTAURANT"}</button>
                      </div>
                  </div>
               </form>

      </div>
    )
  }
}

export default SuggestRestaurant
