import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { Navbar, NavItem, Jumbotron, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router';
import YouTube from 'react-youtube';
import Inquiries from '../../../api/inquiries/inquiries.js';
import handleLogin from '../../../modules/loginMembers';
import { sendApproveEmail } from '../../../api/inquiries/methods.js'


// import AddDocumentModalForm from '../../components/modals/AddDocument'

class Landing extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startTime: null,
      endTime: null,
      videoId: null,
      player: null,
      showRequestForm: false,
      showRequestFormSmallScreen: false,
      instructionText:'',
      openBannerImage: true,
      openInstructionVideo: false,
      displayBodySmallScreen: true,
      openInstructionVideoSmallScreen: false
    };
    this.onToStartTime = this.onToStartTime.bind(this)
  }

  componentDidMount() {
    handleLogin({ component: this });
  }

  handleSubmit(event) {
    event.preventDefault();
  }

  onToStartTime(event) {
    this.setState({
      player: event.target.playVideo(),
    });
      this.state.player.seekTo(this.state.startTime, true);
      if(YouTube.PlayerState=1){
        const loopStart = () => {this.state.player.seekTo(this.state.startTime, true)}
        return setTimeout(loopStart, (this.state.endTime - this.state.startTime)*1000)
      }
  }

  render() {

    handleRequestFormSubmit = () => {



      const upsert = {
         _id: new Meteor.Collection.ObjectID()._str,
         firstName: this.refs.requestFirstName.value,
         email: this.refs.requestEmail.value,
         message: this.refs.requestMessage.value
       }

       const isNotEmpty = () => {
         if(upsert.firstName && upsert.email && upsert.message != ''){
           return true
         }
          Bert.alert("Please fill in all fields", 'danger');
          return false
       }

       const isValidEmail = (value) =>{
         let filter = /^[a-z][a-zA-Z0-9_.]*(\.[a-zA-Z][a-zA-Z0-9_.]*)?@[a-z][a-zA-Z-0-9]*\.[a-z]+(\.[a-z]+)?$/
         if(filter.test(value)){
           return true
         }
         Bert.alert("Please provide a valid email", 'danger');
         return false
       }

       const confirmation = 'Request Sent. Thank you!'
       const emailInput = upsert.email
       const nameInput = upsert.firstName.toUpperCase()
       const messageInput = upsert.message

       if(isNotEmpty(nameInput) && isNotEmpty(emailInput) && isNotEmpty(messageInput) && isValidEmail(emailInput)){

         Meteor.call('inquiry.upsert', upsert , (error, response) =>{
           if (error) {
             Bert.alert(error.reason, 'danger');
           } else {
             Meteor.call('sendApproveEmailMethod', (error, response) =>{
               if (error) {
                 Bert.alert(error.reason, 'danger');
               } else {
                 Bert.alert("Thank you for your request", "success")
                 }
              })
             this.refs.requestFirstName.value = ''
             this.refs.requestEmail.value = ''
             this.refs.requestMessage.value = ''
             this.setState({showRequestForm: !this.state.showRequestForm})
           }
         })
       }
    }

     let width = window.innerWidth
     if(width>780){
       return (
         <div >
             <Navbar className="navbar-fixed-top" style={{zIndex:"50"}}>
               <Navbar.Header>
                 <Navbar.Brand className = "navbar-brand">
                   <Link to="/" style={ { width: "100%", marginLeft: "-25px" }}>
                     <img src="/assets/img/2to5.png" style={ { width: "30%", display: "inline-block", float: "left", marginTop: "-15px", marginLeft: "-17px" }} />
                     <p  style={ { float: "left", paddingTop: "-25px", marginLeft: "15px", display: "inline-block", fontWeight: "300", fontSize: "20px"}}> Deliver Happy Hours</p></Link>
                    {/*
                      <span>
                           <Link to="/news"><p className="pull-right" style={{marginLeft: "50px", marginTop: "3px", fontSize: "12px"}}>NEWS</p></Link>
                      </span>
                    */}

                 </Navbar.Brand>

               </Navbar.Header>
             </Navbar>

               {/*Request Form*/}

               { this.state.showRequestForm
                 ?
                   <div style={{ position: "absolute", top: "50px", left: "33%", width: "34%", zIndex:"100", backgroundColor: "#cccdce"}}>

                       <div style={{marginTop: "20px", marginRight: "20px"}}>
                         <a href="/" onClick={()=>this.setState({showRequestForm: !this.state.showRequestForm})}><i className="fa fa-lg fa-window-close pull-right" aria-hidden="true" style={{width:"30px", backgroundColor: "green", color: "#cccdce", fontSize: "32px"}}></i></a>
                       </div>

                       <div style={{padding: "30px", paddingTop: "2px", paddingBottom:"5px"}}>
                         <h4>REQUEST FORM</h4>
                         <hr />
                         <div className="form-group">
                            <label> First Name </label>
                            <input type="text"  autoFocus name="requestFirstName" ref="requestFirstName" className="form-control" />
                         </div>
                         <div className="form-group">
                            <label> Email </label>
                            <input type="text" name="email" ref="requestEmail" className="form-control" />
                         </div>
                         <div className="form-group">
                            <label> Message </label>
                            <textarea rows="3" ref="requestMessage" className="form-control" />
                         </div>
                         <div style={{paddingBottom: "30px"}}>
                            <button className="btn btn-default pull-right" onClick={()=>  handleRequestFormSubmit()}><i className="fa fa-paper-plane" aria-hidden="true" style={{marginRight: "5px", color: "green"}}></i> SEND REQUEST</button>
                         </div>
                         <hr />
                       </div>
                   </div>

                 : null

               }


               {/*Render body section for large screen*/}

               <div style={{opacity: this.state.showRequestForm ? "0.1" : "1"}}>

                 {/* Start the left section */}

                 <div style={{position: "absolute", top: "40px", left: "60px", bottom: "0px", float: "left", width: "30%", height: "800px", paddingRight: "10px", zIndex: '-100' }}>

                    <div style={{marginTop: "120px", marginBottom:"40px", marginLeft:"10px"}}>

                            <div style={{paddingBottom: "20px", paddingRight:"10px"}}>
                                <h4>MEMBERS LOGIN</h4>
                                  <h4 style={{fontWeight: "300"}}>New to 2-5pm.com?  <a href="#" onClick={()=>this.setState({showRequestForm: !this.state.showRequestForm})}> Join 2-5pm.com</a></h4>
                                <hr />
                                  <form
                                    ref={ form => (this.loginForm = form) }
                                    className="login"
                                    onSubmit={ this.handleSubmit }
                                  >
                                    <div className="form-group">
                                       <label> Email </label>
                                       <input autoFocus type="email"
                                       ref="emailAddress"
                                       name="emailAddress"
                                       placeholder="Email Address" className="form-control" />
                                    </div>

                                    <div className="form-group">
                                       <label> Password </label>
                                       <input type="password"
                                       ref="password"
                                       name="password"
                                       placeholder="Password" className="form-control" />
                                    </div>
                                     <button type="submit" className="btn btn-default pull-right"><i className="fa fa-sign-in" aria-hidden="true" style={{marginRight: "5px", color: "green"}}></i> LOGIN</button>

                                     <h5 style={{fontWeight: "300"}}>Problem with login? <Link to="/recover-password"> Recover password</Link></h5>

                                   </form>
                              </div>

                            {/*Click Image To Open The Intro Video */}

                            <div onClick={()=> this.setState({
                                openInstructionVideo: !this.state.openInstructionVideo,
                                openBannerImage: !this.state.openBannerImage
                              })} style={{paddingRight:"10px"}}>
                              <hr />
                                 <a href="#"><img className="img-responsive"  src="https://source.unsplash.com/-pyjPVh77uE" />
                                 <h5 style={{fontWeight:"300"}}>Click image to play video</h5>
                                 </a>

                             </div>
                          </div>
                     </div>


                 {/*Start the right section*/}

                 <div style={{position: "absolute", top: "50px", left: "39%", bottom: "0px", float: "left",  width: "60%", height: "800px", zIndex: '-100' }}>

                   {/*Open Instruction Video*/}

                   <div style={{marginBottom: "40px"}}>
                      {
                          this.state.openInstructionVideo

                       ?    <div style={{ marginTop: "40px"}}>

                              <div className="embed-responsive embed-responsive-16by9">

                                 <YouTube videoId="iErzbFtgSuY"    onReady={this.onToStartTime}/>

                               </div>
                               <h4 style={{paddingTop:"20px", fontWeight:"400"}}>
                                   Your complete source for natural foods, body care, organically grown produce, and the largest selection of vitamins & herbal remedies on the San Francisco
                               </h4>
                               <hr />
                            </div>

                       : null
                     }
                   </div>
                      <div style={{ marginTop: "45px", marginRight:"20px"}}>
                       <h2>Welcome to Deliver Happy Hours: 2-5pm.com</h2>
                      </div>

                      {
                        this.state.openBannerImage

                        ?
                           <div style={{ marginTop: "30px", paddingRight:"40px"}}>
                             <img className="img-responsive" src="https://source.unsplash.com/jpkfc5_d-DI" />
                           </div>

                        : null
                      }

                      <div style={{ marginTop: "30px", paddingBottom:"30px", marginRight:"20px" }}>
                        <h3 style={{fontWeight: "400"}}>Eat fresh and support local farming one bite at a time!</h3>
                        <h3 style={{fontWeight: "300"}}>While our competitors deliver frozen foods and promote centralized farming, we deliver fresh local produce with selected ingredients and cooking instructions from your favorite local restaurants{"\'"} chefs who buy from local farmers.
                        </h3>

                        <p style={{ fontSize: '16px', color: '#aaa' }}>
                          <a href="/news">Read happy news</a> or
                          <a href="#" onClick={()=>this.setState({showRequestForm: !this.state.showRequestForm})}> request more info</a>
                        </p>
                      </div>
                  </div>

             </div>
         </div>
       )
     } else {
       return (
       <div>
           <Navbar className="navbar-fixed-top" style={{zIndex:"50"}}>
             <Navbar.Header>
               <Navbar.Brand className = "navbar-brand">
                 <Link to="/" style={ { width: "100%" }}>
                   <img src="/assets/img/2to5.png" style={{ width: "20%", display: "inline-block", float: "left", marginTop: width < 680 ? "0px" : "-10px", marginLeft: "-5px" }} />
                     <p  style={{ marginLeft: "10px", display: "inline-block", float: "left", fontWeight: "300", fontSize: "16px"}}> Deliver Happy Hours</p></Link>
                   <span><Link to="/news"><p style={{fontWeight:"300", paddingTop:"3px", fontSize: "12px", paddingRight: "8px"}}    className="pull-right">NEWS</p></Link></span>
               </Navbar.Brand>
             </Navbar.Header>
           </Navbar>
          {/*Show Request Form for small screens*/}
          {
            this.state.showRequestFormSmallScreen
            ?
              <div style={{ position: "absolute", top: "52px", left: "10%", width: "80%", zIndex:"100", backgroundColor: "#cccdce"}}>
                  <div style={{marginTop: "20px", marginRight: "20px"}}>
                    <Link to={"/"} onClick={()=>this.setState({showRequestFormSmallScreen: !this.state.showRequestFormSmallScreen, displayBodySmallScreen: !this.state.displayBodySmallScreen})}><i className="fa fa-lg fa-window-close pull-right" aria-hidden="true" style={{width:"20px", backgroundColor: "#FFF", color: "#cccdce", fontSize: "24px"}}></i></Link>
                  </div>
                  <div style={{padding: "30px", paddingTop: "2px", paddingBottom:"5px"}}>
                    <h4>REQUEST FORM</h4>
                    <hr />
                    <div className="form-group">
                       <label> First Name </label>
                       <input type="text"  autoFocus name="requestFirstName" ref="requestFirstName" className="form-control" />
                    </div>
                    <div className="form-group">
                       <label> Email </label>
                       <input type="text" name="email" ref="requestEmail" className="form-control" />
                    </div>
                    <div className="form-group">
                       <label> Message </label>
                       <textarea rows="3" ref="requestMessage" className="form-control" />
                    </div>
                    <div style={{paddingBottom: "30px"}}>
                       <button className="btn btn-default pull-right" onClick={()=>  handleRequestFormSubmit()}><i className="fa fa-paper-plane" aria-hidden="true" style={{marginRight: "5px", color: "green"}}></i> SEND</button>
                    </div>
                    <hr />
                  </div>
              </div>
            : null
          }
         { this.state.displayBodySmallScreen
           ?
              <div style={{marginTop:"50px", marginLeft: "30px", marginRight: "30px", marginBottom: "100px"}}>
                 {/*Welcome Section*/}
                 <div>
                   <div style={{ paddingTop: "15px"}}>
                    <h3>Welcome to Deliver Happy Hours: 2-5pm.com</h3>
                   </div>
                   {/*Open Instruction Video*/}
                   <div style={{marginBottom: "20px"}}>
                      {
                          this.state.openInstructionVideoSmallScreen
                       ?    <div style={{ marginTop: "20px"}}>
                              <div className="embed-responsive embed-responsive-16by9">
                                 <YouTube videoId="iErzbFtgSuY"    onReady={this.onToStartTime}/>
                               </div>
                               <h4 style={{paddingTop:"20px", fontWeight:"400"}}>
                                   Your complete source for natural foods, body care, organically grown produce, and the largest selection of vitamins & herbal remedies on the San Francisco
                               </h4>
                               <hr />
                            </div>
                       : null
                     }
                   </div>
                    <div style={{ marginTop: "10px"}}>
                      <img className="img-responsive" src="https://source.unsplash.com/jpkfc5_d-DI" />
                    </div>
                   <div style={{ marginTop: "20px" }}>
                     <h4 style={{fontWeight: "400"}}>Eat fresh and support local farming one bite at a time!</h4>
                     <h4 style={{fontWeight: "300"}}>While our competitors deliver frozen foods and promote centralized farming, we deliver fresh local produce with selected ingredients and cooking instructions from your favorite local restaurants{"\'"} chefs who buy from local farmers.
                     </h4>
                     <p style={ { fontSize: '16px', color: '#aaa' } }>
                       <Link to="/news">Read our blog</Link> or
                       <a href="#" onClick={()=>this.setState({showRequestFormSmallScreen: !this.state.showRequestFormSmallScreen, displayBodySmallScreen: !this.state.displayBodySmallScreen})}> request more info </a></p>
                   </div>
                 </div>
                 {/*Click Image To Open The Intro Video */}
                 <div onClick={()=>this.setState({
                    openInstructionVideoSmallScreen: !this.state.openInstructionVideoSmallScreen,
                  })}>
                  <hr />
                     <a href="#"><img className="img-responsive"  src="https://source.unsplash.com/-pyjPVh77uE" />
                     <h5 style={{fontWeight:"300"}}>Click image to play video</h5>
                     </a>
                     <hr />
                 </div>
                 {/*Signin Form*/}
                 <div style={{marginTop:"30px", paddingBottom: "20px"}}>
                     <h4>MEMBERS LOGIN</h4>
                       <h5 style={{fontWeight: "300"}}>New to 2-5pm.com? <a href="#" onClick={()=>this.setState({showRequestFormSmallScreen: !this.state.showRequestFormSmallScreen, displayBodySmallScreen: !this.state.displayBodySmallScreen})}> Request a free trial membership to login</a></h5>
                     <hr />
                     <form
                       ref={ form => (this.loginForm = form) }
                       className="login"
                       onSubmit={ this.handleSubmit }
                       style={{paddingLeft:"15px", paddingRight:"15px"}}
                     >
                       <div className="form-group">
                          <label> Email </label>
                          <input autoFocus type="email"
                          ref="emailAddress"
                          name="emailAddress"
                          placeholder="Email Address" className="form-control" />
                       </div>
                       <div className="form-group">
                          <label> Password </label>
                          <input type="password"
                          ref="password"
                          name="password"
                          placeholder="Password" className="form-control" />
                       </div>
                        <button type="submit" className="btn btn-default pull-right"><i className="fa fa-sign-in" aria-hidden="true" style={{marginRight: "5px", color: "green"}}></i> LOGIN</button>
                        <h5 style={{fontWeight: "300"}}>Problem with login? <Link to="/recover-password"> Recover password</Link></h5>
                      </form>
                   </div>
              </div>
            : null
          }
             <div className="visible-md visible-lg" style={{ position: "absolute", top: "50px", left: "0", width: "100%", height:"1px", zIndex:"200", backgroundColor: "#FFF"}}>
             </div>
       </div>
       )
     }
  }
}


export default Landing
