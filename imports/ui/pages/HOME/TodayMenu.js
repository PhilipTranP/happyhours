// import React, { Component } from 'react'
// import { createContainer } from 'meteor/react-meteor-data'
//
// import Instructions from '../../../api/instructions/instructions.js'
// import Menus from '../../../api/menus/menus.js'
// import Loading from '../../common/Loading'
// import _ from 'lodash'
//
//
// class TodayMenu extends Component {
//
//   renderInstructions(){
//     if(!this.props.topMenuInstructions){return <div style={{marginTop: "80px"}}><Loading /></div>}
//     return this.props.topMenuInstructions.map(instruction=>{
//       return (
//         <div key={instruction._id}>
//             <div>
//                <h4><span>{instruction.title}</span><span className="pull-right" style={{fontWeight: "300", paddingRight:"10px"}}>
//                  $ {instruction.price}</span></h4>
//                <h4 style={{fontWeight: "300", paddingRight: "20px"}}>
//                  {instruction.description}
//                </h4>
//             </div>
//            {/*Clickable Video Thumbs Section To Open The Video Instructions */}
//             <div>
//                 <a href="#"><img className="img-responsive"  src={instruction.image} /></a>
//             </div>
//         </div>
//       )
//     })
//   }
//
//   render() {
//
//     if(!this.props.topMenu){return <div style={{marginTop: "80px"}}><Loading /></div>}
//     const topMenu = this.props.topMenu
//     const totalItem = this.props.topMenuInstructions.length
//     const totalPrice = _.sumBy(this.props.topMenuInstructions, 'price')
//
//     return(
//         <div>
//
//           {/* Menu Header Section */}
//           <div style={{paddingBottom: "20px", paddingRight:"10px"}}>
//               <h3>TODAY{"'"}S TOP MENU<span className="pull-right"><button className="btn btn-success">Order</button></span></h3>
//               <hr />
//               <h3> {topMenu.title}</h3>
//               <h4 style={{fontWeight: "300"}}>{totalItem} items <span className="pull-right">$ {totalPrice}</span></h4>
//               <div>
//                 <a href="#"><img className="img-responsive" style={ { width: "370px" }} src={topMenu.coverImage} /></a>
//               </div>
//               <h4 style={{fontWeight: "300"}}>
//                 {topMenu.description}
//               </h4>
//            </div>
//
//            {/* Social Icons */}
//
//             <div style={{paddingBottom: "30px"}}>
//               <div>
//                 <img className="pull-right" src="/assets/img/socialButton.png" width="100px" />
//                 <span className="pull-left">
//                   <h5 style={{fontWeight: "300", color: "grey"}}>2125 Locals ordered</h5>
//                 </span>
//                </div>
//              </div>
//
//            <hr />
//
//            {this.renderInstructions()}
//         </div>
//     )
//   }
// }
//
//
//
// export default createContainer((props)=>{
//   const menuId = "PFWv6BKfpTq7CR8G3"  //TODO Algo to feed dynamically
//   Meteor.subscribe('instructions.list')
//   Meteor.subscribe('menus.list')
//   return {topMenuInstructions: Instructions.find({"menuId" : menuId}).fetch(),
//           topMenu: Menus.findOne({"_id" : menuId})}
// }, TodayMenu)
