import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import { Jumbotron, Row, Col, Button, FormControl } from 'react-bootstrap';
import { browserHistory, Link } from 'react-router';
import YouTube from 'react-youtube';
import _ from 'lodash';


import Welcome from './components/Welcome'
import RestaurantInfo from './components/RestaurantInfo'
import Documents from '../../../api/documents/documents.js';
import Menus from '../../../api/menus/menus.js';
import Instructions from '../../../api/instructions/instructions.js'
// import TodayMenu from './TodayMenu';
import Loading from '../../common/Loading'



class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTerm: null,
      startTime: null,
      endTime: null,
      videoId: null,
      player: null,
      instructionText:'',
      openInstructionVideo: false,

    };
    this.onToStartTime = this.onToStartTime.bind(this)
    this.first17Words = this.first17Words.bind(this)
    this.submitSearch = this.submitSearch.bind(this)
    this.handleSearch = this.handleSearch.bind(this)
  }

  submitSearch(event) {
    event.preventDefault()
    let url = 'restaurants?q=' + encodeURI(this.state.searchTerm)
    browserHistory.push(url)
  }

  handleSearch(event){
    const searchTerm = event.target.value
    this.setState({ searchTerm})
    let url = 'restaurants?q=' + encodeURI(this.state.searchTerm)
  }

  onToStartTime(event) {
    this.setState({
      player: event.target.playVideo(),
    });
      this.state.player.seekTo(this.state.startTime, true);
      if(YouTube.PlayerState=1){
        const loopStart = () => {this.state.player.seekTo(this.state.startTime, true)}
        return setTimeout(loopStart, (this.state.endTime - this.state.startTime)*1000)
      }
  }

  first17Words(words){
    return words.split(' ').slice(0,17).join(' ') + " ..."
  }

  renderInstructions(){
    if(!this.props.topMenuInstructions){return <div style={{marginTop: "80px"}}><Loading /></div>}
    return this.props.topMenuInstructions.map(instruction=>{
      return (
        <div key={instruction._id}>
            <div>
               <h3><span>{instruction.title}</span><span className="pull-right" style={{fontWeight: "300", paddingRight:"10px"}}>
                 $ {instruction.price}</span></h3>
            </div>
           {/*Clickable Video Thumbs Section To Open The Video Instructions */}
           <div onClick={()=>{this.setState({
               videoId: instruction.instructionVideo,
               startTime: instruction.startTime,
               endTime: instruction.endTime,
               instructionText: instruction.instructions,
               openInstructionVideo: !this.state.openInstructionVideo,
               openMenuIntroVideo: !this.state.openMenuIntroVideo,
               openBannerImage: !this.state.openBannerImage
             })}} style={{paddingRight:"10px", marginTop:"20px"}}>

              <a href="#"><img className="img-responsive"
                 style={{display: "block",
                 marginTop: "30px",
                 marginLeft: "auto",
                 marginRight: "auto" }} src={`http://media2.giphy.com/media/${instruction.image}/200.gif`} alt="" width="130%"/></a>
                 <h4>{instruction.description}</h4>
                 <a href="#"  onClick={ ()=>{this.setState({ videoId:               instruction.instructionVideo,
                  startTime: instruction.startTime,
                  endTime: instruction.endTime,
                  instructionText: instruction.instructions,
                  openInstructionVideo: !this.state.openInstructionVideo,
                  openMenuIntroVideo: !this.state.openMenuIntroVideo,
                  openBannerImage: !this.state.openBannerImage})} } >
                 <h4 style={{fontWeight: "300"}}>{this.first17Words(instruction.instructions)}</h4></a>
                <hr />
            </div>

        </div>
      )
    })
  }


    render(){
      console.log(this.state.searchTerm)
      if(!this.props.menu){return <div style={{marginTop: "80px"}}><Loading /></div>}
      const topMenu = this.props.menu
      const totalItem = this.props.topMenuInstructions.length
      const totalPrice = _.sumBy(this.props.topMenuInstructions, 'price')

      let width = window.innerWidth
      if(width > 768){
        return (
            <div>
                {/* Left Today's Menu Section */}

                <div style={{overflowY: "scroll", position: "absolute", top: "40px", left: "60px", bottom: "0px", float: "left", width: "30%", paddingRight: "10px", zIndex: '-100' }}>

                   <div style={{marginTop: "60px", marginBottom:"30px"}}>

                         {/*Today's Top Menu and Cooking Instructions. Need to find the right menu to match users' preference*/}

                         <div>

                           {/* Menu Header Section */}
                           <div style={{paddingBottom: "20px", paddingRight:"10px"}}>
                               <h3>TODAY{"'"}S TOP MENU<span className="pull-right">
                                 <Link to={`/menus/${topMenu._id}/order`}><button className="btn btn-default"><i className="fa fa-shopping-cart" aria-hidden="true" style={{marginRight: "5px", color: "green"}}></i> ORDER</button></Link></span>
                               </h3>
                               <hr />
                               <h3> {topMenu.title}</h3>
                               <h3 style={{fontWeight: "300"}}>{totalItem} items <span className="pull-right">$ {totalPrice}</span></h3>

                               <div className="embed-responsive embed-responsive-4by3">

                                   <YouTube videoId={topMenu.introVideo}  />

                                </div>

                               </div>
                               <h4 style={{fontWeight: "300"}}>
                                 {topMenu.description}
                               </h4>
                            </div>

                            {/* Social Icons */}

                             <div style={{paddingBottom: "30px"}}>
                               <div>
                                 <img className="pull-right" src="/assets/img/socialButton.png" width="100px" />
                                 <span className="pull-left">
                                   <h5 style={{fontWeight: "300", color: "grey"}}>2125 Locals ordered</h5>
                                 </span>
                                </div>
                              </div>

                            <hr />

                            {this.renderInstructions()}

                         </div>

                    </div>


                {/*Start the right section*/}

                <div style={{overflowY: "scroll", position: "absolute", top: "50px", left: "38%", bottom: "0px", float: "left",  width: "60%", zIndex: '-100' }}>
                  <div style={{paddingRight: "20px"}}>
                      <form onSubmit ={this.submitSearch} style={{paddingBottom: "30px"}}>
                        <FormControl
                          style={{marginTop: "45px", fontSize: "22px", height: "44px", paddingLeft: "20px", width: width > 769 ? "78%" : "98%"}}
                          type="search"
                          autoFocus
                          onKeyUp={ this.handleSearch }
                          placeholder="Type to search for restaurants or menus"
                          className="Search"
                         /><span className="pull-right" style={{marginTop: "-44px"}}><Button style={{padding: "8px", height: "43px", paddingRight: "10px", paddingLeft: "10px"}}  type="submit" bsStyle="default"><i className="fa fa-search" aria-hidden="true" style={{marginRight: "5px", color: "green"}}></i> SEARCH MENUS</Button></span>
                       </form>
                      {/*Open Instruction Video*/}

                      <div>
                         {
                             this.state.openInstructionVideo

                          ?    <Jumbotron className="text-center" style={{ marginTop: "48px"}}>

                                 <div className="embed-responsive embed-responsive-16by9">

                                    <YouTube videoId={this.state.videoId}    onReady={this.onToStartTime}/>

                                  </div>
                                  <h4 style={{paddingTop:"10px", paddingBottom: "10px", fontWeight:"400"}}>
                                     {this.state.instructionText}
                                  </h4>
                                  <hr />
                               </Jumbotron>

                          : null
                        }
                      </div>

                      {/*Open Intro Menu Video*

                      <div>
                         {
                             this.state.openMenuIntroVideo

                          ?    <Jumbotron className="text-center" style={{ marginTop: "48px"}}>

                                 <div className="embed-responsive embed-responsive-16by9">

                                    <YouTube videoId={this.state.menuIntroVideoId}  />

                                  </div>

                               </Jumbotron>

                          : null
                        }
                      </div>



                         {
                           this.state.openBannerImage

                           ?
                              <Jumbotron className="text-center" style={{ marginTop: "48px"}}>
                                <img className="img-responsive" src={`https://source.unsplash.com/${topMenu.coverImage}/960x350`} />
                              </Jumbotron>

                           : null
                        */ }

                      <div>

                            <RestaurantInfo />

                         </div>
                   </div>
                 </div>
            </div>
         )

        } else {
          return(

              <div>

                <form onSubmit ={this.submitSearch} style={{paddingBottom: "30px"}}>
                  <FormControl
                    style={{marginTop: "80px", fontSize: "22px", height: "46px", paddingLeft: "20px", width: width > 769 ? "80%" : "99%"}}
                    type="search"
                    autoFocus
                    onKeyUp={ this.handleSearch }
                    placeholder="Type to search"
                    className="Search"
                   /><span className="pull-right" style={{marginTop: "-43px", marginRight: "2%"}}><Button style={{padding: "8px", height: "41px", paddingRight: "10px", paddingLeft: "10px"}}  type="submit" bsStyle="default"><i className="fa fa-search" aria-hidden="true" style={{marginRight: "5px"}}></i> Search</Button></span>
                 </form>

                   {
                       this.state.openInstructionVideo

                    ?    <Jumbotron className="text-center" style={{ marginTop: "20px"}}>

                           <div className="embed-responsive embed-responsive-16by9">

                              <YouTube videoId={this.state.videoId}    onReady={this.onToStartTime}/>

                            </div>
                            <h4 style={{paddingTop:"10px", paddingBottom: "10px", fontWeight:"400"}}>
                               {this.state.instructionText}
                            </h4>
                            <hr />
                         </Jumbotron>

                    : null
                  }

                {/*Open Intro Menu Video*

                <div>
                   {
                       this.state.openMenuIntroVideo

                    ?    <Jumbotron className="text-center" style={{ marginTop: "48px"}}>

                           <div className="embed-responsive embed-responsive-16by9">

                              <YouTube videoId={this.state.menuIntroVideoId}  />

                            </div>

                         </Jumbotron>

                    : null
                  }
                </div>
                *


                   {
                     this.state.openBannerImage

                     ?
                        <Jumbotron className="text-center" style={{ marginTop: "30px"}}>
                          <img className="img-responsive" src={`https://source.unsplash.com/${topMenu.coverImage}/689x250`} />
                        </Jumbotron>

                     : null
                   */}



                    <div>

                       <RestaurantInfo />

                    </div>

                         <div style={{marginTop: "60px", marginBottom:"30px"}}>

                          {/*Today's Top Menu and Cooking Instructions. Need to find the right menu to match users' preference*/}

                          <div style={{paddingBottom: "20px", paddingRight:"10px", paddingLeft:"10px"}}>
                            <h3>TODAY{"'"}S TOP MENU<span className="pull-right"><button className="btn btn-success">Order</button></span></h3>
                            <hr />
                            {/* Menu Header Section */}
                            <div style={{paddingBottom: "20px", paddingRight:"10px", paddingLeft:"10px"}}>

                                <h3> {topMenu.title}</h3>
                                <h4 style={{fontWeight: "300"}}>{totalItem} items <span className="pull-right">$ {totalPrice}</span></h4>

                                <div className="embed-responsive embed-responsive-4by3" style={{marginTop: "20px"}}>

                                    <YouTube videoId={topMenu.introVideo}  />

                                 </div>

                                </div>
                                <h4 style={{fontWeight: "300", paddingRight:"10px", paddingLeft:"10px"}}>
                                  {topMenu.description}
                                </h4>
                             </div>

                             {/* Social Icons */}

                              <div style={{paddingBottom: "30px", paddingRight:"10px", paddingLeft:"20px"}}>
                                <div>
                                  <img className="pull-right" src="/assets/img/socialButton.png" width="100px" />
                                  <span className="pull-left">
                                    <h5 style={{fontWeight: "300", color: "grey"}}>2125 Locals ordered</h5>
                                  </span>
                                 </div>
                               </div>

                             <hr />
                             <div style={{paddingBottom: "20px", paddingRight:"10px", paddingLeft:"20px"}}>
                              {this.renderInstructions()}
                             </div>

                          </div>



              </div>
          )
        }

    }

}

export default createContainer(() => {
    // const {_id} = this.props.menu  //TODO Algo to feed dynamically
    Meteor.subscribe('menus.list')
    Meteor.subscribe('instructions.topMenu')
    Meteor.subscribe('menus.topMenu')
    return {
            menus: Menus.find({}).fetch(),
            topMenuInstructions: Instructions.find({}).fetch(),
            menu: Menus.findOne({}, {sort: {viewCounts: -1}})}
}, Home)
