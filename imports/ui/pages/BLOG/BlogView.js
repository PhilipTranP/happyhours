import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'
import { Link, router } from 'react-router'
import Blogs from '../../../api/blogs/blogs.js'
import Loading from '../../common/Loading'


class BlogView extends Component {

  render(){
    if(!this.props.blogView){return<div>loading...</div>}
    const { blogView } = this.props
    const goBack = (event)=>{
       history.go(-1);
    }
    let width = window.innerWidth
    return(

        <div style={{backgroundColor: "#FFF", paddingTop: "80px", width: width > 769 ? "80%" : "96%", marginLeft: width > 769 ? "10%" : "2%", marginBottom: "50px"}}>
          <h1>{blogView && blogView.title}</h1>
          <div style={{paddingBottom: "20px", marginTop: "40px"}}>
             <img className="pull-right" src="/assets/img/socialButton.png" width="100px" />
             <h5 style={{fontWeight: "300", color: "grey", marginRight: "60px"}} className="pull-right">{blogView.viewCounts}  views</h5><span>
             <Link><h4 className="pull-left" onClick={()=>goBack()}><i className="fa fa-history" aria-hidden="true"></i> BACK </h4></Link></span>
          </div>
          <hr />
          <h3 className="text-success" style={{fontWeight: "300", paddingBottom: "10px"}}>{blogView.highlight}</h3>
          <img className= "img-responsive" src={`https://source.unsplash.com/${blogView.coverImage}/1200x500`} />
            <pre style={{marginTop: "-5px", fontSize:"16px", wordSpacing:"2px", fontFamily: "Lato", whiteSpace: "pre-wrap", wordBreak: "normal"}}><h4 style={{fontWeight: "400", paddingLeft: "35px", paddingTop:"30px", paddingBottom:"20px", paddingRight: "20px"}}>{blogView.body}</h4></pre>


            <img className="img-responsive" style={{marginTop: "-15px"}} src={`https://source.unsplash.com/${blogView.bodyImage}/1200x500`} />
            <hr />
            <div style={{paddingBottom: "50px"}}>
              <Link>
              <h3 className="pull-left" onClick={()=> goBack()}><i className="fa fa-history" aria-hidden="true"></i> BACK</h3>
              </Link>
            </div>

        </div>


    )
  }
}



export default createContainer((props) => {
    const {_id} = props.params
    Meteor.subscribe('blogs.list')
    return {blogView: Blogs.findOne({"_id": _id})}
}, BlogView)
