import React, { Component } from 'react'
import { ReactiveVar } from 'meteor/reactive-var';
import { Jumbotron, FormControl} from 'react-bootstrap'
import { Link } from 'react-router'
import { createContainer} from 'meteor/react-meteor-data'
import { markdown } from 'markdown'
import Blogs from '../../../api/blogs/blogs.js'
import Loading from '../../common/Loading'

import { blogUpdateViewCounts } from '../../../api/blogs/methods.js'

const searchQuery = new ReactiveVar(null)

class BlogMain extends Component {
  constructor(props){
    super(props)
    this.state = {
      scrollTop: 0,
      searchTerm: null,
      showBlogBody: false,
      blogViewCounts: 0,
      blogTitle: '',
      blogHighlight: '',
      blogCoverImage: '',
      blogBody: '',
      blogBodyImage: ''
    }
    this.handleBlogClick = this.handleBlogClick.bind(this)
    this.handleSearch = this.handleSearch.bind(this)
    this.handleScroll = this.handleScroll.bind(this)
  }

  componentWillMount(){
  window.addEventListener('scroll', this.handleScroll);
  }
  componentWillUnmount(){
    window.removeEventListener('scroll', this.handleScroll);
  }
  handleScroll() {
     this.setState({scrollTop: $(window).scrollTop()});
  }


  handleSearch(event){
    const searchTerm = event.target.value
    this.setState({ searchTerm})
    searchQuery.set(searchTerm)
  }

  handleBlogClick(_id, title, highlight, coverImage, viewCounts, body, bodyImage){
    Meteor.call('blogViewCounts.update', {_id})
    this.setState({showBlogBody: !this.state.showBlogBody,
    blogTitle: title,
    blogHighlight: highlight,
    blogCoverImage: coverImage,
    blogViewCounts: viewCounts,
    blogBody: body,
    blogBodyImage: bodyImage
    })
  }

  renderBlogs(){
    if(!this.props.blogs){return<div style={{marginTop: "80px"}}><Loading /></div>}
    return this.props.blogs.map(blog=>{
      return (
        <Jumbotron key={blog._id} style={{padding: "20px"}}>

          <div style={{paddingBottom: "20px"}}>
            <h3>{blog.title}</h3>
            <h5 style={{fontWeight: "300", color: "grey"}} className="pull-right">{blog.viewCounts} views</h5>
          </div>

          <hr />

          <Link to={`news/${blog._id}`}><img className="img-responsive" onClick={()=> this.handleBlogClick(blog._id, blog.title, blog.highlight, blog.coverImage, blog.viewCounts, blog.body, blog.bodyImage)} src={`https://source.unsplash.com/${blog.coverImage}/1200x400`} /></Link>
          <div style={{paddingBottom: "30px"}}>
            <h4 style={{fontWeight: "300"}} className=" text-success">{blog.highlight}</h4>
            <Link to={`news/${blog._id}`} className="pull-right" onClick={()=> this.handleBlogClick(blog._id, blog.title, blog.highlight, blog.coverImage, blog.viewCounts, blog.body, blog.bodyImage)}> Read more...</Link>
          </div>

        </Jumbotron>
      )
    })
  }

  renderSearchResults(){
    if(!this.props.searchResults){return<div style={{marginTop: "80px"}}><Loading /></div>}
    const { searchResults } = this.props
    if(searchResults.length > 1){
     return searchResults.map(result=>{
      return (
        <Jumbotron key={result._id} style={{padding: "20px"}}>

          <div style={{paddingBottom: "20px"}}>
            <h3>{result.title}</h3>
            <h5 style={{fontWeight: "300", color: "grey"}} className="pull-right">{result.viewCounts} views</h5>
          </div>

          <hr />

          <Link to={`news/${result._id}`}><img className="img-responsive"  src={`https://source.unsplash.com/${result.coverImage}/1200x400`} /></Link>
          <div style={{paddingBottom: "30px"}}>
            <h4 style={{fontWeight: "300"}} className=" text-success">{result.highlight}</h4>
            <Link to={`news/${result._id}`} className="pull-right" > Read more...</Link>
          </div>

        </Jumbotron>
      )
    })
    } else {
      return(<Jumbotron><h2> Sorry! No results for <strong>{ this.state.searchTerm }</strong> </h2></Jumbotron>)
    }
  }

  render() {
    if(!this.props.topBlog){return<div style={{marginTop: "80px"}}><Loading /></div>}
    const {topBlog} = this.props
    let width = window.innerWidth
    if(width > 768){
      return (
      <div>
         <div style={{overflowY: "scroll", position: "absolute", top: "40px", left: "50px", bottom: "0px", float: "left", width: "31%", paddingRight: "10px", zIndex: '-100' }}>

            <h2 style={{paddingTop: "25px"}} className="text-muted">Latest News</h2>
            {this.renderBlogs()}
            <hr />
          </div>

          {/*Right Most Views Section*/}

           <div style={{overflowY: "scroll", position: "absolute", top: "50px", left: "38%", bottom: "0px", float: "left",  width: "61%", zIndex: '-100' }}>
              <div style={{marginTop: "20px", marginRight: "30px", marginBottom: "30px"}}>
                 <FormControl
                   style={{fontSize: "24px", height: "60px", paddingLeft: "20px"}}
                   type="search"
                   autoFocus
                   onKeyUp={ this.handleSearch }
                   placeholder="Type to search for news"
                   className="Search"
                 />
                 <hr />
              </div>

              {/*Show Search Results*/}

              { this.state.searchTerm
                 ?
                    <div style={{marginTop: "30px", marginRight:"30px"}}>
                     <h2 className="text-muted">SEARCH RESULTS</h2>
                     {this.renderSearchResults()}
                    </div>
                 : null
              }


               <div style={{marginBottom: "50px"}}>
                  <div style={{paddingTop: "20px", paddingRight: "20px"}}>
                     <h2 style={{paddingRight: "10px"}}><span className="text-muted">Featured Report:  </span> {topBlog.title}</h2>
                     <div style={{paddingBottom: "20px", marginTop: "40px"}}>
                        <img className="pull-right" src="/assets/img/socialButton.png" width="100px" />
                        <h5 style={{fontWeight: "300", color: "grey", marginRight: "60px"}} className="pull-right">{topBlog.viewCounts}     views</h5>
                     </div>
                  </div>
                  <div style={{paddingRight: "30px"}}>
                     <hr />
                     <h4 style={{fontWeight:"300"}} className=" text-success">{topBlog.highlight}</h4>
                     <img className="img-responsive" style={{paddingTop: "10px"}} src={`https://source.unsplash.com/${topBlog.coverImage}/1200x400`} />


                         <pre style={{marginTop: "-5px", fontSize:"16px", wordSpacing:"2px", fontFamily: "Lato", whiteSpace: "pre-wrap", wordBreak: "normal"}}><h4 style={{fontWeight: "400", paddingLeft: "30px", paddingTop:"20px", paddingRight: "25px", paddingBottom: "15px"}}>{topBlog.body}</h4></pre>


                       <img className="img-responsive" style={{marginTop: "-15px"}} src={`https://source.unsplash.com/${topBlog.bodyImage}/1200x400`} />
                       <hr />
                  </div>
               </div>

           </div>

       </div>
    )
    } else {
     return (
        <div style={{marginTop: "80px", marginRight:"5px", marginLeft:"5px"}}>
            <div>
              <FormControl
                style={{fontSize: "24px", height: "60px", paddingLeft: "20px", marginBottom: "20px"}}
                type="search"
                onKeyUp={ this.handleSearch }
                autoFocus
                placeholder="Type to search for news"
                className="Search"
              />

             {this.renderSearchResults()}


             <h3 className="text-muted">FEATURED ARTICLE</h3>
              <a href="#"><div className="pull-right" onClick={()=>this.handleScroll()}>GO TO TOP</div></a>
             <hr />
            </div>
            <div>
                <div style={{paddingTop: "20px"}}>
                   <h2>{topBlog.title}</h2>
                   <div style={{paddingBottom: "20px"}}>
                      <img className="pull-right" src="/assets/img/socialButton.png" width="100px" />
                      <h5 style={{fontWeight: "300", color: "grey", marginRight: "60px"}} className="pull-right">{topBlog.viewCounts}     views</h5>
                   </div>
                </div>
                <div>
                   <hr />
                   <h4 style={{fontWeight:"300"}} className=" text-success">{topBlog.highlight}</h4>
                   <img className="img-responsive" style={{paddingTop: "10px"}} src={`https://source.unsplash.com/${topBlog.coverImage}/900x300`} />

                    <pre style={{marginTop: "-5px", fontSize:"16px", wordSpacing:"2px", fontFamily: "Lato", whiteSpace: "pre-wrap", wordBreak: "normal"}}><h4 style={{fontWeight: "400", paddingLeft: "10px", paddingTop:"5px", paddingBottom:"5px", paddingRight: "10px"}}>{topBlog.body}</h4></pre>
                   <img className="img-responsive" style={{marginTop: "-25px", paddingTop: "10px"}} src={`https://source.unsplash.com/${topBlog.bodyImage}/900x300`} />
                    <a href="#"><div className="pull-right" onClick={()=>this.handleScroll()} style={{marginTop: "10px", paddingBottom: "25px"}}>GO TO TOP</div></a>
                   <hr />
                </div>
            </div>
        </div>
      )
     }
  }
}

// export default BlogMain
export default createContainer(() => {
    Meteor.subscribe('blogs.search', searchQuery.get())
    // Meteor.subscribe('blogs.list')
    Meteor.subscribe('blogs.topBlog')
    return {
            blogs: Blogs.find({}).fetch(),
            searchResults: Blogs.find({}, {sort: {title: 1}}).fetch(),
            topBlog: Blogs.findOne({}, {sort: {viewCounts: -1}})}
}, BlogMain)
