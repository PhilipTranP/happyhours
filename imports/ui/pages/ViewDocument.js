import React, { Component } from 'react';
import { ButtonToolbar, ButtonGroup, Button, Row, Col } from 'react-bootstrap';
import { browserHistory, Link } from 'react-router';
import { Bert } from 'meteor/themeteorchef:bert';
import { removeDocument } from '../../api/documents/methods.js';
import NewMenu from './NewMenu.js'
import ViewCurrentMenu from './ViewCurrentMenu.js'

const handleEdit = (_id) => {
  browserHistory.push(`/admin/restaurants/${_id}/edit`);
}


class ViewDocument extends Component {

  constructor(props) {
    super(props);
    this.state = { childVisible: false };
  }

  onAddMenuClick(event) {
    event.preventDefault()
    this.setState({childVisible: !this.state.childVisible});
  }

  render(){
    const { doc } = this.props;
    return (
      <div style={{marginTop: "80px", marginBottom: "30px", paddingLeft: "150px"}}>
        <Row>
           <Col xs={12} lg={12} md={12} className="page-header clearfix">
              <h2 className="pull-left" style={{paddingTop: "20px"}}>{ doc && doc.restaurantName }</h2>
              <ButtonToolbar className="pull-right page-header clearfix">
                <ButtonGroup bsSize="small">

                  <Button onClick={ () => handleEdit(doc._id) }>Edit Restaurant Info</Button>

                  {/****To avoid accidental delete of the restaurant, this button is comment out
                  <Button onClick={ () => handleRemove(doc._id) } className="text-danger">Delete</Button>
                  *************/}

                  </ButtonGroup>
              </ButtonToolbar>
           </Col>




          <Col xs={12} lg={10} md={10}>

              <h3>About { doc && doc.restaurantName }</h3>

              <h4 style={{fontWeight:"300"}}>{ doc && doc.restaurantAbout }</h4>

              <h3>Featured Chef: <strong>{ doc && doc.chefName }</strong></h3>

              <h4 style={{fontWeight:"300"}}>{ doc && doc.chefBio }</h4>

          </Col>

          {/******** Curent menu section ***********/}

          <Col xs={12} lg={12} md={12} style={{marginRight: "20px"}}>
            
{/*
            <ButtonToolbar className="pull-right page-header clearfix">
              <ButtonGroup bsSize="small">

                <Button onClick={this.onAddMenuClick.bind(this)}

                    bsSize="small"
                    className="pull-right"
                >
                  Add Menu
                </Button>

                ****To avoid accidental delete of the restaurant, this button is comment out
                <Button onClick={ () => handleRemove(doc._id) } className="text-danger">Delete</Button>
                *************

                </ButtonGroup>
            </ButtonToolbar>

*/}

            {/**This is Add Menu Title and Desc Section to show when click Add Menu Button**/}

                {
                   this.state.childVisible
                   ? <Col xs={12} lg={10} md={10}>
                       <NewMenu restaurantInfo={doc} />
                     </Col>
                   : null
                }

               <div style={{marginTop:"40px"}}>
                 <ViewCurrentMenu restaurantInfo={doc} />
               </div>

          </Col>

        </Row>
      </div>
    )
  }
}


ViewDocument.propTypes = {
  restaurantInfo: React.PropTypes.object,
};

export default ViewDocument;
