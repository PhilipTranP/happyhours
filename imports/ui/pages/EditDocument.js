import React from 'react';
import DocumentEditor from '../components/DocumentEditor.js';

const EditDocument = ({ doc }) => (
  <div className="EditDocument" style={{marginTop: "80px", marginBottom: "30px", paddingLeft: "150px"}}>
    <h4 className="page-header">Editing "{ doc.restaurantName }"</h4>
    <DocumentEditor doc={ doc } />
  </div>
);

EditDocument.propTypes = {
  doc: React.PropTypes.object,
};

export default EditDocument;
