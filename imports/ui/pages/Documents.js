import React from 'react';
import { Link } from 'react-router';
import { Row, Col, Button } from 'react-bootstrap';
import DocumentsList from '../containers/DocumentsList.js';

const Documents = () => (
  <div className="Documents" style={{marginTop: "80px", marginBottom: "30px", paddingLeft: "150px"}}>
    <Row>
      <Col xs={ 12 }>
        <div className="page-header clearfix">
          <h4 className="pull-left">Restaurants</h4>
          <Link to="/restaurants/new">
            <Button
              bsStyle="success"
              className="pull-right"
            >New Restaurant</Button>
          </Link>
        </div>
        <DocumentsList />
      </Col>
    </Row>
  </div>
);

export default Documents;
