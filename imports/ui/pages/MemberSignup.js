import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data'
import { Link } from 'react-router';
import { Row, Col, FormGroup, ControlLabel, FormControl, Button } from 'react-bootstrap';
import handleSignup from '../../modules/member-signup';
import Inquiries from '../../api/inquiries/inquiries.js'

class MemberSignup extends Component {

  componentDidUpdate() {
    handleSignup({ component: this });
  }

  handleSubmit(event) {
    event.preventDefault();
  }

  render() {
    if(!this.props.inquiry){return<div>loading...</div>}
    const memberName = this.props.inquiry.firstName
    return (
      <div>
          <div >
            <h2 className="page-header" style={{marginTop: '80px'}}>Hello {memberName}</h2>
          </div>
          <div>
            <Row>
              <Col xs={ 12 } sm={ 6 } md={ 4 }>
                <h4 style={{fontWeight: "300"}}>Once signed-up, you will get access to the member area. Enjoy the service!</h4>
                <h4 className="page-header">MEMBER SIGNUP FORM</h4>
                <form
                  ref={ form => (this.signupForm = form) }
                  onSubmit={ this.handleSubmit }
                >
                  <Row>
                    <Col xs={ 6 } sm={ 6 }>
                      <FormGroup>
                        <ControlLabel>First Name</ControlLabel>
                        <FormControl
                          type="text"
                          ref="firstName"
                          name="firstName"
                          placeholder="First Name"
                        />
                      </FormGroup>
                    </Col>
                    <Col xs={ 6 } sm={ 6 }>
                      <FormGroup>
                        <ControlLabel>Last Name</ControlLabel>
                        <FormControl
                          type="text"
                          ref="lastName"
                          name="lastName"
                          placeholder="Last Name"
                        />
                      </FormGroup>
                    </Col>
                  </Row>
                  <FormGroup>
                    <ControlLabel>Email Address</ControlLabel>
                    <FormControl
                      type="text"
                      ref="emailAddress"
                      name="emailAddress"
                      placeholder="Email Address"
                    />
                  </FormGroup>
                  <FormGroup>
                    <ControlLabel>Password</ControlLabel>
                    <FormControl
                      type="password"
                      ref="password"
                      name="password"
                      placeholder="Password"
                    />
                  </FormGroup>
                  <Button type="submit" bsStyle="success" className="pull-right">Signup</Button>
                </form>
                <p>Already have an account? <Link to="/">Log In</Link>.</p>
              </Col>
            </Row>
          </div>
      </div>
    );
  }
}

export default createContainer((props)=>{
  const {_id} = props.params
  const subscription = Meteor.subscribe('inquiries.view', _id);
  const loading = !subscription.ready()
  const inquiry = Inquiries.findOne()
  return { loading, inquiry}
}, MemberSignup)
