import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'
import { Meteor } from 'meteor/meteor'
import { Link } from 'react-router'
import Menus from '../../api/menus/menus.js'
import Instructions from '../../api/instructions/instructions.js'
import { Jumbotron, Row, Col } from 'react-bootstrap'
import YouTube from 'react-youtube'
import _ from 'lodash'


import Loading from '../common/Loading.js';


class ViewMenu extends Component {

  constructor(props) {
    super(props);
    this.state = {
      startTime: null,
      endTime: null,
      videoId: null,
      player: null,
      instructionText:'',
      openInstructionVideo: false

    };
    this.onToStartTime = this.onToStartTime.bind(this)
    this.first15Words = this.first15Words.bind(this)
  }

  onToStartTime(event) {
     // eslint-disable-line
    this.setState({
      player: event.target.playVideo(),
    });
      this.state.player.seekTo(this.state.startTime, true);
      if(YouTube.PlayerState=1){
        const loopStart = () => {this.state.player.seekTo(this.state.startTime, true)}
        return setTimeout(loopStart, (this.state.endTime - this.state.startTime)*1000)
      }
  }


  first15Words(words){
    return words.split(' ').slice(0,15).join(' ') + " ..."
  }


  renderInstructions(){
    if(!this.props.instructions){return<div style={{marginTop: "100px"}}><Loading /></div>}
      return this.props.instructions.map(instruction => {
        return(
          <div key={instruction._id}>

            {/* Menu Item Desciption Section */}
             <div>
               <h3><span>{instruction.title}</span><span className="pull-right" style={{fontWeight: "300", paddingRight: "10px"}}>$ {instruction.price}</span></h3>

             </div>

            {/* Menu Clickable Image Leads To Video Instruction */}
            <div onClick={()=>{this.setState({
                videoId: instruction.instructionVideo,
                startTime: instruction.startTime,
                endTime: instruction.endTime,
                instructionText: instruction.instructions,
                openInstructionVideo: !this.state.openInstructionVideo
              })}}>
              <a href="#"><img className="img-responsive"
                 style={{display: "block",
                 marginTop: "30px",
                 marginLeft: "auto",
                 marginRight: "auto" }} src={`http://media2.giphy.com/media/${instruction.image}/200.gif`} alt="" width="130%" /></a>

              {/*
              <a href="#"><img className="img-responsive" src={`https://source.unsplash.com/${instruction.image}/400x250`} alt="" /></a>
              */}
              </div>
              <div style={{marginTop: "20px"}}>
                <h4>{instruction.description}</h4>
                <a href="#"  onClick={ ()=>{this.setState({ videoId: instruction.instructionVideo,
                startTime: instruction.startTime,
                endTime: instruction.endTime,
                instructionText: instruction.instructions,
                openInstructionVideo: !this.state.openInstructionVideo})} } >
                <h4 style={{fontWeight: "300"}}>{this.first15Words(instruction.instructions)}</h4></a>
              </div>
              <hr />
          </div>
        )
      })
  }

  render(){
    if(!this.props.menu){return<div style={{marginTop: "100px"}}><Loading /></div>}

      const totalPrice = _.sumBy(this.props.instructions, 'price')
      const totalItem = this.props.instructions.length

      let width = window.innerWidth
      if(width > 768){
        return (

           <div>

               {/* Start Left Section */}

               <div className="visible-md visible-lg" style={{overflowY: "scroll", position: "absolute", top: "40px", left: "60px", bottom: "0px", float: "left", width: "30%", paddingRight: "10px", zIndex: '-100' }}>

                    <div style={{marginTop: "50px", marginBottom: "40px"}}>

                       <div style={{paddingBottom: "20px"}}>
                         <img className="img-responsive" src={`https://source.unsplash.com/${this.props.menu.coverImage}/400x250`} />
                         <h2>{this.props.menu.title}</h2>
                         <h3 style={{fontWeight: "300", paddingRight:"10px"}}> {totalItem } items <span className="pull-right">$ { totalPrice}</span></h3>
                       </div>

                       {/* Start Social Icons Section*/ }
                       <div style={{paddingBottom: "30px"}}>
                          <div>
                             <img className="pull-right" src="/assets/img/socialButton.png" width="100px" />
                             <span className="pull-left">
                               <h5 style={{fontWeight: "300", color: "grey"}}>225 Locals ordered</h5>
                             </span>
                          </div>
                        </div>

                        <hr />

                        {this.renderInstructions()}

                    </div>

               </div>


               {/* Start of the right section */}


               <div className="visible-md visible-lg" style={{overflowY: "scroll", position: "absolute", top: "40px", left: "38%", bottom: "0px", float: "left",  width: "60%", zIndex: '-100' }}>

                   <div style={{paddingRight: "20px", marginBottom:"20px"}}>

                          {/*Intro Menu and Order Button Section*/}

                          <div className="menu-intro" >
                             <div style={{marginTop: "40px"}}>
                               <h2>{this.props.menu.restaurantName} restaurant offers the {this.props.menu.title} </h2>
                               <Link to={`/menus/${this.props.menu._id}/order`}><button className="btn btn-default pull-right"><i className="fa fa-shopping-cart" aria-hidden="true" style={{marginRight: "5px", color: "green"}}></i> ORDER </button></Link>
                             </div>
                             <div style={{ marginRight: "70px"}} >
                               <h4  style={{ fontWeight: "300" }} >
                                 {this.props.menu.description}
                               </h4>
                             </div>
                          </div>
                          <hr />

                        {/* Hide and Show Video Instruction Section */}

                        <div>
                           {
                               this.state.openInstructionVideo

                            ?  <div>
                                   <div className="embed-responsive embed-responsive-16by9">

                                      <YouTube videoId={this.state.videoId}    onReady={this.onToStartTime}/>

                                    </div>
                                    <h4 style={{paddingTop:"20px", paddingBottom: "30px", fontWeight:"400"}}>
                                       {this.state.instructionText}
                                    </h4>
                                      {/* Start Social Icons Section*/ }
                                      <div style={{paddingBottom: "30px"}}>
                                         <div>
                                            <img className="pull-right" src="/assets/img/socialButton.png" width="100px" />
                                            <span className="pull-left">
                                              <h5 style={{fontWeight: "300", color: "grey"}}>112 views </h5>
                                            </span>
                                         </div>
                                       </div>
                                       <hr />
                                </div>

                            : null
                          }
                        </div>

                        {/* Menu Intro Video Section */}

                        <div className="chef-welcome" style ={{marginTop: "30px", paddingBottom: "20px"}}>

                           <div className="embed-responsive embed-responsive-16by9">
                               <YouTube videoId={this.props.menu.introVideo} />
                           </div>
                        </div>
                    </div>
                 </div>

                {/********Small screen layout
                 <div className="visible-xs" style={{overflowY: "scroll", position: "absolute", top: "60", left: "10", bottom: "0", float: "left", width: "30%", zIndex: "-100" }}>

                 </div>

                 <div className="visible-xs" style={{overflowY: "scroll", position: "absolute", top: "60", left: "37%", bottom: "0", float: "left",  width: "65%", zIndex: '-100' }}>

                </div>
                ****/}

          </div>
        )
      } else {
        return (

               <div style={{marginTop: "90px", paddingLeft: "20px", paddingRight: "20px", marginBottom:"50px"}}>

                    {/*Intro Menu and Order Button Section*/}

                      <div className="menu-intro" >
                         <div style={{marginTop: "40px"}}>
                           <h2>{this.props.menu.restaurantName} restaurant offers the {this.props.menu.title} </h2>
                            <Link to={`/menus/${this.props.menu._id}/order`}><button className="btn btn-default pull-right"><i className="fa fa-shopping-cart" aria-hidden="true" style={{marginRight: "5px", color: "green"}}></i> ORDER </button></Link>
                         </div>
                         <div style={{ marginRight: "70px"}} >
                           <h4  style={{ fontWeight: "300" }} >
                             {this.props.menu.description}
                           </h4>
                         </div>
                      </div>
                      <hr />

                    {/* Hide and Show Video Instruction Section */}

                    <div>
                       {
                           this.state.openInstructionVideo

                        ?  <div>
                               <div className="embed-responsive embed-responsive-16by9">

                                  <YouTube videoId={this.state.videoId}    onReady={this.onToStartTime}/>

                                </div>
                                <h4 style={{paddingTop:"20px", paddingBottom: "30px", fontWeight:"400"}}>
                                   {this.state.instructionText}
                                </h4>

                                {/* Start Social Icons Section*/ }
                                <div style={{paddingBottom: "30px"}}>
                                   <div>
                                      <img className="pull-right" src="/assets/img/socialButton.png" width="100px" />
                                      <span className="pull-left">
                                        <h5 style={{fontWeight: "300", color: "grey"}}>112 views </h5>
                                      </span>
                                   </div>
                                 </div>
                                 <hr />
                            </div>

                        : null
                      }
                    </div>

                    {/* Menu Intro Video Section */}

                    <div className="chef-welcome" style ={{marginTop: "30px", paddingBottom: "20px"}}>

                       <div className="embed-responsive embed-responsive-16by9">
                           <YouTube videoId={this.props.menu.introVideo} />
                       </div>
                    </div>

                    <div style={{marginTop: "50px", marginBottom: "40px"}}>

                       <div style={{paddingBottom: "20px"}}>
                         <img className="img-responsive" src={`https://source.unsplash.com/${this.props.menu.coverImage}`} />
                         <h3>{this.props.menu.title}</h3>
                         <h4 style={{fontWeight: "300", paddingRight:"10px"}}> {totalItem } items <span className="pull-right">$ { totalPrice}</span></h4>
                       </div>

                       {/* Start Social Icons Section*/ }
                       <div style={{paddingBottom: "30px"}}>
                          <div>
                             <img className="pull-right" src="/assets/img/socialButton.png" width="100px" />
                             <span className="pull-left">
                               <h5 style={{fontWeight: "300", color: "grey"}}>225 Locals ordered</h5>
                             </span>
                          </div>
                        </div>

                        <hr />

                        {this.renderInstructions()}

                    </div>
                    <div   >
                      <Link to={`/menus/${this.props.menu._id}/order`}><button className="btn btn-default" style={{display: "block",
                       marginTop: "30px",
                       marginLeft: "auto",
                       marginRight: "auto", marginBottom:"30px" }}><i className="fa fa-shopping-cart" aria-hidden="true" style={{marginRight: "5px", color: "green"}}></i> ORDER </button></Link>
                    </div>

                    {/* Start Social Icons Section*/ }
                    <div style={{paddingBottom: "30px"}}>
                       <div>
                          <img className="pull-right" src="/assets/img/socialButton.png" width="100px" />
                          <span className="pull-left">
                            <h5 style={{fontWeight: "300", color: "grey"}}>225 Locals ordered</h5>
                          </span>
                       </div>
                     </div>

               </div>
        )
      }
  }
}

export default createContainer((props) => {
    const {_id} = props.params
    Meteor.subscribe('menus.list')
    Meteor.subscribe('instructions.list')

    return {menu: Menus.findOne({"_id": _id}),
             instructions: Instructions.find({"menuId": _id}).fetch() }
}, ViewMenu)
