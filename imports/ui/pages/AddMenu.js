// import React from 'react';
// import { ButtonToolbar, ButtonGroup, Button } from 'react-bootstrap';
// import { browserHistory, Link } from 'react-router';
// import { Bert } from 'meteor/themeteorchef:bert';
// import { removeDocument } from '../../api/documents/methods.js';
//
// const handleEdit = (_id) => {
//   browserHistory.push(`/restaurants/${_id}/add-menu`);
// }
//
//
// //To avoid accidental delete of the restaurant, this function is commented out
//
// // const handleRemove = (_id) => {
// //   if (confirm('Are you sure? This is permanent!')) {
// //     removeDocument.call({ _id }, (error) => {
// //       if (error) {
// //         Bert.alert(error.reason, 'danger');
// //       } else {
// //         Bert.alert('Document deleted!', 'success');
// //         browserHistory.push('/restaurants');
// //       }
// //     });
// //   }
// // };
//
// const ViewDocument = ({ doc }) => (
//   <div className="ViewDocument">
//     <div className="page-header clearfix">
//       <h4 className="pull-left">{ doc && doc.restaurantName }</h4>
//       <ButtonToolbar className="pull-right">
//         <ButtonGroup bsSize="small">
//           <Link to="/menus/new">
//             <Button
//               bsStyle="success"
//               className="pull-right"
//             >Add Menu</Button>
//           </Link>
//           <Button onClick={ () => handleEdit(doc._id) }>Edit Restaurant Info</Button>
//
//           {/****To avoid accidental delete of the restaurant, this button is comment out
//           <Button onClick={ () => handleRemove(doc._id) } className="text-danger">Delete</Button>
//           *************/}
//
//         </ButtonGroup>
//       </ButtonToolbar>
//     </div>
//     { doc && doc.restaurantAbout }
//     <hr />
//     <h5 className="pull-left">Featured Chef: <strong>{ doc && doc.chefName }</strong></h5>
//     <hr />
//     <p className="pull-left">{ doc && doc.chefBio }</p>
//   </div>
// );
//
// ViewDocument.propTypes = {
//   doc: React.PropTypes.object,
// };
//
// export default ViewDocument;
