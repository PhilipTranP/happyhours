import React from 'react';
import DocumentEditor from '../components/DocumentEditor.js';

const NewDocument = () => (
  <div className="NewDocument">
    <h4 className="page-header">New Restaurant</h4>
    <DocumentEditor />
  </div>
);

export default NewDocument;
