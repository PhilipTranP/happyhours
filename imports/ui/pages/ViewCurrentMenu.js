import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data'
import { browserHistory, Link } from 'react-router'
import { Col, ListGroup, ListGroupItem } from 'react-bootstrap';
import MenuEditor from '../components/MenuEditor'
import { removeMenu } from '../../api/menus/methods.js';

import Menus from '../../api/menus/menus.js';

const handleNav = (_id) => {
  browserHistory.push(`/admin/restaurants/menus/${_id}`)
}

class ViewCurrentMenu extends Component {

  constructor(props){
    super(props)
    this.state ={
      showMenuEditor: false,
      showAddMenuButton: true,
      showCancelAddMenuButton: false,
      restaurantId: null,
      restaurantName: null,
      restaurantAbout: null,
      chefName: null,
      chefBio: null
    }
  }


      RenderMenus() {

        if(!this.props.menus){
          return <div>Loading...</div>
        }

        const handleRemoveMenu = (_id) => {
          if (confirm('Are you sure? This is permanent!')) {
            Meteor.call('menu.remove', { _id }, (error) => {
              if (error) {
                Bert.alert(error.reason, 'danger');
              } else {
                Bert.alert('Menu deleted!', 'success');
              }
            });
          }
        };

        return this.props.menus.map(menu => {
          return (
           <div key={menu._id} >

              <ListGroup >

                  <ListGroupItem onClick={ () => handleNav(menu._id) }>
                    <h4> Title: {menu.title} </h4>
                  </ListGroupItem>

                  <ListGroupItem style={{paddingBottom:"20px"}}>
                    <h4 style ={{fontWeight: "300"}}>Description: {menu.description}</h4>
                    <Link to={`/admin/restaurants/menus/${menu._id}/edit`}>
                      <button className="btn btn-default pull-right"> Edit Menu </button>
                    </Link>

                    <Link to={`/admin/restaurants/${menu.restaurantId}`}><button className="btn btn-default pull-right" onClick={
                        ()=> handleRemoveMenu(menu._id)
                      }>
                       Delete Menu
                    </button></Link>


                  </ListGroupItem>

               </ListGroup>
            </div>

          )
        })
       }

  render() {

     return(

       <Col xs={12} lg={10} md={10} style={{marginTop:"40px"}}>

          <div>
            { this.state.showAddMenuButton
              ?
                <button className="btn btn-default pull-right" style={{ marginBottom:"20px" }}
                  onClick={()=>this.setState({showMenuEditor: !this.state.showMenuEditor,
                    showCancelAddMenuButton: !this.state.showCancelAddMenuButton,
                    showAddMenuButton: !this.state.showAddMenuButton,
                    restaurantId: this.props.restaurantInfo._id,
                    restaurantName: this.props.restaurantInfo.restaurantName,
                    restaurantAbout: this.props.restaurantInfo.restaurantAbout,
                    chefName: this.props.restaurantInfo.chefName,
                    chefBio: this.props.restaurantInfo.chefBio})}>
                  Add Menu
                </button>
               : null
             }
            <div style={{marginTop:"10px", marginBottom: "40px"}}>

               { this.state.showMenuEditor
                 ?
                   <MenuEditor
                       restaurantId = {this.state.restaurantId}
                       restaurantName = {this.state.restaurantName}
                       restaurantAbout = {this.state.restaurantAbout}
                       chefName = {this.state.chefName}
                       chefBio = {this.state.chefBio}
                    />
                 : null
               }
               { this.state.showCancelAddMenuButton
                 ?
                   <button style={{marginTop: "-20px"}} className="btn btn-default pull-left" onClick={()=>this.setState({
                       showAddMenuButton: !this.state.showAddMenuButton,
                       showMenuEditor: !this.state.showMenuEditor,
                       showCancelAddMenuButton: !this.state.showCancelAddMenuButton})}>
                   Cancel
                   </button>
                 : null
               }
             </div>

             <h3 style={{marginBottom: "20px"}} className="pull-left">Curent Menus</h3>
             {this.RenderMenus()}
          </div>

       </Col>

     )
  }

}

export default createContainer((props) => {
    const { _id } = props.restaurantInfo
    Meteor.subscribe('menus.list')
    Meteor.subscribe('menus.view', _id)
    return { menus: Menus.find({"restaurantId" : _id }).fetch(),
             menu: Menus.findOne()}
}, ViewCurrentMenu)
