import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'
import { Jumbotron } from 'react-bootstrap';
import { FormGroup, ControlLabel, FormControl } from 'react-bootstrap'

import Menus from '../../api/menus/menus.js';
import Instructions from '../../api/instructions/instructions.js'
import Loading from '../common/Loading'


let subTotal
class OrderMenu extends Component {

  constructor(props){
    super(props)
    this.state={
      selectedOption: null
    }
    this.handleOnChange = this.handleOnChange.bind(this)
      this.formSumit = this.formSumit.bind(this)
  }

  handleOnChange(event){
    this.setState({selectedOption: event.target.value})
  }

  formSumit(event){
    event.preventDefault()
    alert(this.refs.test1.value)
    this.refs.test1.value = ''
  }
  renderInstructions(){

      if(!this.props.instructions){return <div>Loading...</div>}
      return this.props.instructions.map((instruction, index) => {
        let orderItem = index + 1

        return(
          <div key={instruction._id}>
            <Jumbotron>
              <div style={{ paddingBottom:"50px"}}>
                 <h4 style={{marginBottom:"60px"}}>SELECT ITEM {orderItem} QUANTITY:
                 <span className="pull-right" style={{marginLeft: "30px", marginBottom:"50px"}}>
                   <select onChange={()=>this.setState({})}>
                      <option value="-1" disabled>--</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      </select>
                 </span></h4>
               <h4 className="pull-left" style={{paddingTop:"0px"}}> <a href="#"> {instruction.title}</a></h4>
                 <span >
                     <h4 className="pull-right" style={{paddingTop:"0px"}}><small>$ </small> {instruction.price}</h4>
                 </span>
              </div>
               <hr />
               <div style={{paddingBottom: "40px"}}>
                 <h4 className="pull-left">Subtotal: {instruction.price * 3}</h4>
                 <form className="form-group">

                    <label> SUBTOTAL </label>
                    <input type="text" ref="test1" className="form-control" />
                    <button onClick={()=>this.formSubmit}>Submit</button>
                 </form>
                 <span>
                   <h4 className="pull-right"><small>$ </small> { instruction.price }</h4>
                 </span>
               </div>

            </Jumbotron>
          </div>
        )
      })
  }

  render(){



    if(!this.props.menu){return <div style={{marginTop:"80px"}}><Loading /></div>}
    const { menu } = this.props
    const menuNameUpperCase = menu.title.toUpperCase()
    const imageURL = `https://source.unsplash.com/${menu.coverImage}/1300x200`
    return(
      <div>

            <div style={{ marginTop: "50px", position: "absolute", left: "0px", width: "100%"}}>
                <div className="text-center" style={{background: `url(${imageURL})`, opacity: "0.4", color: "#FFF", paddingTop:"50px", paddingBottom:"50px", backgroundColor: "grey"}}>
                  <h2 >ORDER MENU</h2>
                </div>
             </div>

            <div style={{overflowY: "scroll", position: "absolute", top: "160px", left: "4%",  bottom: "0px", float: "left",  width: "44%", zIndex: '-100' }}>

               <div style={{marginTop: "40px", padding: "20px"}}>
                 <div style={{paddingBottom: "100px"}}>
                    <h3 className="pull-left"> SUMMER SELECTION FOR ACTIVE LIFESTYLES</h3>
                    <h3 style={{fontWeight: "300"}} className="pull-right"><strong>12</strong> items, total: <small>$ </small><strong> 75</strong></h3>

                 </div>
                 <hr />


                 <div>
                   {this.renderInstructions()}
                  </div>
               </div>
             </div>

             <div style={{overflowY: "scroll", position: "absolute", top: "200px", left: "52%", bottom: "0px", float: "left",  width: "44%", zIndex: '-100' }}>
               <Jumbotron style={{marginTop: "40px", padding: "20px"}}>
                 <h2>Welcome to Deliver Happy Hours: 2-5pm.com</h2>
                 <h4 style={{fontWeight: "300"}}>While our competitors deliver frozen foods and promote centralized farming, we deliver fresh local produce with selected ingredients and cooking instructions from your favorite local restaurant chefs who buy from local farmers.
                 </h4>
                 <h4 style={{fontWeight: "300"}}>While our competitors deliver frozen foods and promote centralized farming, we deliver fresh local produce with selected ingredients and cooking instructions from your favorite local restaurant chefs who buy from local farmers.
                 </h4>
                 <h4 style={{fontWeight: "300"}}>While our competitors deliver frozen foods and promote centralized farming, we deliver fresh local produce with selected ingredients and cooking instructions from your favorite local restaurant chefs who buy from local farmers.
                 </h4>

               </Jumbotron>

            </div>
      </div>
    )
  }
}
// export default OrderMenu
export default createContainer((props) => {
    const {_id} = props.params
    Meteor.subscribe('menus.list')
    Meteor.subscribe('instructions.list')

    return {menu: Menus.findOne({"_id": _id}),
             instructions: Instructions.find({"menuId": _id}).fetch() }
}, OrderMenu)
