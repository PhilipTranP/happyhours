import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'
import { Jumbotron, Row, Col, FormGroup, ControlLabel, Button, Alert } from 'react-bootstrap'

import Menus from '../../../api/menus/menus.js';
import Instructions from '../../../api/instructions/instructions.js'
import Loading from '../../common/Loading'
import SubTotal from './components/SubTotal'
import CreditCard from './components/CreditCard'
import _ from 'lodash'



class MenuOrder extends Component {

  constructor(props){
    super(props)
    this.state={
      selectedOption: null
    }
    this.handleOnChange = this.handleOnChange.bind(this)
  }

  handleOnChange(event){
    this.setState({selectedOption: event.target.value})
  }


  render(){

    if(!this.props.menu){return <div style={{marginTop:"80px"}}><Loading /></div>}
    const { menu } = this.props
    const menuNameUpperCase = menu.title.toUpperCase()
    const imageURL = `https://source.unsplash.com/${menu.coverImage}/1300x200`
    const amount = _.sumBy(this.props.instructions, 'price')
    if(!this.props.instructions){return <div style={{marginTop:"80px"}}><Loading /></div>}
    const items = _.lastIndexOf(this.props.instructions)
    let width = window.innerWidth
    if(width>768){
      return(
        <div>

              <div style={{ marginTop: "50px", position: "absolute", left: "0px", width: "100%"}}>
                  <div className="text-center" style={{background: `url(${imageURL})`, opacity: "0.4", color: "#FFF", paddingTop:"40px", paddingBottom:"50px", backgroundColor: "grey"}}>
                    <h2 >ORDER MENU</h2>
                  </div>
               </div>

              <div style={{overflowY: "scroll", position: "absolute", top: "150px", left: "4%",  bottom: "0px", float: "left",  width: "44%", zIndex: '-100' }}>

                 <div style={{marginTop: "40px", padding: "20px", paddingBottom: "50px"}}>
                   <div style={{paddingBottom: "100px"}}>
                      <h3 className="pull-left">{menuNameUpperCase}</h3>
                      <h3 style={{fontWeight: "300", paddingBottom: "40px"}} className="pull-right"><strong>{items}</strong> items, total: <small>$ </small><strong>{amount}</strong></h3>

                   </div>

                   <div style={{marginTop: "50px"}}>

                      <Jumbotron>
                       <h4>Payments are securely processed by <a href="#" onClick={()=>window.open(
                          'https://stripe.com/',
                          '_blank'
                        )}>Stripe.</a>
                       </h4>
                       <h4 style={{fontWeight: "300"}}>Your card is not charged until after you've confirmed your order.</h4>
                       <hr />
                       <h4>Promo code</h4>
                       <form>
                        <Row style={{paddingLeft: "50px", paddingRight: "50px", paddingTop:"10px"}}>
                          <Col>
                            <FormGroup>
                              <input
                                className="form-control text-center"
                                type="text"
                                ref="promo"
                                placeholder="promo code"
                              />
                            </FormGroup>

                         </Col>
                         <span><Button type="submit" bsStyle="success" block>APPLY</Button></span>
                        </Row>
                       </form>
                     </Jumbotron>

                  </div>
                 </div>
               </div>

               <div style={{overflowY: "scroll", position: "absolute", top: "150px", left: "52%", bottom: "0px", float: "left",  width: "44%", zIndex: '-100' }}>
                 <Jumbotron style={{marginTop:"80px"}}>
                   <CreditCard menuId={menu && menu._id} menuTitle={menu && menu.title} menuAmount={amount} items={items}/>
                 </Jumbotron>
              </div>
        </div>
      )
    } else {
      return(
        <div style={{marginTop: "64px"}}>
          <div className="text-center" style={{background: `url(${imageURL})`, opacity: "0.4", color: "#FFF", paddingTop:"40px", paddingBottom:"50px", backgroundColor: "grey", marginLeft: "0px auto", marginRight: "0px auto"}}>
            <h2 >ORDER MENU</h2>
          </div>

          <div style={{ padding: "20px", paddingBottom: "50px"}}>
            <div style={{paddingBottom: "100px"}}>
               <h3 className="pull-left">{menuNameUpperCase}</h3>
               <h3 style={{fontWeight: "300", paddingBottom: "40px"}} className="pull-right"><strong>{items}</strong> items, total: <small>$ </small><strong>{amount}</strong></h3>

            </div>

            <div style={{marginTop: "50px"}}>
               <Jumbotron>
                <h4>Payments are securely processed by <a href="#" onClick={()=>window.open(
                   'https://stripe.com/',
                   '_blank'
                 )}>Stripe.</a>
                </h4>
                <h4 style={{fontWeight: "300"}}>Your card is not charged until after you've confirmed your order.</h4>
                <hr />
                <h4>Promo code</h4>
                <form>
                 <Row style={{paddingLeft: "50px", paddingRight: "50px", paddingTop:"10px"}}>
                   <Col>
                     <FormGroup>
                       <input
                         className="form-control text-center"
                         type="text"
                         ref="promo"
                         placeholder="promo code"
                       />
                     </FormGroup>

                  </Col>
                  <span><Button type="submit" bsStyle="success" block>APPLY</Button></span>
                 </Row>
                </form>
              </Jumbotron>
            </div>

            <Jumbotron style={{marginTop:"30px"}}>
              <CreditCard menuId={menu && menu._id} menuTitle={menu && menu.title} menuAmount={amount} items={items}/>
            </Jumbotron>

          </div>

        </div>
      )
    }
  }
}
// export default OrderMenu
export default createContainer((props) => {
    const {_id} = props.params
    Meteor.subscribe('menus.list')
    Meteor.subscribe('instructions.list')

    return {menu: Menus.findOne({"_id": _id}),
             instructions: Instructions.find({"menuId": _id}).fetch() }
}, MenuOrder)
