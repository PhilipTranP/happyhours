import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'
import Payment from 'payment'
import { Jumbotron, Row, Col, FormGroup, ControlLabel, Button, Alert } from 'react-bootstrap'
import { Bert } from 'meteor/themeteorchef:bert'
import _ from 'lodash'
// import Orders from '../../../../api/orders/orders.js'
import { upsertOrder, sendReceivedOrderEmail } from '../../../../api/orders/methods.js'
// import { getStripeToken } from '../../../../modules/get-stripe-token.js'
import { browserHistory } from 'react-router'


class CreditCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      address_city: null,
      address_line1: null,
      address_zip: null,
      exp_month: null,
      exp_year: null,
      last4: null,
      brand: null,
      menuId: '',
      menuTitle: '',
      items: '',
      menuAmount: '',
      orderAmount: ''
    }
    this.setCardType = this.setCardType.bind(this)
    this.handlePaymentInfoSubmit = this.handlePaymentInfoSubmit.bind(this)
    this.resetCard = this.resetCard.bind(this)
    this.confirmOrder = this.confirmOrder.bind(this)
    this.handleOrderSubmit = this.handleOrderSubmit.bind(this)
  }

  resetCard() {
    this.setState({ number: null, exp_month: null, exp_year: null, cvc: null, token: null });
  }

  handlePaymentInfoSubmit(event) {
    event.preventDefault()
    const { refs } = this;
    const number = refs.number.value;
    const expiration = refs.expiration.value.split('/');
    const exp_month = parseInt(expiration[0], 10);
    const exp_year = parseInt(expiration[1], 10);
    const cvc = refs.cvc.value;
    const address_line1 = refs.street.value;
    const address_city = refs.city.value;
    const address_zip = refs.zip.value

    const card = { number, exp_month, exp_year, cvc, address_line1, address_city, address_zip }
    const billingAddress = address_line1 + ' ' + address_city + ' ' + address_zip
    Stripe.createToken(card, (status, result) => {
        if(result.error){
          alert(result.error.message);
        }else{
          Meteor.call("addCard", result.id, (err, response)=>{
            if(err){
              Bert.alert(error.reason, 'danger');
            }
            this.setState({
              address_line1: result.card.address_line1,
              address_city: result.card.address_city,
              address_zip: result.card.address_zip,
              exp_month: result.card.exp_month,
              exp_year: result.card.exp_year,
              brand: result.card.brand,
              last4: result.card.last4,
              menuId: this.props.menuId,
              menuTitle: this.props.menuTitle,
              items: this.props.items,
              menuAmount: this.props.menuAmount,
            })
            const {items, menuAmount, menuId, menuTitle} = this.state
            const address = this.state.address_line1 + ' ' + this.state.address_city + ' ' + this.state.address_zip
            const upsert = {
              menuId: menuId,
              userId: Meteor.userId(),
              menuTitle: menuTitle,
              amount: menuAmount,
              items: items,
              address: address
            }

            Meteor.call("orders.upsert", upsert, (err, response)=>{
              if(err){
                Bert.alert(err.reason, 'danger')
              }
               Meteor.call ("loadCardInfo", (err, result)=>{
                 if(err){
                   Bert.alert(err.reason, 'danger')
                 }
                 this.setState({ orderAmount: result.order_amount })
                 console.log(result)
               })
            })
          })
        }
      })
  }

  handleOrderSubmit(event){
    event.preventDefault()
    Meteor.call("chargeUser", function(err, result){
      if(err){
        Bert.alert(err.reason, 'danger')
      }else{
        Meteor.call('sendReceivedOrderEmailMethod', function (err,response){
          if (err){
            Bert.alert(err.reason, 'danger')
          }else{
            Bert.alert('Confirmation email sent. Please check your inbox. Thanks for the order!', 'success')
           }
            browserHistory.push("/menus")
        })
        console.log(result)
      }
    })
  }


  setCardType(event) {
    const type = Payment.fns.cardType(event.target.value);
    const cards = document.querySelectorAll('[data-brand]');

    [].forEach.call(cards, (element) => {
      if (element.getAttribute('data-brand') === type) {
        element.classList.add('active');
      } else {
        element.classList.remove('active')
      }
    });
  }

  renderCardList() {
    return (<ul className="credit-card-list clearfix">
      <li><i data-brand="visa" className="fa fa-cc-visa"></i></li>
      <li><i data-brand="amex" className="fa fa-cc-amex"></i></li>
      <li><i data-brand="mastercard" className="fa fa-cc-mastercard"></i></li>
      <li><i data-brand="jcb" className="fa fa-cc-jcb"></i></li>
      <li><i data-brand="discover" className="fa fa-cc-discover"></i></li>
      <li><i data-brand="dinersclub" className="fa fa-cc-diners-club"></i></li>
    </ul>);
  }

  renderCardForm() {

    return (<form className="CardForm" onSubmit={ this.handlePaymentInfoSubmit }>
      <Row>
        <Col xs={ 12 }>
          <FormGroup>
            <ControlLabel>Card Number</ControlLabel>
            <input
              autoFocus
              onKeyUp={ this.setCardType }
              className="form-control"
              type="text"
              ref="number"
              placeholder="Card Number"
            />
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col xs={ 6 } sm={ 5 }>
          <FormGroup>
            <ControlLabel>Expiration</ControlLabel>
            <input
              className="form-control text-center"
              type="text"
              ref="expiration"
              placeholder="MM/YYYY"
            />
          </FormGroup>
        </Col>
        <Col xs={ 6 } sm={ 4 } smOffset={ 3 }>
          <FormGroup>
            <ControlLabel>CVC</ControlLabel>
            <input
              className="form-control text-center"
              type="text"
              ref="cvc"
              placeholder="CVC"
            />
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col xs={ 12 }>
          <FormGroup>
            <ControlLabel>Billing Address</ControlLabel>
            <input
              className="form-control"
              type="text"
              ref="street"
              placeholder="Street"
            />
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col xs={ 6 } sm={ 5 }>
          <FormGroup>
            <input
              className="form-control text-center"
              type="text"
              ref="city"
              placeholder="City"
            />
          </FormGroup>
        </Col>
          <Col xs={ 6 } sm={ 4 } smOffset={ 3 }>
            <FormGroup>
              <input
                className="form-control text-center"
                type="text"
                ref="zip"
                placeholder="Zip"
              />
            </FormGroup>
         </Col>
      </Row>
      <Button type="submit" style={{marginTop: "20px"}} bsStyle="success" block>CONTINUE</Button>
    </form>);
  }

  confirmOrder() {
    const { address_city, address_line1, address_zip, exp_month, exp_year, last4, brand} = this.state;
    return(
      <div>
           <h3>Address</h3>
           <hr />
           <h4>Street: <span style={{fontWeight: "300"}}>{address_line1}</span></h4>
           <h4>City: <span style={{fontWeight: "300"}}>{address_city}</span></h4>
           <h4>Zip: <span style={{fontWeight: "300"}}>{address_zip}</span></h4>
           <hr />
           <h3>Payment Info</h3>
           <hr />
           <h4>Expire Month: <span style={{fontWeight: "300"}}>{exp_month}</span></h4>
           <h4>Expire Year: <span style={{fontWeight: "300"}}>{exp_year}</span></h4>
           <h4>{brand} Card: <span style={{fontWeight: "300"}}>...{last4}</span></h4>
           <hr />
           <h3>Order Amount: $ {this.state.orderAmount}</h3>
           <hr />
           <form className="CardForm" onSubmit={ this.handleOrderSubmit }>
               <Button type="submit" style={{marginTop: "20px"}} bsStyle="success" block>PLACE ORDER</Button>
           </form>
      </div>
    )
  }




  componentDidMount() {
    const { number, expiration, cvc } = this.refs;
    Payment.formatCardNumber(number);
    Payment.formatCardExpiry(expiration);
    Payment.formatCardCVC(cvc);
  }

  render() {

   const items = this.props.items

    return (

          <div>
            { this.state.address_city
              ?
               <div style={{ paddingLeft: window.innerWidth > 768 ? "40px" : "0px"}}>
                {this.confirmOrder()}
               </div>
              :
                <div className="CreditCard" style={{ paddingLeft: window.innerWidth > 768 ? "40px" : "0px"}}>
                   { this.renderCardList() }
                   { this.renderCardForm() }
                </div>
            }
          </div>
    )
  }
}

CreditCard.propTypes = {};

export default CreditCard
// export default createContainer(()=>{
//   const subscription = Meteor.subscribe('order.amount')
//   const loading = !subscription.ready()
//   const allOrder = Orders.find().fetch()
//   return { loading, allOrder}
// }, CreditCard)
