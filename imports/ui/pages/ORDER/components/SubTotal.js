import React, { Component } from 'react'

let amountSelected
class SubTotal extends Component {
  constructor(props){
    super(props)
    this.state = {
      amountSelected : null
    }
    this.handleOnChange = this.handleOnChange.bind(this)
  }

  handleOnChange(event){
    event.preventDefault()
    this.setState({amountSelected: event.target.value})
  }
  render(){
    if(!this.props.item){return<div>loading...</div>}

    return(
      <div>
        <div style={{paddingBottom: "40px"}}>
          <h4 className="pull-left">Subtotal: </h4>
          <span>
            <h4 className="pull-right"><small>$ </small> </h4>
          </span>
        </div>
        <h4 style={{marginBottom:"60px"}}>SELECT QUANTITY:
        <span className="pull-right" style={{marginLeft: "30px", marginBottom:"50px"}}>
          <select onChange={this.handleOnChange}>
             <option value="-1" disabled>--</option>
             <option value="1">1</option>
             <option value="2">2</option>
             <option value="3">3</option>
             </select>
        </span></h4>
      </div>
    )
  }
}

export default SubTotal
