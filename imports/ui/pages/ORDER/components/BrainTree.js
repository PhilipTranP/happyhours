import React, { Component } from 'react'
import DropIn from 'braintree-react'
import braintree from 'braintree-web'

class BrainTree extends Component {
  render(){
    return(
     <div>
       <DropIn braintree={braintree} />
     </div>
    )
  }
}

export default BrainTree
