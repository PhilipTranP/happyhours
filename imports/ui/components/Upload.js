// import React, { Component } from 'react'
// import ReactDOM from 'react-dom'
// import { Template } from 'meteor/templating'
// import { Blaze } from 'meteor/blaze'
//
// export default
// class Upload extends Component {
//   componentDidMount() {
//     this.view = Blaze.render(Template.upload,
//       ReactDOM.findDOMNode(this.refs.container))
//   }
//   componentWillUnmount() {
//     Blaze.remove(this.view)
//   }
//
//   render () {
//     return (
//       <div ref="container"></div>
//     )
//   }
// }

// import React from 'react'
// import ReactDOM from 'react-dom'
// import { ReactiveVar } from 'meteor/reactive-var'
// import { Template } from 'meteor/templating'
// import { Blaze } from 'meteor/blaze'
//
// export default
// class Upload extends React.Component {
//     render() { return <div ref="blazeContainer"><h1>Hello There!</h1></div> }
//
//     componentDidMount() {
//         const container = ReactDOM.findDOMNode(this.refs.blazeContainer)
//         const {name, data} = this.props
//
//         this.reactiveData = new ReactiveVar(data)
//         this.componentWillReceiveProps(this.props)
//         this.blazeView = Blaze.renderWithData(
//             Template[name],
//             () => this.reactiveData.get(),
//             container
//         )
//     }
//
//     componentWillUnmount() {
//         Blaze.remove(this.blazeView)
//     }
//
//     componentWillReceiveProps(nextProps) {
//         const {data} = nextProps
//         this.reactiveData.set(data)
//     }
//
//     shouldComponentUpdate() { return false }
// }
