/* eslint-disable max-len, no-return-assign */

import React from 'react';
import { browserHistory, Link } from 'react-router';
import { FormGroup, ControlLabel, FormControl, Button } from 'react-bootstrap';
import documentEditor from '../../modules/document-editor.js';
import { removeDocument } from '../../api/documents/methods.js';

export default class DocumentEditor extends React.Component {
  componentDidMount() {
    documentEditor({ component: this });
    setTimeout(() => { document.querySelector('[name="restaurantName"]').focus(); }, 0);
  }

  render() {

    const { doc } = this.props;

    const handleRemove = (_id) => {
      if (confirm('Are you sure? This is permanent!')) {
        removeDocument.call({ _id }, (error) => {
          if (error) {
            Bert.alert(error.reason, 'danger');
          } else {
            Bert.alert('Document deleted!', 'success');
            browserHistory.push('/restaurants');
          }
        });
      }
    };
        return (
          <form
          ref={ form => (this.documentEditorForm = form) }
          onSubmit={ event => event.preventDefault() }
           >
              <FormGroup>
                <ControlLabel>Restaurant Name</ControlLabel>
                <FormControl
                  type="text"
                  name="restaurantName"
                  defaultValue={ doc && doc.restaurantName }
                  placeholder="restaurant name!"
                />
              </FormGroup>
              <FormGroup>
                <ControlLabel>About The Restaurant</ControlLabel>
                <FormControl
                  componentClass="textarea"
                  style={{height: '300px'}}
                  name="restaurantAbout"
                  defaultValue={ doc && doc.restaurantAbout }
                  placeholder="What is so special about this restaurant?"
                />
              </FormGroup>
              <FormGroup>
                <ControlLabel>Chef Name</ControlLabel>
                <FormControl
                  type="text"
                  name="chefName"
                  defaultValue={ doc && doc.chefName }
                  placeholder="chef name!"
                />
              </FormGroup>
              <FormGroup>
                <ControlLabel>Chef Bio</ControlLabel>
                <FormControl
                  componentClass="textarea"
                  style={{height: '300px'}}
                  name="chefBio"
                  defaultValue={ doc && doc.chefBio }
                  placeholder="What is so good about this chef?"
                />
              </FormGroup>
              <Button type="submit" bsStyle="success">
                { doc && doc._id ? 'Save Changes' : 'Add Restaurant' }
              </Button>
              <Button onClick={ () => handleRemove(doc._id) } bsStyle="default" className="pull-right">
                { doc && doc._id ? 'Delete Restaurant' : null}
              </Button>
            </form>

         )

   }
}

DocumentEditor.propTypes = {
  doc: React.PropTypes.object,
};
