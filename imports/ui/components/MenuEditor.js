/* eslint-disable max-len, no-return-assign */

import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data'
import { browserHistory, Link } from 'react-router';
import { FormGroup, ControlLabel, FormControl, Button } from 'react-bootstrap';
import menuEditor from '../../modules/menu-editor.js';
// import handleSubmit from '../../modules/menu-editor.js';


class MenuEditor extends Component {
  constructor(props){
    super(props)
    this.state ={
      restaurantId: null,
      restaurantName: null,
      restaurantAbout: null,
      chefName: null,
      chefBio: null
    }
  }
  componentDidMount() {
    setTimeout(() => { document.querySelector('[name="title"]').focus(); }, 0);
  }

  render() {

  const{ menu } = this.props
  const handleAddMenu = () => {

      const upsert = {
        isPublished: false,
        title: this.refs.title.value,
        description: this.refs.description.value,
        introVideo: this.refs.introVideo.value.trim(),
        instructionVideo: this.refs.instructionVideo.value.trim(),
        coverImage: this.refs.coverImage.value.trim(),
        restaurantId:  this.props.restaurantId,
        restaurantName: this.props.restaurantName,
        restaurantAbout: this.props.restaurantAbout,
        chefName: this.props.chefName,
        chefBio: this.props.chefBio,
        viewCounts: 1
      }
      Meteor.call('menus.upsert', upsert , (error, response) =>{
        if (error) {
          Bert.alert(error.reason, 'danger');
        } else {
          this.menuEditorForm.reset();
          Bert.alert('Menu Added OK!', 'success');
          browserHistory.push(`/admin/restaurants/menus/${response.insertedId}`)
        }
      })
    }

    const handleUpdateMenu = () => {
      //  const titleCase = title => title
      //     .split(/ /g).map(word =>
      //         `${word.substring(0,1).toUpperCase()}${word.substring(1)}`)
      //     .join(" ");
        const upsert = {
          isPublished: false,
          // title: titleCase(this.refs.title.value),
          title: this.refs.title.value,
          description: this.refs.description.value,
          introVideo: this.refs.introVideo.value.trim(),
          instructionVideo: this.refs.instructionVideo.value.trim(),
          coverImage: this.refs.coverImage.value.trim(),
          //Get restaurantID and Name by props passed down from ViewDocument page
          restaurantId:  menu.restaurantId,
          restaurantName: menu.restaurantName,
          restaurantAbout: menu.restaurantAbout,
          chefName: menu.chefName,
          chefBio: menu.chefBio,
          viewCounts: 1
        }
        if (menu && menu._id) upsert._id = menu._id;
        Meteor.call('menus.upsert', upsert , (error, response) =>{
          if (error) {
            Bert.alert(error.reason, 'danger');
          } else {
            this.menuEditorForm.reset();
            Bert.alert('Menu Updated OK!', 'success');
            browserHistory.push(`/admin/restaurants/menus/${response.insertedId || menu._id}`)
          }
        })
      }

      return (
          <div style={{paddingBottom: "18px"}}>
             { menu && menu._id
               ? <div style={{paddingBottom: "18px"}}>
                   <h3> Editing Menu: {menu.title}</h3>
                   {/*Remove a menu is performed on restaurant view page
                   <span>
                     <Button onClick={ () => handleRemove(menu._id) } bsStyle="default" className="pull-right">
                        Delete Menu
                     </Button>
                   </span>
                   */}
                 </div>
               : null
             }

            <form
            ref={ form => (this.menuEditorForm = form)}
            onSubmit={ event => event.preventDefault() }
              >

               {/*
               <div className="form-check">
                  <label className="form-check-label">
                    <input className="form-check-input"  ref= "isPublished" type="checkbox" value="FALSE" />
                    Set Menu Private
                  </label>
                </div>
                <div className="form-check disabled">
                  <label className="form-check-label">
                    <input className="form-check-input" type="checkbox" value="" disabled />
                    System admin will publish the menu
                  </label>
                </div>
                */}
             <div className="form-group">
               <label> Title </label>
               <input type="text" name ="title" ref="title" className="form-control" defaultValue={menu && menu.title} />
             </div>
             <div className="form-group">
               <label> Description </label>
               <textarea className="form-control" name ="description" rows="5" ref="description" defaultValue={menu && menu.description}></textarea>
             </div>
             <div className="form-group">
               <label> Intro Video </label>
               <input type="text" ref="introVideo" name ="introVideo" className="form-control" defaultValue={menu && menu.introVideo} placeholder="INSERT ONLY THE ID PART <ZJy1ajvMU1k> e.g. 'https://www.youtube.com/watch?v=ZJy1ajvMU1k'"/>
             </div>
             <div className="form-group">
               <label> Instruction Video </label>
               <input type="text" ref="instructionVideo" name ="instructionVideo" className="form-control" defaultValue={menu && menu.instructionVideo} placeholder="INSERT ONLY THE ID PART <ZJy1ajvMU1k> e.g. 'https://www.youtube.com/watch?v=ZJy1ajvMU1k'"/>
             </div>
             <div className="form-group">
               <label> Cover Image </label>
               <input type="text" ref="coverImage" name ="coverImage" className="form-control" defaultValue={menu && menu.coverImage} placeholder="INSERT ONLY THE ID PART <jpkfc5_d-DI> e.g. 'https://unsplash.com/search/restaurant?photo=jpkfc5_d-DI'"/>
             </div>
             <div>
                 { menu && menu._id
                   ? <Button onClick={()=>handleUpdateMenu().bind(this)} bsStyle="success" className="pull-right">
                     Save Changes
                    </Button>
                   : null
                 }
             </div>
             <div style={{marginTop:"20px"}}>

                 { this.props.restaurantId
                   ? <Button onClick={()=>handleAddMenu()} bsStyle="success" className="pull-right">
                     Add Menu
                     </Button>
                   : null
                 }
             </div>
           </form>
         </div>
       )
   }
}

export default MenuEditor
