import React from 'react';
import { browserHistory } from 'react-router';
import { ListGroup, ListGroupItem, Alert } from 'react-bootstrap';

const handleNav = (_id) => {
  browserHistory.push(`/admin/restaurants/${_id}`);
}

const DocumentsList = ({ documents }) => (
  documents.length > 0 ? <ListGroup className="DocumentsList">
    {documents.map(({ _id, restaurantName }) => (
      <ListGroupItem key={ _id } onClick={ () => handleNav(_id) }>
        { restaurantName }
      </ListGroupItem>
    ))}
  </ListGroup> :
  <Alert bsStyle="warning">No restaurants yet.</Alert>
);

DocumentsList.propTypes = {
  documents: React.PropTypes.array,
};

export default DocumentsList;
