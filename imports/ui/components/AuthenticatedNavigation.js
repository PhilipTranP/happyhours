import React, { PropTypes } from 'react';
import { browserHistory, Link } from 'react-router';
import { LinkContainer } from 'react-router-bootstrap';
import { Nav, NavItem, NavDropdown, MenuItem, Button } from 'react-bootstrap';
import { Meteor } from 'meteor/meteor';
import IsRole from '../common/IsRole';

const handleLogout = () => Meteor.logout(() => browserHistory.push('/'));


const userName = () => {
  const user = Meteor.user();
  const name = user && user.profile ? user.profile.name : '';
  return user ? `${name.first} ${name.last}` : '';
};

const AuthenticatedNavigation = () => (
  <div>
    <Nav>
      <LinkContainer to="/menus">
        <NavItem eventKey={ 1 } href="/menus" style={{fontSize: "12px", marginTop: "2px"}}>MENUS</NavItem>
      </LinkContainer>
    </Nav>
    <Nav>
      <LinkContainer to="/recipes">
        <NavItem eventKey={ 1.1 } href="/recipes" style={{fontSize: "12px", marginTop: "2px"}}>RECIPES</NavItem>
      </LinkContainer>
    </Nav>
    <Nav>
      <LinkContainer to="/news">
        <NavItem eventKey={ 1.2 } href="/news" style={{fontSize: "12px", marginTop: "2px"}}>NEWS</NavItem>
      </LinkContainer>
    </Nav>


    <IsRole role="admin">
      <Nav>
        <LinkContainer to="/admin/restaurants">
          <NavItem eventKey={ 2 } href="/admin/restaurants">Restaurants</NavItem>
        </LinkContainer>
        <LinkContainer to="/admin/inquiries">
          <NavItem eventKey={ 2.1 } href="/admin/inquiries">Inquiries</NavItem>
        </LinkContainer>
        <LinkContainer to="/admin/blogs">
          <NavItem eventKey={ 2.2 } href="/admin/blogs">Write Blogs</NavItem>
        </LinkContainer>
      </Nav>
    </IsRole>
    <Nav pullRight style={{marginRight: "-28px"}}>

      <NavDropdown eventKey={ 3 } title={ userName() } id="basic-nav-dropdown">
        <LinkContainer to="/my-collections">
          <MenuItem eventKey={ 3.1 } href="/my-collections">My Collections</MenuItem>
        </LinkContainer>
        <MenuItem eventKey={ 3.2 } onClick={ handleLogout }>Logout</MenuItem>
      </NavDropdown>
    </Nav>
  </div>
);

AuthenticatedNavigation.propTypes = {
  role: PropTypes.string
}
export default AuthenticatedNavigation;
