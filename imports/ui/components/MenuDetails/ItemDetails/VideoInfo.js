import React, { Component } from 'react'
import ReactPlayer from 'react-player'

export default class VideoInfo extends Component {
  render() {
    return (
      <div >
         <h3> Making The Vegetarian Vietnamese Crêpe <span className="pull-right" style={{fontWeight: "300"}}>Time: 7 mins</span>
         </h3>
         <hr />
         <h4 style={{fontWeight: "300"}}>
          Mix rice flour, sugar, 1/2 teaspoon salt, and turmeric together in a large bowl. Beat in coconut milk to make a thick batter. Slowly beat in water until batter is the consistency of a thin crepe batter. Heat 1 1/2 tablespoon oil in a large skillet over medium-high heat...
         </h4>
          <Media>
            <div className="media">
              <div className="media-controls">
                <CurrentTime/>
                <SeekBar/>
                <Duration/>
                <PlayPause />
              </div>
              <div  className="embed-responsive embed-responsive-16by9">
               <ReactPlayer url={this.props.VideoURL} playing controls="true" />
              </div>

            </div>
          </Media>

          <hr />



        </div>
    )
  }
}
