import React from 'react';
import { Navbar } from 'react-bootstrap';
import { Link, LinkContainer } from 'react-router';
import PublicNavigation from './PublicNavigation.js';
import AuthenticatedNavigation from './AuthenticatedNavigation.js';

const renderNavigation = hasUser => (hasUser ? <AuthenticatedNavigation /> : <PublicNavigation />);

const AppNavigation = ({ hasUser }) => {
  let width = window.innerWidth

  if (width > 400){
    return (
      <Navbar className="navbar-fixed-top" style={{width: "100%"}}>
        <Navbar.Header>
          <Navbar.Brand className = "navbar-brand">
            <Link to="/" style={ { width: "300px" }}>
              <img src="/assets/img/2to5.png" style={ { width: "35%", display: "inline-block", float: "left", marginTop: "-5px", marginLeft: "-5px" }} />
              {/*
              <p  style={ { marginLeft: "10px", display: "inline-block", float: "left", fontWeight: "300", fontSize: "15px"}}>Deliver Happy Hours</p>
              */}
            </Link>
          </Navbar.Brand>
          <Navbar.Toggle />
          {/*
          <Link to="/blog"><p className="pull-right" style={{marginTop: "15px", paddingRight: "20px"}}>Blog</p></Link>
          */}
        </Navbar.Header>
        <Navbar.Collapse>
          { renderNavigation(hasUser) }
        </Navbar.Collapse>
      </Navbar>
    )
  } else if(width > 600) {
    return (
      <Navbar className="navbar-fixed-top" style={{width: "100%"}}>
        <Navbar.Header>
          <Navbar.Brand className = "navbar-brand">
            <Link to="/" style={ { width: "450px" }}>
              <img src="/assets/img/2to5.png" style={ { width: "36%", display: "inline-block", float: "left", marginTop: "-15px", marginLeft: "-17px" }} />
              <p  style={ { float: "left", paddingTop: "-25px", marginLeft: "15px", display: "inline-block", fontWeight: "300", fontSize: "24px"}}> Deliver Happy Hours</p>
            </Link>
          </Navbar.Brand>
          <Navbar.Toggle />
        {/* <Link to="/blog"><p className="pull-right" style={{marginTop: "15px", paddingRight: "20px"}}>Blog</p></Link> */}
        </Navbar.Header>
        <Navbar.Collapse>
          { renderNavigation(hasUser) }
        </Navbar.Collapse>
      </Navbar>
    )
  } else {
    return (
      <Navbar className="navbar-fixed-top" style={{width: "100%"}}>
        <Navbar.Header>
          <Navbar.Brand className = "navbar-brand">
            <Link to="/" style={ { width: "200px" }}>
              <img src="/assets/img/2to5.png" style={ { width: "50%", display: "inline-block", float: "left", marginTop: "-5px", marginLeft: "-5px" }} />
            </Link>
          </Navbar.Brand>
          <Navbar.Toggle />
          {/*
          <Link to="/blog"><p className="pull-right" style={{marginTop: "15px", paddingRight: "20px"}}>Blog</p></Link>
          */}
        </Navbar.Header>
        <Navbar.Collapse>
          { renderNavigation(hasUser) }
        </Navbar.Collapse>
      </Navbar>
    )
  }

}


AppNavigation.propTypes = {
  hasUser: React.PropTypes.object,
};

export default AppNavigation;
