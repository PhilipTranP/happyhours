import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import Menus from '../../../api/menus/menus.js';
import Menu from '../components/Menu.js';
import Loading from '../../common/Loading.js';
import { ListGroup } from 'react-bootstrap';



class MenusList extends Component {
  // render(){
  //   if(!this.props.menus){return <div style={{marginTop: "200px"}}>Nothing Yet</div>}
  //   console.log(this.props.menus)
  //   return <div style={{marginTop: "200px"}}>HELLO</div>
  // }

  RenderMenus() {
    const {menus} = this.props

    return menus.map(menu => {
      return (
        <ListGroup key={menu._id}>
           <Menu
             menuId = {menu._id}
             menuTitle = {menu.title}
             menuDesc = {menu.description}
           />
        </ListGroup>
      )
    })
  }

  render(){

    if(!this.props.menus){
      return <Loading />
    }
      return (
      <div>
       {this.RenderMenus()}
      </div>
    )
   }
}


MenusList.propTypes = {
  menuId: React.PropTypes.string,
  menuTitle: React.PropTypes.string,
  menuDesc: React.PropTypes.string
}

export default createContainer(() => {
    Meteor.subscribe('menus.list')
    return {menus: Menus.find({}).fetch()}
}, MenusList)
