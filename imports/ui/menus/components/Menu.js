import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import { ListGroup, ListGroupItem, Alert } from 'react-bootstrap';

const handleNav = (_id) => {
  browserHistory.push(`/menus-list/${_id}`);
}

class Menu extends Component {
  render() {
  const _id = this.props.menuId
  const title = this.props.menuTitle
  const description = this.props.menuDesc
   if(_id.length > 0){
     return (
      <div>
         <ListGroupItem onClick={ () => handleNav(_id) }>
           {title}
         </ListGroupItem>
         <ListGroupItem>
           {description}
         </ListGroupItem>
       </div>
     )
   } else {
     return (
        <Alert bsStyle="warning">No menus yet.</Alert>
     )
   }

  }
}



export default Menu;
