import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'
import { Jumbotron } from 'react-bootstrap';
import Inquiries from '../../../api/inquiries/inquiries.js'
import { removeInquiry } from '../../../api/inquiries/methods.js'
import { sendEmail } from '../../../api/inquiries/methods.js'
import { epochToHuman } from '../../../modules/dates';

import Loading from '../../common/Loading'


class InquiryList extends Component {

  renderInquiries (){

    const handleRemoveInquiry = (_id) => {
      if (confirm('Are you sure? This is permanent!')) {
        Meteor.call('inquiry.remove', { _id }, (error) => {
          if (error) {
            Bert.alert(error.reason, 'danger')
          } else {
            Bert.alert('Inquiry removed!', 'success')
          }
        })
      }
    }

    const handleSendEmail = (fromEmail, message) => {
        if(!this.props.inquiries){return<div style={{marginTop: '80px'}}><Loading /></div>}
        Meteor.call('sendEmail',
                'plt1028@gmail.com',
                 fromEmail,
                'Hello from 2-5pm.com!',
                 message);
    }

    if(!this.props.inquiries){return<div style={{marginTop: '80px'}}><Loading /></div>}

      return this.props.inquiries.map(inquiry => {
        return(
          <Jumbotron key={inquiry._id} style={{paddingBottom: '30px'}}>
                <div className="pull-right">
                  <button className="btn btn-success" onClick={()=>handleSendEmail(inquiry.email, inquiry.message)}>Send SignUp Link</button>
                  <button className="btn btn-danger" style={{marginLeft:"10px"}}
                    onClick={() => handleRemoveInquiry(inquiry._id)}>Delete</button>
                </div>
                <h3>Name: {inquiry.name}</h3>
                <h3>Date: {epochToHuman(inquiry.createdAt)}</h3>
                <h3>Email: {inquiry.email}</h3>
                <h3 >Message: </h3>
                <span><h3 style={{fontWeight: "300"}}>{inquiry.message}</h3></span>

          </Jumbotron>
        )
      })
  }
  render(){

    return(
        <div style={{marginTop: "80px"}}>
          {this.renderInquiries()}
        </div>
    )
  }
}

export default createContainer(() => {
    const subscription = Meteor.subscribe('inquiries.list')
    const loading = !subscription.ready()
    const inquiries = Inquiries.find({}, {
        sort: {
          createdAt: -1
        }
      }).fetch()
    return { loading, inquiries}
}, InquiryList)
