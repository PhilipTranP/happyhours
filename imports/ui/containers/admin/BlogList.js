import React, { Component } from 'react'
import { createContainer } from 'meteor/react-meteor-data'
import { Jumbotron } from 'react-bootstrap'
import Blogs from '../../../api/blogs/blogs.js'
import { upsertBlog } from '../../../api/blogs/methods.js'
import { removeBlog } from '../../../api/blogs/methods.js'


import Loading from '../../common/Loading'

class BlogList extends Component {
  constructor(props){
    super(props)
    this.state = {
      showBlogEditor: false,
      blogId: '',
      blogTitle: '',
      blogHighlight: '',
      blogCoverImage: '',
      blogBody: '',
      blogBodyImage: '',
      blogViewCounts: ''
    }
  }

  renderBlogs (){


    const handleRemoveBlog = (_id) => {
      if (confirm('Are you sure? This is permanent!')) {
        Meteor.call('blog.remove', { _id }, (error) => {
          if (error) {
            Bert.alert(error.reason, 'danger')
          } else {
            Bert.alert('Blog removed!', 'success')
          }
        })
      }
    }


    if(!this.props.blogs){return<div style={{marginTop: '80px'}}><Loading /></div>}
      return this.props.blogs.map(blog => {

        return(
          <div key={blog._id}>
            <Jumbotron  style={{paddingBottom: '30px'}}>
                  <div className="pull-right">
                    <button className="btn btn-danger" style={{marginLeft:"10px"}}
                      onClick={() => handleRemoveBlog(blog._id)}>DELETE</button>
                      <span><button className="btn btn-default" style={{marginLeft:"10px"}}
                        onClick={() => this.setState
                          ({showBlogEditor: !this.state.showBlogEditor,
                            blogId: blog._id,
                            blogTitle: blog.title,
                            blogCoverImage: blog.coverImage,
                            blogHighlight: blog.highlight,
                            blogBodyImage: blog.bodyImage,
                            blogBody: blog.body,
                            blogViewCounts: blog.viewCounts
                          })}>EDIT</button></span>
                  </div>
                  <h3>{blog.title}</h3>
                  <hr />
                  <img  className="img-responsive" src={`https://source.unsplash.com/${blog.coverImage}/1200x350`} />
                  <h3 style={{fontWeight: "300"}} className="text-center text-success">{blog.highlight}</h3>
                  <hr />
                  <div>
                       <img  className="img-responsive" src={`https://source.unsplash.com/${blog.bodyImage}/1200x350`} />
                         <div>
                           <pre style={{fontSize:"16px", fontFamily: "Lato", whiteSpace: "pre-wrap", wordBreak: "normal"}}>{blog.body}</pre>
                         </div>
                  </div>

            </Jumbotron>
          </div>
        )
      })
  }
  render(){

    handleBlogUpsert = () => {

      const upsert = {

         title: this.refs.title.value.trim(),
         highlight: this.refs.highlight.value.trim(),
         coverImage: this.refs.coverImage.value.trim(),
         body: this.refs.blogBody.value.trim(),
         bodyImage: this.refs.bodyImage.value.trim(),
         viewCounts: parseInt(this.refs.viewCounts.value)

       }

       const confirmation = 'blog added!'
       const _id = this.state.blogId
       if(!_id == '') upsert._id = _id
       Meteor.call('blog.upsert', upsert , (error, response) =>{
         if (error) {
           Bert.alert(error.reason, 'danger');
         } else {
           Bert.alert(confirmation, 'success');
           this.refs.title.value = ''
           this.refs.highlight.value = ''
           this.refs.coverImage.value = ''
           this.refs.blogBody.value = ''
           this.refs.bodyImage.value = ''
           this.refs.viewCounts.value = ''
           this.setState({showBlogEditor: !this.state.showBlogEditor,
             blogId: '',
             blogTitle: '',
             blogHighlight: '',
             blogCoverImage: '',
             blogBodyImage: '',
             blogBody: '',
             blogViewCounts
           })
         }
       })
    }

      return(
        <div style={{marginTop: "80px"}}>
          <div style={{marginBottom: "50px"}}>
            <h3>Create Happy News</h3>
            <button className=" btn btn-success pull-right" onClick={()=> this.setState({showBlogEditor: !this.state.showBlogEditor})}>ADD NEW ARTICLE</button>
          </div>
          <div>
          <hr />
            { this.state.showBlogEditor
              ?
                <Jumbotron style={{marginTop: "10px", paddingBottom: "30px"}}>

                      {/*display edit form when click*/}

                      <div>

                            <div className="form-group">

                               <div className="form-group">
                                  <label> Title </label>
                                  <input type="text" ref="title" className="form-control" defaultValue={this.state.blogTitle} placeholder="blog title"/>
                               </div>

                               <div className="form-group">
                                 <label> Cover Image </label>
                                 <input type="text" ref="coverImage" className="form-control" defaultValue={this.state.blogCoverImage} placeholder="insert image id from Unplash"/>
                               </div>

                               <div className="form-group">
                                   <label> Blog Highlight </label>
                                   <textarea className="form-control" rows="5"  ref="highlight" defaultValue={this.state.blogHighlight} placeholder="insert blog highlight"></textarea>
                               </div>

                               <div className="form-group">
                                  <label> Body Image </label>
                                  <input type="text" ref="bodyImage" className="form-control" defaultValue={this.state.blogBodyImage} placeholder="insert one body image id from Unplash"/>
                               </div>

                               <div className="form-group">
                                   <label> Blog Body </label>
                                   <textarea className="form-control" rows="8"  ref="blogBody" defaultValue={this.state.blogBody} placeholder="insert blog body"></textarea>
                               </div>

                                <div className="form-group">
                                   <label> View Counts </label>
                                   <input type="text" ref="viewCounts" className="form-control" defaultValue={this.state.blogViewCounts} placeholder="Set view count for featured blog"/>
                                </div>

                               <button className="btn btn-default" onClick={()=>this.setState({showBlogEditor: !this.state.showBlogEditor})}>

                                 CANCEL

                               </button>

                               <span>
                                 <button className="btn btn-success pull-right" onClick={()=>handleBlogUpsert()}>

                                 { this.state.blogId == ''  ?  'ADD ARTICLE'  : 'SAVE CHANGES' }

                                 </button>
                               </span>
                           </div>

                      </div>
                </Jumbotron>
              : null
            }
          </div>
          <div>
            {this.renderBlogs()}
          </div>
        </div>
      )
  }
}

export default createContainer(() => {
    const subscription = Meteor.subscribe('blogs.list')
    const loading = !subscription.ready()
    const blogs = Blogs.find({}, {
        sort: {
          createdAt: -1
        }
      }).fetch()
    return { loading, blogs}
}, BlogList)
