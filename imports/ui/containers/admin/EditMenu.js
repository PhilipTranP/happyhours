import React, { Component } from 'react'
import { Meteor } from 'meteor/meteor'
import { Link } from 'react-router'
import { createContainer } from 'meteor/react-meteor-data'
import { Bert } from 'meteor/themeteorchef:bert'
import YouTube from 'react-youtube';
import _ from 'lodash'

import { CookingInstruction } from '../../../api/instructions/methods.js'
import Menus from '../../../api/menus/menus.js'
import menuEditor from '../../../modules/menu-editor.js'
import { removeInstruction } from '../../../api/instructions/methods.js'
import Instructions from '../../../api/instructions/instructions.js'
import Loading from '../../common/Loading'
import MenuEditor from '../../components/MenuEditor'
import ViewDocument from '../../pages/ViewDocument'


class EditMenu extends Component {

    constructor(props) {
      super(props);
      this.state = {
       showAddInstructionButton: true,
       displayInstructionUpdateForm: false,
       showHideMenuEditor: true,
       showCreateInstructionForm: false,
       instructionVideo: null,
       instructionVideoFromMenu: null,
       restaurantIdFromMenu:null,
       openInstructionVideo: false,
       startTime: null,
       endTime: null,
       instructionText: '',
       videoId: null,
       player: null
      }
      this.onToStartTime = this.onToStartTime.bind(this)
      this.first17Words = this.first17Words.bind(this)
    }


    onToStartTime(event) {
      this.setState({
        player: event.target.playVideo(),
      });
        this.state.player.seekTo(this.state.startTime, true);
        if(YouTube.PlayerState=1){
          const loopStart = () => {this.state.player.seekTo(this.state.startTime, true)}
          return setTimeout(loopStart, (this.state.endTime - this.state.startTime)*1000)
        }
    }

    componentDidMount(){
      menuEditor({component: this})
    }

    first17Words(words){
      return words.split(' ').slice(0,17).join(' ') + " ..."
    }

    RenderMenus() {
       if(!this.props.menu){return <div>loading...</div>}
       const { menu } = this.props
        return (
          <div>
            <div className="embed-responsive embed-responsive-16by9" style={{marginTop: "52px"}}>
                <YouTube videoId={menu.instructionVideo}/>
            </div>

           { this.state.showAddInstructionButton

             ?

              <div style={{marginTop: "20px"}}>
                <button className="btn btn-success pull-right" onClick={()=>this.setState({showCreateInstructionForm: !this.state.showCreateInstructionForm,
                restaurantIdFromMenu: menu.restaurantId,
                instructionVideoFromMenu: menu.instructionVideo,
                showAddInstructionButton: !this.state.showAddInstructionButton
                })}>

                  ADD INSTRUCTION

                </button>
              </div>

              : null

            }

            <div style={{paddingBottom: "20px"}}>
              <h3>{menu.restaurantName} restaurant serves {menu.title} </h3>
              <h4 style={{fontWeight:"300"}}>{menu.description}</h4>
            </div>
            <div className="embed-responsive embed-responsive-16by9" style={{marginTop: "52px"}}>
                <YouTube videoId={menu.introVideo}/>
            </div>
          </div>
        )
    }


    RenderRestaurantInfo() {
         if(!this.props.menu){return <div>loading...</div>}
         const { menu } = this.props

          return (
            <div>
              <div style={{marginTop: "30px", paddingBottom: "20px"}}>
                <h3>{menu.restaurantName.toUpperCase()} RESTAURANT</h3>
                <h4 style={{fontWeight:"300"}}>{menu.restaurantAbout}</h4>
              </div>
              <div>
                <img  className="img-responsive" src={`https://source.unsplash.com/${menu.coverImage}/400x250`} />
              </div>

            </div>

          )
      }

    RenderInstructions(){
      if(!this.props.instructions){return <div style={{marginTop:"100px"}}><Loading /></div>}

        const handleRemoveInstruction = (_id) => {
          if (confirm('Are you sure? This is permanent!')) {
            Meteor.call('instructions.remove', { _id }, (error) => {
              if (error) {
                Bert.alert(error.reason, 'danger');
              } else {
                Bert.alert('Instruction deleted!', 'success');
              }
            });
          }
        };

      return this.props.instructions.map((instruction, index) => {
        return(
              <div key={instruction._id}  style={{paddingBottom: "20px"}}>

                  <h3>{instruction.title}<span style={{fontWeight: "300"}} className="pull-right">$ {instruction.price}</span></h3>

                  <a href="#"  onClick={ ()=>{this.setState({startTime: instruction.startTime, endTime: instruction.endTime, videoId: instruction.instructionVideo, instructionText: instruction.instructions, openInstructionVideo: !this.state.openInstructionVideo})} } >


                  <img className="img-responsive"
                     style={{display: "block",
                     marginTop: "30px",
                     marginLeft: "auto",
                     marginRight: "auto" }} src={`http://media2.giphy.com/media/${instruction.image}/200.gif`} alt="" />

                  </a>

                  {/*setState from display none to display block to show edit form*/}

                    <div>
                        <h4>{instruction.description}</h4>
                        <a href="#"  onClick={ ()=>{this.setState({startTime: instruction.startTime, endTime: instruction.endTime, videoId: instruction.instructionVideo, instructionText: instruction.instructions, openInstructionVideo: !this.state.openInstructionVideo})} } >
                        <h4 style={{fontWeight: "300"}}>{this.first17Words(instruction.instructions)}</h4></a>

                         <div style={{marginLeft: "10px", paddingBottom: "1px"}}>
                           <a href="#"><i className="fa fa-pencil-square-o pull-right"
                             style={{marginLeft: "10px", marginTop: "1px"}}
                             onClick={()=>this.setState({displayInstructionUpdateForm: !this.state.displayInstructionUpdateForm})}></i></a>

                           <a href="#"><i className="fa fa-trash-o pull-right"  onClick={()=>handleRemoveInstruction(instruction._id)}></i></a>
                       </div>
                       <hr />
                   </div>

                  {/*display edit form when click*/}

                  <div style={{display: this.state.displayInstructionUpdateForm  ? 'block' : 'none'}}>

                        <div className="form-group" style={{marginTop: "30px", marginRight: "20px"}}>

                           <div className="form-group">
                              <label> Title </label>
                              <input type="text" ref="titleUpdate" defaultValue={instruction.title} className="form-control" />
                           </div>

                           <div className="form-group">
                             <label> Item Price </label>
                             <input type="text" ref="priceUpdate" className="form-control" defaultValue={instruction.price} placeholder="Example: 8 "/>
                           </div>

                           <div className="form-group">
                               <label> Description </label>
                               <textarea className="form-control" rows="5" defaultValue={instruction.description} ref="descriptionUpdate"></textarea>
                           </div>

                           <div className="form-group">
                               <label> Instructions </label>
                                 <div className="input-group"style={{marginBottom: "15px"}}>
                                     <input type="text" ref="startTimeUpdate" className="form-control" defaultValue={instruction.startTime} placeholder="Start time: e.g. 0:08"  />
                                     <span className="input-group-addon">-</span>
                                     <input type="text" ref="endTimeUpdate" className="form-control" defaultValue={instruction.endTime} placeholder="End time: e.g. 5:15"/>
                                 </div>
                                 <textarea className="form-control" rows="5" defaultValue={instruction.instructions} ref="instructionsUpdate"></textarea>
                           </div>

                           <div className="form-group">
                              <label> Image </label>
                              <input type="text" ref="imageUpdate" defaultValue={instruction.image} className="form-control" />
                           </div>
                           <div style={{display: 'none'}}>
                             <div className="form-group">
                                <label> Instruction Video </label>
                                <input type="text" ref="instructionVideoUpdate" defaultValue={instruction.instructionVideo} className="form-control" />
                             </div>
                             <div className="form-group">
                                <label> MenuId </label>
                                <input type="text" ref="menuIdUpdate" defaultValue={instruction.menuId} className="form-control" />
                             </div>
                             <div className="form-group">
                                <label> RestaurantId </label>
                                <input type="text" ref="restaurantIdUpdate" defaultValue={instruction.restaurantId} className="form-control" />
                             </div>

                           </div>
                           <button className="btn btn-default" onClick={()=>this.setState({displayInstructionUpdateForm: !this.state.displayInstructionUpdateForm})}>

                             Cancel

                           </button>
                           <span>
                             <button className="btn btn-success pull-right" onClick={ () => handleInstructionUpdateForm() }>

                             Save Changes

                             </button>
                           </span>
                       </div>

                  </div>
            </div>
        )
      })
    }



  //Main rendering starts here!!!

  render() {

    let totalPrice = _.sumBy(this.props.instructions, 'price')
    if(this.props.instructions.length < 1){totalPrice = null}

    handleInstructionFormSubmit = () => {

      {/*Function to convert hour:munute:second input to number in seconds*/}

      const convertTime = (hms) => {
        if (hms.length <3){
         return hms
        } else if (hms.length <6){
          const a = hms.split(':')
          return hms = (+a[0]) * 60 + (+a[1])
        } else {
          const a = hms.split(':')
          return hms = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2])
        }
      }

      const upsert = {
         authorId: Meteor.userId(),
         subscribers: [ Meteor.userId()],
         title: this.refs.title.value.trim(),
         price: parseInt(this.refs.price.value),
         description: this.refs.description.value.trim(),
         instructionVideo: this.state.instructionVideoFromMenu,
         startTime: parseInt(convertTime(this.refs.startTime.value.trim())),
         endTime: parseInt(convertTime(this.refs.endTime.value.trim())),
         image: this.refs.image.value.trim(),
         instructions: this.refs.instructions.value,
         menuId: this.props.params._id,
         restaurantId: this.state.restaurantIdFromMenu

       }

       const confirmation = 'Cooking instructions added!'

       Meteor.call('CookingInstruction.upsert', upsert , (error, response) =>{
         if (error) {
           Bert.alert(error.reason, 'danger');
         } else {
           Bert.alert(confirmation, 'success');
           this.refs.title.value = ''
           this.refs.price.value = ''
           this.refs.startTime.value = ''
           this.refs.endTime.value = ''
           this.refs.image.value = ''
           this.refs.description.value =''
           this.refs.instructions.value = ''
           this.setState({showCreateInstructionForm: !this.state.showCreateInstructionForm,
           showAddInstructionButton: !this.state.showAddInstructionButton})
         }
       })
    }


    handleInstructionUpdateForm = () => {

          {/*Function to convert hour:munute:second input to number in seconds*/}


          const upsert = {
             title: this.refs.titleUpdate.value.trim(),
             price: parseInt(this.refs.priceUpdate.value),
             description: this.refs.descriptionUpdate.value.trim(),
             instructionVideo: this.refs.instructionVideoUpdate.value.trim(),
             startTime: parseInt(this.refs.startTimeUpdate.value),
             endTime: parseInt(this.refs.endTimeUpdate.value),
             image: this.refs.imageUpdate.value.trim(),
             instructions: this.refs.instructionsUpdate.value,
             menuId: this.refs.menuIdUpdate.value,
             restaurantId: this.refs.restaurantIdUpdate.value
           }

           const confirmation = 'Cooking instructions Updated!'
           Meteor.call('CookingInstruction.upsert', upsert , (error, response) =>{
             if (error) {
               Bert.alert(error.reason, 'danger');
             } else {
               Bert.alert(confirmation, 'success');
               this.refs.titleUpdate.value = ''
               this.refs.priceUpdate.value = ''
               this.refs.startTimeUpdate.value = ''
               this.refs.endTimeUpdate.value = ''
               this.refs.imageUpdate.value = ''
               this.refs.descriptionUpdate.value =''
               this.refs.instructionsUpdate.value = ''
               this.refs.menuIdUpdate.value = ''
               this.refs.instructionVideoUpdate.value = ''
               this.refs.restaurantIdUpdate.value = ''
               this.setState({displayInstructionUpdateForm: !this.state.displayInstructionUpdateForm,
               })
             }
           })
        }

    return(
      <div>

        {/** Left Section **/}

        <div style={{overflowY: "scroll", position: "absolute", top: "40px", left: "60px", bottom: "0px", float: "left", width: "30%", paddingRight: "20px", zIndex: '-100' }}>

          {/* Hide and Show Instruction Form*/}

          <div>
            {
            this.state.showCreateInstructionForm

            ?
               <div className="form-group" style={{marginTop: "30px", marginRight: "20px"}}>

                  <div className="form-group">
                     <label> Title </label>
                     <input type="text" ref="title" className="form-control" />
                  </div>

                  <div className="form-group">
                    <label> Item Price </label>
                    <input type="text" ref="price" className="form-control" placeholder="Example: 8 "/>
                  </div>

                  <div className="form-group">
                      <label> Description </label>
                      <textarea className="form-control" rows="5" ref="description"></textarea>
                  </div>

                  <div className="form-group">
                      <label> Instructions </label>
                        <div className="input-group"style={{marginBottom: "15px"}}>
                            <input type="text" ref="startTime" className="form-control" placeholder="Start time: e.g. 0:08"  />
                            <span className="input-group-addon">-</span>
                            <input type="text" ref="endTime" className="form-control" placeholder="End time: e.g. 5:15"/>
                        </div>
                        <textarea className="form-control" rows="5" ref="instructions"></textarea>
                  </div>

                  <div className="form-group">
                     <label> Image </label>
                     <input type="text" ref="image" className="form-control" />
                  </div>

                  <button className="btn btn-default" onClick={()=>this.setState({
                      showCreateInstructionForm: !this.state.showCreateInstructionForm,
                      showAddInstructionButton: !this.state.showAddInstructionButton
                    })}>

                    Cancel

                  </button>
                  <span>
                    <button className="btn btn-success pull-right" onClick={ () => handleInstructionFormSubmit() }>

                    Submit

                    </button>
                  </span>
              </div>

            : null

          }
          </div>

          {/*Show Restaurant Header Section */}

            <div>

              {this.RenderRestaurantInfo()}

            </div>

          {/*Items and Instructions Section */}

          <div style={{marginTop: "30px", paddingBottom: "10px"}}>
             <div>
                <h3 style={{fontWeight: "300"}}>{this.props.instructions.length}
                    { this.props.instructions.length > 1 ? ' items' : ' menu item ' }
                    <span style={{fontSize: '16px', fontWeight: "300"}}>{ this.props.instructions.length < 1 ? ' Click "Add Instruction" to add' : null }</span>
                     <span className="pull-right" style={{fontWeight: "300"}}>{ this.props.instructions.length < 1 ? null : '$' } {totalPrice} </span>
                </h3>
                <hr />

                {this.RenderInstructions()}

            </div>

            <a href="#">
              <button style={{marginTop: "22px"}} className="btn btn-default pull-right">
                { this.props.instructions.length > 0 ? 'Publish Menu' : null}
              </button>
            </a>
          </div>

        {/*Guide of how to make instruction section */}

           <div style={{marginTop: "50px"}}>

                 <hr />

                 <h3>Quick Start Guide</h3>

                 <h3 style={{fontWeight:"300", paddingBottom: "30px"}}>
                   In making effective step by step cooking instruction with accompanied video clips
                 </h3>

               <div className="embed-responsive embed-responsive-4by3" >

                   <YouTube videoId="JHW-Wzoh8a0"/>

               </div>
           </div>

     </div> {/* End of Left Section */}

      {/** Right Section **/}

      <div className="visible-md visible-lg" style={{overflowY: "scroll", position: "absolute", top: "0px", left: "470px", bottom: "0px", float: "left",  width: "60%", zIndex: '-100' }}>

        {/*Show Menu Editor */}

            <div style={{marginTop: "100px", marginBottom:"30px", paddingRight:"20px"}}>

              <Link to={`/admin/restaurants/menus/${this.props.params._id}`}>
               <button className="btn btn-default pull-right" style={{marginTop: "22px"}}>Cancel</button>
              </Link>

              { this.state.showHideMenuEditor
                 ?
                   <MenuEditor menu={this.props.menu}/>

                 : null
               }
              <hr />

            </div>



        {/*Show video playing at startTime when click on the left instruction*/}

            {
              this.state.openInstructionVideo
              ?
                 <div style={{marginTop: "50px", marginRight: "20px"}}>
                    <div className="embed-responsive embed-responsive-16by9" >

                        <YouTube videoId={this.state.videoId}  onReady={this.onToStartTime}/>

                    </div>
                    <h4 style={{paddingTop:"20px", paddingBottom: "30px", fontWeight:"400"}}>
                       {this.state.instructionText}
                    </h4>
                    <hr />
                  </div>
              : null
            }

          <div style={{paddingRight:"20px"}}>

               { this.RenderMenus() }

          </div>
        </div>
      </div>
    )
  }
}

export default createContainer((props) => {
    const {_id} = props.params
    const subscription = Meteor.subscribe('menus.view', _id);
    const subscription2 = Meteor.subscribe('instructions.list')
    const loading = !subscription.ready() &&  !subscription2.ready()
    const menu = Menus.findOne()
    const instructions = Instructions.find({"menuId" : _id}).fetch()
    return { loading, menu, instructions}
}, EditMenu)
