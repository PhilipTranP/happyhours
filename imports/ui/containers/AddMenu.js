import { Meteor } from 'meteor/meteor';
import { composeWithTracker } from 'react-komposer';
import Menus from '../../api/menus/menus.js';
import AddMenu from '../pages/AddMenu.js';
import Loading from '../components/Loading.js';

const composer = ({ params }, onData) => {
  const subscription = Meteor.subscribe('documents.view', params._id);

  if (subscription.ready()) {
    const doc = Documents.findOne();
    onData(null, { doc });
  }
};

export default composeWithTracker(composer, Loading)(EditDocument);
