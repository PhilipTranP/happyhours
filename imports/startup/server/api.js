import '../../api/documents/methods.js';
import '../../api/documents/server/publications.js';
import '../../api/accounts/server/accounts.js';

import '../../api/menus/methods.js';
import '../../api/menus/server/publications.js';

import '../../api/instructions/methods.js';
import '../../api/instructions/server/publications.js';


import '../../api/stats/methods.js';
import '../../api/stats/server/publications.js';


import '../../api/inquiries/methods.js';
import '../../api/inquiries/server/publications.js';


import '../../api/transactions/transactions.js';
import '../../api/transactions/server/publications.js';

import '../../api/blogs/methods.js';
import '../../api/blogs/server/publications.js';

import '../../api/my-collections/methods.js';
import '../../api/my-collections/server/publications.js';

import '../../api/orders/methods.js';
import '../../api/orders/server/publications.js';
import '../../api/orders/server/email-notifs.js';
import '../../api/orders/server/stripe.js';

import '../../api/public';
