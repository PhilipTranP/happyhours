import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
import { Accounts } from 'meteor/accounts-base';



if (!Meteor.isProduction) {
  const users = [
    {
      email: 'plt1028@gmail.com',
      password: 'password',
      profile: {
        name: { first: 'Phuong', last: 'Tran' },
      },
      roles: ['admin'],
    },

//     {
//       email: 'philiptranp@gmail.com',
//       password: 'password',
//       profile: {
//         name: { first: 'Philip', last: 'Tran' },
//       },
//       roles: ['admin'],
//     },
//
//     {
//       email: 'quanptit410@gmail.com',
//       password: 'password',
//       profile: {
//         name: { first: 'Quan', last: 'Trinh' },
//       },
//       roles: ['admin'],
//     },
//
//     {
//       email: 'bdangvn@gmail.com',
//       password: 'password',
//       profile: {
//         name: { first: 'Dang', last: 'Bui' },
//       },
//       roles: ['admin'],
//     }
//
];

  users.forEach(({ email, password, profile, roles }) => {
    const userExists = Meteor.users.findOne({ 'emails.address': email });

    if (!userExists) {
      const userId = Accounts.createUser({ email, password, profile });
      Roles.addUsersToRoles(userId, roles);
    }
  });
}
