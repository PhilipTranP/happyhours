import { Bert } from 'meteor/themeteorchef:bert';
import 'bootstrap/dist/css/bootstrap.min.css';
import './routes.js';

Bert.defaults.style = 'fixed-top';
Bert.defaults.hideDelay = 5000;


Stripe.setPublishableKey(Meteor.settings.public.stripe);
