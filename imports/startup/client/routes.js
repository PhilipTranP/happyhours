/* eslint-disable max-len */

import React from 'react';
import { render } from 'react-dom';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import { Meteor } from 'meteor/meteor';


import VideoInfo from '../../ui/components/MenuDetails/ItemDetails/VideoInfo'
import App from '../../ui/layouts/App.js';
import Documents from '../../ui/pages/Documents.js';
import NewDocument from '../../ui/pages/NewDocument.js';
import NewMenu from '../../ui/pages/NewMenu.js';



import AdminLogin from '../../ui/containers/admin/AdminLogin.js';
import Menu from '../../ui/containers/admin/Menu.js';
import InquiryList from '../../ui/containers/admin/InquiryList.js';
import BlogList from '../../ui/containers/admin/BlogList.js';
import EditMenu from '../../ui/containers/admin/EditMenu.js';


import EditDocument from '../../ui/containers/EditDocument.js'
import ViewDocument from '../../ui/containers/ViewDocument.js'


import Landing from '../../ui/pages/HOME/Landing'
import InstructionList from '../../ui/pages/HOME/InstructionList'
import MyCollectionsList from '../../ui/pages/HOME/My-Collections'
import Home from '../../ui/pages/HOME/index'
import Restaurants from '../../ui/pages/HOME/Restaurants'
import SuggestRestaurant from '../../ui/pages/HOME/SuggestRestaurant'
import Unsubscribe from '../../ui/pages/HOME/Unsubscribe'

import BlogMain from '../../ui/pages/BLOG/Main'
import BlogView from '../../ui/pages/BLOG/BlogView'


import ViewMenu from '../../ui/pages/ViewMenu.js'
import MenuOrder from '../../ui/pages/ORDER/MenuOrder.js'

import MenuDetailsLayout from '../../ui/components/MenuDetails/MenuDetailsLayout'

import NotFound from '../../ui/pages/NotFound.js';
import RecoverPassword from '../../ui/pages/RecoverPassword.js';
import ResetPassword from '../../ui/pages/ResetPassword.js';
import Signup from '../../ui/pages/Signup.js';
import MemberSignup from '../../ui/pages/MemberSignup.js';

import MenusList from '../../ui/menus/containers/MenusList.js'


const authenticate = (nextState, replace) => {
  if (!Meteor.loggingIn() && !Meteor.userId()) {
    replace({
      pathname: '/',
      state: { nextPathname: nextState.location.pathname },
    });
  }
};

Meteor.startup(() => {
  render(
    <Router history={ browserHistory }>
       <Route path="/">
          <IndexRoute component={ Landing } />
          <Route path="/" component={ App }>
            <Route name="home" path="menus" component={ Home } />
            <Route name="instructionList" path="/recipes" component={ InstructionList } onEnter={ authenticate } />
            <Route name="myCollectionsList" path="/my-collections" component={ MyCollectionsList } onEnter={ authenticate } />
            <Route name="news" path="news" component={ BlogMain } onEnter={ authenticate }/>
            <Route name="unsubscribe" path="unsubscribe" component={ Unsubscribe } />
            <Route name="newsView" path="/news/:_id" component={ BlogView } onEnter={ authenticate } />
            <Route name="restaurants" path="restaurants" component={ Restaurants } onEnter={ authenticate } />
            <Route name="suggestRestaurants" path="restaurants/suggest" component={ SuggestRestaurant } />
            <Route name="menu-detail" path="menu-details" component={ MenuDetailsLayout } />
            <Route name="adminRestaurants" path="admin/restaurants" component={ Documents } onEnter={ authenticate } />
            <Route name="adminInquiries" path="admin/inquiries" component={ InquiryList } onEnter={ authenticate } />
            <Route name="adminBlogs" path="admin/blogs" component={ BlogList } onEnter={ authenticate } />
            <Route name="newRestaurant" path="restaurants/new" component={ NewDocument } onEnter={ authenticate } />
            <Route name="newMenu" path="menus/new" component={ NewMenu } onEnter={ authenticate } />
            <Route name="menuList" path="menus-list" component={ MenusList } onEnter={ authenticate } />
            <Route name="editRestaurant" path="admin/restaurants/:_id/edit" component={ EditDocument } onEnter={ authenticate } />
            <Route name="viewRestaurantMenu" path="restaurants/menus/:_id" component={ MenuDetailsLayout } onEnter={ authenticate } />
            <Route name="viewRestaurant" path="admin/restaurants/:_id" component={ ViewDocument } onEnter={ authenticate } />
            <Route name="Menu" path="admin/restaurants/menus/:_id" component={ Menu } onEnter={ authenticate } />
            <Route name="editMenu" path="admin/restaurants/menus/:_id/edit" component={ EditMenu } onEnter={ authenticate } />
            <Route name="viewMenu" path="menus/:_id" component={ ViewMenu } onEnter={ authenticate } />
            <Route name="menuOrder" path="menus/:_id/order" component={ MenuOrder } onEnter={ authenticate } />
            <Route name="adminLogin" path="adm/login" component={ AdminLogin } />
            <Route name="recover-password" path="recover-password" component={ RecoverPassword } />
            <Route name="reset-password" path="reset-password/:token" component={ ResetPassword } />
            <Route name="signup" path="signup" component={ Signup } />
            <Route name="memberSignup" path="member-signup/:_id" component={ MemberSignup } />
            <Route path="*" component={ NotFound } />
          </Route>
        </Route>
    </Router>,
    document.getElementById('react-root')
  );
});
