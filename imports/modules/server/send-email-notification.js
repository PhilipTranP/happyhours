import { Meteor } from 'meteor/meteor';
import { Email } from 'meteor/email';
// import templateToHTML from './template-to-html.js';

const sendEmail = (template, { to, from, replyTo, subject, attachments }, payload) => {
  const email = {
    to,
    from: from || '2-5pm.com <notifications@2to5pm.com>',
    replyTo,
    subject,
    html: templateToHTML(template, payload),
  };

  if (attachments) email.attachments = attachments;
  Meteor.defer(() => { Email.send(email); });
};

const notifications = {
  orderWasPlaced({ to, orderId }) {
    sendEmail('standard', {
    to,
    replyTo: 'notifications@2to5pm.com',
    subject: '[2-5pm.com] Your Order Was Submitted!',
    }, {
      title: 'Your package was delivered!',
      subtitle: 'ShipShape detected a succesful delivery of your package.',
      body: templateToHTML('shipment-delivered', { shipmentId }),
      callToAction: {
        url: `https://shipshape.fm/shipments/${shipmentId}`,
        label: 'View Shipment Details',
      },
    });
  },
  shipmentIsDelayed({ to, shipmentId }) {},
  shipmentWasDelivered({ to, shipmentId }) {},
};

export default function(type, payload) {
  const notification = notifications[type];
  if (notification) {
    notification(payload);
  } else {
    throw new Meteor.Error('500', 'Sorry, that notification does not exist.');
  }
}
