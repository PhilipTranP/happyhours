/* eslint-disable no-undef */

import { browserHistory } from 'react-router';
import { Bert } from 'meteor/themeteorchef:bert';
import { FormGroup, ControlLabel, FormControl, Button } from 'react-bootstrap';
import { upsertMenu } from '../api/menus/methods.js';
import './validation.js';


let component;

const handleUpsert = () => {
  const { menu } = component.props;
  const confirmation = menu && menu._id ? 'Menu updated!' : 'Menu added!';
    const upsert = {
      isPublished: false,
      title: document.querySelector('[name="title"]').value.trim(),
      description: document.querySelector('[name="description"]').value.trim(),
      introVideo: document.querySelector('[name="introVideo"]').value.trim(),
      instructionVideo: document.querySelector('[name="instructionVideo"]').value.trim(),
      instructionVideo: component.refs.instructionVideo.value.trim(),
      coverImage: "https://source.unsplash.com/" + document.querySelector('[name="coverImage"]').value.trim(),
      restaurantId: menu.restaurantId,
      restaurantName: menu.restaurantName,
      restaurantAbout: menu.restaurantAbout,
      chefName: menu.chefName,
      chefBio: menu.chefBio,
      viewCounts: 1
    }

  if (menu && menu._id) upsert._id = menu._id;

  Meteor.call ('menus.upsert', upsert, (error, response) => {
    if (error) {
      console.log("update error")
      Bert.alert(error.reason, 'danger');
    } else {
      component.menuEditorForm.reset();
      Bert.alert(confirmation, 'success');
      browserHistory.push(`admin/restaurants/menus/${response.insertedId || menu._id}`);
    }
  });
};

const validate = () => {
  $(component.menuEditorForm).validate({
    rules: {
      isPublished: {
        required: true,
      },
      title: {
        required: true,
      },
      description: {
        required: true,
      },
      introVideo: {
        required: true,
      },
      instructionVideo: {
        required: true,
      },
      coverImage: {
        required: true,
      },
      restaurantId: {
        required: true,
      },
      restaurantName: {
        required: true,
      },
      restaurantAbout: {
        required: true,
      },
      chefName: {
        required: true,
      },
      chefBio: {
        required: true,
      },
      viewCounts: {
        required: true,
      }
    },
    messages: {
      title: {
        required: 'Need a title in here!',
      },
      description: {
        required: 'Need some info about the menu, please.',
      },
      introVideo: {
        required: 'Need intro video here!',
      },
    },
    submitHandler() { handleUpsert(); },
  });
};

export default function menuEditor(options) {
  component = options.component;
  validate();
}
