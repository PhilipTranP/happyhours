/* eslint-disable no-undef */

import { browserHistory } from 'react-router';
import { Bert } from 'meteor/themeteorchef:bert';
import { upsertDocument } from '../api/documents/methods.js';
import './validation.js';

let component;

const handleUpsert = () => {
  const { doc } = component.props;
  const confirmation = doc && doc._id ? 'Document updated!' : 'Document added!';
  const upsert = {
    restaurantName: document.querySelector('[name="restaurantName"]').value.trim(),
    restaurantAbout: document.querySelector('[name="restaurantAbout"]').value.trim(),
    chefName: document.querySelector('[name="chefName"]').value.trim(),
    chefBio: document.querySelector('[name="chefBio"]').value.trim(),
  };

  if (doc && doc._id) upsert._id = doc._id;

  upsertDocument.call(upsert, (error, response) => {
    if (error) {
      Bert.alert(error.reason, 'danger');
    } else {
      component.documentEditorForm.reset();
      Bert.alert(confirmation, 'success');
      browserHistory.push(`admin/restaurants/${response.insertedId || doc._id}`);
    }
  });
};

const validate = () => {
  $(component.documentEditorForm).validate({
    rules: {
      restaurantName: {
        required: true,
      },
      restaurantAbout: {
        required: true,
      },
      chefName: {
        required: true,
      },
      chefBio: {
        required: true,
      },
    },
    messages: {
      restaurantName: {
        required: 'Need a name in here!',
      },
      restaurantAbout: {
        required: 'Need some info about the restaurant, please.',
      },
      chefName: {
        required: 'Need a chef name in here!',
      },
      chefBio: {
        required: 'Please add some bio of the chef.',
      },
    },
    submitHandler() { handleUpsert(); },
  });
};

export default function documentEditor(options) {
  component = options.component;
  validate();
}
