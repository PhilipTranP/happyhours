# Deliver Happy Hours - 2-5pm.com
A full stack foods delivery systems developed by Philip Phuong Tran.
The backend of the system uses Meteor and MongoDb. The front end uses React.

<table>
  <tbody>
    <tr>
      <th>React Version</th>
      <td>v15.4.2</td>
    </tr>
    <tr>
      <th>Meteor Version</th>
      <td>v1.4.2.3</td>
    </tr>
  </tbody>
</table>
